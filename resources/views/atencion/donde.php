<?php $this->renderPartial('/atencion/menu', array('xPagina' => 'donde')); ?>

<div class="container-fluid donde_top">
	<div class="container sp">
    	<div class="col-xs-12">
    		<h1>OFICINAS DE ATENCIÓN AL CLIENTE</h1>
        </div>
   	  	<div class="col-xs-3 donde">
            <h2>NUEVA CÓRDOBA</h2>
			<p>Obispo Oro 50. Nueva Córdoba 
			Atención de 9 a 18 hs.</p>
        </div>
        <div class="col-xs-3 donde">
            <h2>CERRO DE LAS ROSAS</h2>
			<p>Av Rafael Nuñez 5021. Local 1
			Atención de 9 a 18 hs.</p>
        </div>
        <div class="col-xs-3 donde">
            <h2>VILLA ALLENDE</h2>
			<p>Río de Janeiro 281 Local 2. Villa Allende
			Atención de 9 a 13 hs. / 14 a18 hs.</p>
        </div>
        <div class="col-xs-3 donde">
            <h2 style="color:#00a893;">CENTROS MÉDICOS MET</h2>
			<p>· Independencia 714. Nueva Córdoba<br>
            · Av Rafael Nuñez 5021. Local 1<br>
			Atención de 8 a 20 hs.</p>
        </div>
    </div>
</div>

<div class="container-fluid at_banner06">
	<div class="container sp">
    	<div class="col-xs-4">
        	<ul class="data_donde">
            	<li class="telefono">0800 3450 638 <br><span>CENTRO DE ATENCIÓN MET</span></li>
                <!--<li class="whatsapp">351-8007777 <br><span>WHATSAPP TURNOS CENTRO MÉDICO MET</span></li>-->
                <li class="face"><a class="small" href="https://www.facebook.com/MetMedicinaPrivada" target="_blank">METMEDICINAPRIVADA</a></li>
                <li class="twitter"><a class="small" href="https://twitter.com/MET_Medicina" target="_blank">MET_MEDICINA</a></li>
            </ul>
        </div>
    </div>
</div>