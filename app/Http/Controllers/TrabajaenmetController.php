<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrabajaenmetController extends Controller
{
	public function index() {
		return view('trabajaenmet.index');
	}
	public function gracias() {
		return view('trabajaenmet.gracias');
	}
}