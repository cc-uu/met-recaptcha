<?php

namespace App\Clases\Persona;
use Illuminate\Http\Request;
use App\Clases\Common\Segmento;

class PersonaWS {
	public $idPersona;
	public $idExterno;
	public $nombre;
	public $apellido;
	public $dni;
	public $habilitado;
	public $fechaNac;
	public $fechaAlta;
	public $fechaModificacion;
	public $codigoPais;
	public $telefono;
	public $telefono2;
	public $telefono3;
	public $movil;
	public $pref1;
	public $pref2;
	public $pref3;
	public $direccion;
	public $numero;
	public $barrio;
	public $pais;
	public $provincia;
	public $localidad;
	public $nomFantasia;
	public $email;
	public $email2;
	public $dpto;
	public $piso;
	public $cp;
	public $observaciones;
	public $sexo;
	public $empresa;
	public $tipoPers;
	public $usuario;
	public $clave;
	public $segmento;

	public function __construct() {
		$this->idPersona = 0;
		$this->idExterno = '';
		$this->nombre = '';
		$this->apellido = '';
		$this->dni = 0;
		$this->habilitado = 'S';
		$this->fechaNac = '';
		$this->fechaAlta = '';
		$this->fechaModificacion = '';
		$this->codigoPais = '';
		$this->telefono = 0;
		$this->telefono2 = 0;
		$this->telefono3 = '';
		$this->movil = 0;
		$this->pref1 = 0;
		$this->pref2 = 0;
		$this->pref3 = 0;
		$this->direccion = '';
		$this->numero = '';
		$this->barrio = '';
		$this->pais = '';
		$this->provincia = '';
		$this->localidad = '';
		$this->nomFantasia = '';
		$this->email = '';
		$this->email2 = '';
		$this->dpto = '';
		$this->piso = '';
		$this->cp = '';
		$this->observaciones = '';
		$this->sexo = '';
		$this->empresa = '';
		$this->tipoPers = 'persona';
		$this->usuario = '';
		$this->clave = '';
		$this->segmento = new Segmento();
	}

	public static function cast(Persona $oPersona) {
		$oPersonaWS = new PersonaWS();
		$oPersonaWS->idPersona = (int) $oPersona->idPersona ?: 0;
		$oPersonaWS->idExterno = '';
		$oPersonaWS->nombre = $oPersona->nombre ?: '';
		$oPersonaWS->apellido = $oPersona->apellido ?: '';
		$oPersonaWS->dni = ($oPersona->dni instanceof Documento) ? (int) $oPersona->dni->numero : ((int) $oPersona->dni ?: 0);
		$oPersonaWS->habilitado = $oPersona->habilitado ?: 'S';
		$oPersonaWS->fechaNac = ($oPersona->fecha instanceof DateTime) ? $oPersona->fecha->format('Y-m-d') : '';
		$oPersonaWS->fechaAlta = '';
		$oPersonaWS->fechaModificacion = '';
		$oPersonaWS->camposPersonales = isset($oPersona->camposPersonales) ? $oPersona->camposPersonales : [];
		$oPersonaWS->perfilCustomxPersona = isset($oPersona->perfilCustomxPersona) ? $oPersona->perfilCustomxPersona : [];

		$oPersonaWS->direccion = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->calle :
		(isset($oPersona->direccion) ? $oPersona->direccion : '');

		$oPersonaWS->numero = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->numero :
		(isset($oPersona->numero) ? $oPersona->numero : '');

		$oPersonaWS->barrio = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->barrio :
		(isset($oPersona->barrio) ? $oPersona->barrio : '');

		$oPersonaWS->pais = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->pais :
		(isset($oPersona->pais) ? $oPersona->pais : '');

		$oPersonaWS->provincia = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->provincia :
		(isset($oPersona->provincia) ? $oPersona->provincia : '');

		$oPersonaWS->localidad = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->localidad :
		(isset($oPersona->localidad) ? $oPersona->localidad : '');

		$oPersonaWS->dpto = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->dpto :
		(isset($oPersona->dpto) ? $oPersona->dpto : '');

		$oPersonaWS->piso = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->piso :
		(isset($oPersona->piso) ? $oPersona->piso : '');

		$oPersonaWS->cp = (isset($oPersona->direccion) && $oPersona->direccion instanceof Direccion) ? $oPersona->direccion->cp :
		(isset($oPersona->cp) ? $oPersona->cp : '');

		$oPersonaWS->nomFantasia = $oPersona->nombreFantasia ?: '';
		$oPersonaWS->email = isset($oPersona->emails[0]) ? $oPersona->emails[0] : ((string) $oPersona->email ?: '');
		$oPersonaWS->email2 = isset($oPersona->emails[1]) ? $oPersona->emails[1] : ((string) $oPersona->email2 ?: '');
		$oPersonaWS->observaciones = $oPersona->observaciones ?: '';
		$oPersonaWS->empresa = $oPersona->empresa ?: '';
		$oPersonaWS->tipoPers = $oPersona->tipo ?: 'persona';
		$oPersonaWS->usuario = $oPersona->usuario ?: '';
		$oPersonaWS->clave = $oPersona->clave ?: '';
		$oPersonaWS->segmento = ['idSegmento' => 'MzY4MQ'];

		$oPersonaWS->sexo = $oPersona->sexo; //($oPersona->sexo instanceof Sexo) ? str_replace( ['0','1','3'], ['F','M','I'], (string) $oPersona->sexo->idSexo ) : 'I'; // M/F/I
		$oPersonaWS->codigoPais = (!empty($oPersona->telefonosAPI['codigoPais'])) ? $oPersona->telefonosAPI['codigoPais'] : '54';
		$oPersonaWS->telefono = (int) (!empty($oPersona->telefono) ? $oPersona->telefono : 0);
		$oPersonaWS->telefono2 = (int) (!empty($oPersona->telefono2) ? $oPersona->telefono2 : 0);
		$oPersonaWS->movil = (int) (!empty($oPersona->movil) ? $oPersona->movil : 0);
		$oPersonaWS->pref1 = (int) (!empty($oPersona->pref1) ? $oPersona->pref1 : 0);
		$oPersonaWS->pref2 = (int) (!empty($oPersona->pref2) ? $oPersona->pref2 : 0);
		$oPersonaWS->pref3 = (int) (!empty($oPersona->pref3) ? $oPersona->pref3 : 0);
		$oPersonaWS->telefono3 = (string) (!empty($oPersona->telefono3) ? $oPersona->telefono3 : 0);

		return $oPersonaWS;
	}
}