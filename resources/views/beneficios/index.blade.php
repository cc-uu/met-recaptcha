@extends('layouts.app')
@section('title', ' | Beneficios')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion beneficios ampliada">     
        <div class="container encabezado">
            <h1>BENEFICIOS</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <button class="chat">Chat <span>Online</span></button>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse"><h1>VER POR ÁREA</h1><img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul class="title-menu d-none d-sm-block">
                        <li>
                            <h1>VER POR ÁREA</h1>
                        </li>
                    </ul>
                    <ul class="menu-beneficios">
                        <li>
                            <a class="{{ !(request()->is('beneficios/automotor')) ? : 'active'}}" href="{{ route('beneficioByTag', ['tag' => 'automotor' ]) }}">Automotor</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/educacion')) ? : 'active'}}"   href="{{ route('beneficioByTag', ['tag' => 'educacion' ]) }}" >Educación</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/estetica')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'estetica' ]) }}">Estética</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/gimnasio')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'gimnasio' ]) }}">Gimnasio</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/gastronomia')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'gastronomia' ]) }}">Gastronomía</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/hogar')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'hogar' ]) }}">Hogar</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/hoteleria')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'hoteleria' ]) }}">Hoteleria</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/indumentaria')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'indumentaria' ]) }}">Indumentaria</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/niños')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'niños' ]) }}">Niños</a>
                        </li>
                        <li>
                            <a class="{{ !(request()->is('beneficios/optica')) ? : 'active'}}"  href="{{ route('beneficioByTag', ['tag' => 'optica' ]) }}">Óptica</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2 class="titulo-section"> {{ $Valorfiltro }}  </h2>
                    <div class="row m-0">
                        <div class="col-12 formulario">
                            <div class="vertical">
                               
                               @foreach ($Beneficios as $item)
                               
                                    <div class="modulo-beneficio row">
                                        <div class="col-md-4">
                                            <div class="centrar-img">
                                                <img src="{{ asset($item->imagenes[0]->path) }}" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-5 col-sm-8 col-9 p-md-0">
                                            <div class="info-beneficio">
                                                <h4>{{$item->titulo}}</h4>
                                                <p>  {{ Str::limit(strip_tags($item->descripcion),100)}} </p>
                                                  
                                             
                                                {{-- <p>Estar radiante es tu elección y nosotros te acercamos un beneficio con MET,
                                                en PIEL PLENA accedes al 10% de </p> --}}
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-3 col-sm-4 col-3 p-0 p-md-3">
                                            <a style="color:#000" href="{{ route('beneficioDesplegado', ['id' => $item->idContenido ,'titulo' => str_slug($item->titulo) ]) }}">
                                                <div class="descuento-beneficio">
                                                    @if ($item->bajada)
                                                        <div>{{$item->bajada}}</div>
                                                    @endif
                                                    <span>Ver más</span>
                                                </div>
                                            </a>
                                        </div>
                                        <hr>
                                    </div>
                                         
                               @endforeach

                                <nav aria-label="navigation">
                                    {!! $Beneficios->links() !!}
                                    {{-- <ul class="pagination">
                                      <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1"><img class="svg" src="{{ asset('../images/iconos/down.svg') }}" alt=""></a>
                                      </li>
                            
                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                                      <li class="page-item">
                                        <a class="page-link" href="#"><img class="svg" src="{{ asset('../images/iconos/up.svg') }}" alt=""></a>
                                      </li>
                                    </ul> --}}
                                  </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection
