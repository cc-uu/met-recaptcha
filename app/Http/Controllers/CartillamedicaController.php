<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\WebService\WebService;

use function GuzzleHttp\json_decode;

class CartillamedicaController extends Controller
{
	public function index() {
		$webService = new WebService();
		// $especialidades = $webService->getEspecialidades();
		// $localidades = $webService->getLocalidades();
		// $planes = $webService->getPlanes();

		
		return view('cartillamedica.index');
	}

	public function getInstituciones($institucion,$especialidad,$localidad,$plan,$offset,$limit) {
		$webService = new WebService();
		$instituciones = $webService->getInstituciones($institucion,$especialidad,$localidad,$plan,$offset,$limit);
		return $instituciones;
	}

	public function getProfesionalesByInstitucion($especialidad,$cuit) {
		$webService = new WebService();
		$profesionales = $webService->getProfesionalesByInstitucion($especialidad,$cuit);
		return $profesionales;
	}

	public function getProfesionales($profesional,$especialidad,$localidad,$plan,$offset,$limit) {
		$webService = new WebService();
		$profesionales = $webService->getProfesionales($profesional,$especialidad,$localidad,$plan,$offset,$limit);
		return $profesionales;
	}



	public function getFarmacias($localidad,$plan,$offset,$limit) {
		$webService = new WebService();
		$farmacias = $webService->getFarmacias($localidad,$plan,$offset,$limit);
		return $farmacias;
	}

	public function getOpticas($localidad,$plan,$offset,$limit) {
		$webService = new WebService();
		$opticas = $webService->getOpticas($localidad,$plan,$offset,$limit);
		return $opticas;
	}

	public function getOdontologia($localidad,$plan,$offset,$limit) {
		$webService = new WebService();
		$odontologias = $webService->getOdontologia($localidad,$plan,$offset,$limit);
		return $odontologias;
	}


	public function geolocalizacion() {
		// $webService = new WebService();
		// $especialidades = $webService->getEspecialidades();
		// $localidades = $webService->getLocalidades();
		// $planes = $webService->getPlanes();
		return view('cartillamedica.geolocalizacion');
	}
}