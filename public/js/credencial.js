app.controller("CredencialController", function($scope, $http){
	$scope.tipoDoc = 0;
	$scope.nroDoc = '';
	$scope.tiposDocumentos = vTiposDocumentos;
	$scope.credencial = null;
	$scope.email = null;
	$scope.estadoEmail = null;

	$scope.getTiposDocumentos = function(){
		try{
			if(vTiposDocumentos.length == 0){
				$http.jsonp(xHostWS + "credencial/getTiposDocumentos?callback=JSON_CALLBACK")
				.success(function(result){
					$scope.tiposDocumentos = result[0];
				})
				.error(function(error){
					console.log(error);
				});
			}
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.getCredencial = function(){
		try{
			if($scope.validarFormulario()){
				$http.jsonp(xHostWS + "credencial/getCredencial/" + $scope.tipoDoc + '/' + $scope.nroDoc + '/1/?callback=JSON_CALLBACK')
				.success(function(result){
					$scope.credencial = null;
					$scope.credencial = result[0];
					$scope.resultado = [];

					if($scope.credencial.medDomicilio == 'NO' && $scope.credencial.odontologia == 'NO' && $scope.credencial.pmi == 'NO'){
						$scope.resultado = 'NO';
					}
					else{
						if($scope.credencial.medDomicilio != 'NO')
							$scope.resultado [0]  = 'Médico a Domicilio | ';

						if($scope.credencial.odontologia != 'NO')
							$scope.resultado [1] = 'Odontología | ';

						if($scope.credencial.pmi != 'NO')
							$scope.resultado [2] = 'Plan Materno Infantil';
					}

					if($scope.credencial.documento != null)
						$('.datos-credencial').show();
					else
						$('.datos-credencial').hide();
				})
				.error(function(error){
					console.log(error);
				})
			}
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.ready = function(){
		try{
			$scope.getTiposDocumentos();
			$scope.clearEmail();
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.validarFormulario = function(){
		try{
			var elements = $('form')[0].elements;
			var r = true;
			angular.forEach(elements, function(self){
				var type = $(self)[0].tagName;
				if(type == 'SELECT'){
					if($(self).val() == '0'){
						$(self).addClass('error');
						r = false;
					}
					else
						$(self).removeClass('error');
				}
				if(type == 'INPUT'){
					if($(self).val() == '' || $(self).val().length < 4 || $(self).val().length > 11){
						$(self).addClass('error');
						r = false;
					}
					else
						$(self).removeClass('error');
				}
			});
			return r;
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.cancelar = function(){
		try{
			$('.datos-credencial').hide();
			$scope.credencial = null;
			$scope.tipoDoc = 0;
			$scope.nroDoc = '';
			$('#modal_credencial').modal('hide')
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.mail = function(){
		try{
			ga('send', 'event', 'enviar credencial por email', 'mail');
			expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if($scope.email == null || $scope.email == '')
				$('#sendCredencial').modal();
			else if(expr.test($scope.email)){
				$('#btn-enviar-mail').prop('disabled', true);
				$scope.estadoEmail = 'Enviando mensaje...';

				$http.jsonp(
					xHostWS + 'credencial/sendCredencial/' + 
					$scope.tipoDoc + '/' + 
					$scope.nroDoc + '/' + 
					$scope.email + 
					'?callback=JSON_CALLBACK'
				)
				.success(function(result){
					if(result == 1)
						$scope.estadoEmail = 'Su credencial online fue enviada correctamente al email ' + $scope.email + '.';
					else
						$scope.estadoEmail = 'Ocurrió un error al enviar su credencial. Por favor intente nuevamente.';
				})
				.error(function(error){
					console.log(error)
					$scope.estadoEmail = 'Ocurrió un error al enviar su credencial. Por favor intente nuevamente.';
				});
			}
			else{
				$scope.estadoEmail = 'El email ingresado no es válido.';
			}
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.clearEmail = function(){
		try{
			$('#sendCredencial').on('hidden.bs.modal', function(){
				$scope.email = null;
				$scope.estadoEmail = null;
				$('#btn-enviar-mail').prop('disabled', false);
			});
		}
		catch(e){
			console.log(e);
		}
	}

	$scope.imprimir = function(){
		try{
			$http.jsonp(xHostWS + "credencial/getCredencial/" + $scope.tipoDoc + '/' + $scope.nroDoc + '/2/?callback=JSON_CALLBACK')
			.success()
			.error();
			ga('send', 'event', 'imprimir credencial', 'imprimir');
			setTimeout(function(){
					window.print();
				}, 500)
		}
		catch(e){
			console.log(e);
		}
	}
});

var vTiposDocumentos = Array();

$(document).ready(function(){
	$('.only-numbers').on('keypress', function(event){
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	});

	$('#sendCredencial').on('hidden.bs.modal', function(){
	});
});