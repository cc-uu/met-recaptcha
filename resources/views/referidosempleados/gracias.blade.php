<?php //$this->renderPartial('/layouts/script/css'); ?>

<!-- Codigo de analitycs -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23691022-1']);
  _gaq.push(['_setDomainName', 'metmedicinaprivada.com']);
  _gaq.push(['_trackPageview']);



  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.3.min.js') }}"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos-landing-referidos-empleados.css') }}" />
<script type="text/javascript" src="{{ asset('/js/vendor-referidos-empleados.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/main-referidos-empleados.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/piecu.min.js') }}"></script>


	<header class="head">
        <div class="mwrap">
            <?php if (!empty($xMensaje)) {?>
                <div class="title"><?php echo $xMensaje; ?></div>
                <div class="title">Gracias!</div>
             <?php }?>
        </div>
    </header>
    <div class="banner">
        <div class="banner-image bg-image" style="background-image: url(images/landing-referidos-empleados/slide1.jpg);">
            <div class="banner-content">
                <div class="mwrap">
                    <div class="add-wrapper">
                        <div class="box-text box-text-1">
                            <div class="wrap">
                                <div class="text-1">METé</div>
                                <div class="text-2">un afiliado</div>
                            </div>
                        </div>
                        <div class="box-text box-text-2">
                            <div class="wrap">
                                <div class="text-1">y METé</div>
                                <div class="text-2">bono</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="foot">
        <div class="mwrap">
            <div class="logo">
                <img src="{{ asset('images/landing-referidos-empleados/logo.png') }}" alt="Logo de MET">
            </div>
            <div id="piecu"></div>
        </div>
    </footer>
   