<?php //$this->renderPartial('/layouts/script/css'); ?>

<!-- Codigo de analitycs -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23691022-1']);
  _gaq.push(['_setDomainName', 'metmedicinaprivada.com']);
  _gaq.push(['_trackPageview']);



  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.3.min.js') }}"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MET · Plan MTI</title>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos-landing-mti.css') }}" />
<script type="text/javascript" src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/piecu.min.js') }}"></script>

@include('layouts.headLanding')

  <!-- Slide Head-->
  <div class="container-fluid p-0">
    <div class="wrap-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-md-6">
            <div class="wrap-logo">
              <img src="{{ asset('images/landing-mti/logo.png') }}" alt="">
            </div>
          </div>
          <div class="col-sm-7 col-md-6 float-right d-none d-sm-block">
            <div class="wrap-form">
              <p>Dejanos tus datos en el formulario de
                contacto y te llamamos hoy mismo.</p>
                <form class="main-form" action="{{ route('mtisPost') }}" method="post" onsubmit="enviarFormulario('enviar2')">
                    @csrf
                  <div class="form-group">
                    <input type="text" name="nombre" class="form-control" placeholder="NOMBRE" required>
                  </div>

                  <div class="form-group">
                    <input type="text" name="apellido" class="form-control" placeholder="APELLIDO" required>
                  </div>

                  <div class="form-group">
                    <input type="text" name="lugar" class="form-control" placeholder="LUGAR DE TRABAJO O REPARTICIÓN" required>
                  </div>

                  <div class="form-group form-flex">
                    <span class="pr-1">(</span>
                      <input type="number" name="pref1" class="form-control pref" placeholder="PREF" required>
                    <span class="pr-2 pl-1">)</span>
                    <input type="number" name="telefono" class="form-control tel" placeholder="TELÉFONO" required>
                  </div>

                  <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="E-MAIL" required>
                  </div>

                  <div class="field-wrap">
                      <input type="text" name="utm_source" id="utm_source" value="{{ $utm_source }}" hidden="hidden">
                  </div>

                  <button id="enviar2" type="submit" class="btn btn-primary">QUIERO INFORMACIÓN</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Form Mobile-->
  <div class="container-fluid d-block d-sm-none form-mobile">
    <div class="row">
      <div class="col-12">
        <div class="wrap-form">
          <p>Dejanos tus datos en el formulario de
            contacto y te llamamos hoy mismo.</p>
            <form action="{{ route('mtisPost') }}" method="post" onsubmit="enviarFormulario('enviar')">
              @csrf
              <div class="form-group">
                <input type="text" class="form-control" placeholder="NOMBRE">
              </div>

              <div class="form-group">
                <input type="text" class="form-control" placeholder="APELLIDO">
              </div>

              <div class="form-group">
                <input type="text" class="form-control" placeholder="LUGAR DE TRABAJO O REPARTICIÓN">
              </div>

              <div class="form-group form-flex">
                <span class="pr-1">(</span>
                  <input type="tel" class="form-control pref" placeholder="PREF">
                <span class="pr-2 pl-1">)</span>
                <input type="tel" class="form-control tel" placeholder="TELÉFONO">
              </div>

              <div class="form-group">
                <input type="email" class="form-control" placeholder="E-MAIL">
              </div>

              <div class="field-wrap">
                  <input type="text" name="utm_source" id="utm_source" value="{{ $utm_source }}" hidden="hidden">
              </div>
              
              <button  id="enviar" type="submit" class="btn btn-primary">QUIERO INFORMACIÓN</button>
            </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Info plan-->
  <div class="bg-plan">
    <div class="container">
      <div class="row">
        <div class="col-12">
            <p>Accedé a un plan que <span>cubre al 100% todas las necesidades </span>que puedan surgir en <span>cualquier internación</span></p>
        </div>
      </div>
    </div>
  </div>

  <!-- Icons plan-->
  <div class="container p-4">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-1.png') }}">
          <p> <strong>Clínicas, Sanatorios y Profesionales </strong> de primer nivel</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-2.png') }}">
          <p><strong>Atención telefónica personalizada y online</strong></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-3.png') }}">
          <p><strong>Club de
            Beneficios MET</strong></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-4.png') }}">
          <p>App <strong>MET MOBILE </strong>para tu celular.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-5 offset-lg-1 col-md-6">
          <h6>COBERTURA EN INTERNACIONES</h6>
          <ul>
            <li>Clínicas</li>
            <li>Terapia intensiva intermedia</li>
            <li>UTI Neonatal/ Internación pediátrica</li>
            <li>Quirúrgicas</li>
            <li>Urgencias</li>
            <li>Controles obstétricos y estudios de tercer trimestre de embarazo</li>
            <li>Hemodinamia diagnóstica y terapéutica</li>
            <li>Cirugía cardiovascular con circulación extracorpórea</li>
            <li>Neurocirugía</li>
            <li>Hemodiálisis</li>
          </ul>
        </div>
        <div class="col-lg-5 offset-lg-1 col-md-6">
          <h6>PRESTADORES</h6>
          <ul>
            <li><strong>Sanatorio Allende (Nva. Cba.)</strong></li>
            <li><strong>Sanatorio Allende (Cerro)</strong></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="piecu" data-color="color"></div>
  </footer>
  <script type="text/javascript">
  $(document).ready(function () {
      $('#btn-submit-mti').on('click', function (e) {
          if($('#form-mti')){
              $('#btn-submit-mti').prop('disabled', true)
              $('#btn-submit-mti').html('SOLICITANDO INFORMACIÓN')
              $('#form-mti').submit()
          }
      })
      $('input').on('click', function() {
          console.log('enabled');
          $('#btn-submit-mti').prop('disabled', false)
          $('#btn-submit-mti').html('QUIERO INFORMACIÓN')
      })
   })
    </script>
  <body>
</html>
