<?php
namespace App\Clases;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class EmailTo{
	public $from;
	public $to;
	public $subject;
	public $isBodyHtml;
	public $body;

	public function __construct($xFrom = null, $xTo = null, $xSubject = null, $xIsBodyHtml = true, $xBody = null){
		try{
			$this->from = $xFrom;
			$this->to = $xTo;
			$this->subject = $xSubject;
			$this->isBodyHtml = $xIsBodyHtml;
			$this->body = $xBody;
		}
		catch(Exception $e){
			Log::writeLog($e);
		}
	}

	public function setEmailTo($eEmail = null,$xPerfil = null,$Consulta = null,$xEstuxPadre = null){
		try{
			if (!empty($eEmail)) {
				$oEmailTo = new EmailTo();				
				$oEmailTo->from = 'formularios@fidelitytools.net';
				$oEmailTo->isBodyHtml = true;
				$oEmailTo->subject = $Consulta. ' Proviene de ' . $xPerfil . ' - '.$xEstuxPadre;
				$oEmailTo->to = 'ezalazar@metmedicinaprivada.com,comercial@metmedicinaprivada.com,deivy@sawubona.com.ar';
				$fecha = date("Y-m-d"); 
				$body = 'body';
				$oEmailTo->body = $body;
				print_r($oEmailTo);die;
				return $oEmailTo;
			}
		}		
		catch(Exception $e){
			Log::writeLog($e);
		}
	}
}
?>