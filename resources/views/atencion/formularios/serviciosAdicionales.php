<form method="post" id="2939" onsubmit="enviarFormulario('enviar')">
<input type="hidden" name="idSegmento" value="2934">
    <ul class="list-group list_afil">
        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-dd" name="dnititular" placeholder="NÚMERO DE DOCUMENTO DEL TITULAR DE LA COBERTURA*" id="1249">
        </li>

        <li class="list-group-item input_afil_b">
            <div class="row">
                <div class="col-xs-6">
                    <label>FECHA DE NAC. DEL TITULAR</label> 
                </div>
                <div class="col-xs-6">
                    <input type="date" class="validar form-dd" name="fechanacimiento"  id="1264">
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b">
        	<div class="row">
                <div class="col-xs-6">
                	<label>SERVICIO ADICIONAL PARA</label>
                </div>
                <div class="col-xs-6">
                    <select id="btnAdicional" name="cambioplan" class="validar form-pc">
                        <option value="0">SELECCIONE</option>
                        <option value="4181">TODO EL GRUPO</option>
                        <option value="4182">INTEGRANTE</option>
                    </select>
                </div>
            </div>
        </li>

        <div id="contIntegrante">        
            <li class="list-group-item input_afil_b">
                <input type="text" class="validar form-dd" name="dnicambio" placeholder="NRO. DE DOCUMENTO DEL INTEGRANTE QUE CAMBIA DE PLAN*" id="1256">
            </li>
        </div>

        <li class="list-group-item input_afil_b">
        	<div class="row">
                <div class="col-xs-6">
                	<label>ADICIONAL</label>
                </div>
                <div class="col-xs-6">
                    <select id="btnPlanAdicional" name="adicional" class="validar form-pc">
                        <option value="0">SELECCIONE</option>
                        <option value="4187">MÉDICO A DOMICILIO</option>
                        <option value="4188">PLAN ODONTOLÓGICO</option>
                    </select>
            	</div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <div class="row">
                <div class="col-xs-4">
                    <input class="validarInt form-c" type="text" name="pref" placeholder="PREFIJO" id="CaractTelefono">                         
                </div>
                <div class="col-xs-8">
                    <input class="validarInt form-c" type="text" name="telefono" placeholder="TELÉFONO" id="NumberTelefono">                            
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarEmail form-c" placeholder="EMAIL*" name="email" id="E-mail">
        </li>

        <li class="list-group-item">
            <input id="enviar" type="submit" class="btnEnviar" value="ENVIAR">
        </li>
    </ul>
</form>