<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendContact.
 */
class SendCliente extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Request
     */
    public $request;

    /**
     * SendContact constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


    
	$subject = '';
 
	switch($this->request['idform']){
       
        case 'plancorporativo':
        
            $toCliente = array($this->request['email']);
            $viewCliente = 'mail.cliente';
            $SubjectCliente = 'Gracias por escribirnos';
        
        break;

        case 'contactoafiliado':
            $toCliente = array($this->request['email']);
            $viewCliente = 'mail.cliente';
            $SubjectCliente = 'Gracias por escribirnos';
        break;
    
    
    
    }

          $this->to($toCliente)
        ->view($viewCliente)
        ->subject($SubjectCliente)
        ->from($this->request->email, $this->request->nombre)
        ->replyTo($this->request->email, $this->request->nombre);


   
        
    }
}