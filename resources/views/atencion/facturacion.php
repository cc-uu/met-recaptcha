

<div class="container-fluid at_banner04">
	<div class="container sp">
   	  <div class="col-xs-5 cont_atencion">
        	<h1>DESCARGÁ TU FACTURA</h1>
            <p>Podés imprimir tu factura y pagarla en cualquier Rapipago o PagoFácil. 
Si no llega la factura a tu domicilio, no olvides realizar el reclamo a través de nuestros Centros de Atención al Cliente (CHAT 0800 3450 638) y nuestras oficinas de Independencia 714. </p>
            <a href="http://prestaciones.metcordoba.com.ar/AGC/" target="_blank">DESCARGAR FACTURA</a>
        </div>
    </div>
</div>

<div class="container-fluid at_banner05">
	<div class="container sp">
   	  <div class="col-xs-5 cont_atencion">
        	<h1>MODIFICACIÓN DE DATOS DE FACTURACIÓN</h1>
            <p>Para poder recibir antes tu factura, consultarla en cualquier momento y lugar con acceso a Internet, necesitamos contar con tu correo electrónico actualizado. Completa el formulario y podrás tener todos estas beneficios con un clik.</p>
            <a href="" data-toggle="modal" data-target="#modal_datos">MODIFICAR MIS DATOS</a>
        </div>
    </div>
</div>

<!-- Modal Datos personales -->
<div class="modal fade" id="modal_datos" tabindex="-1" role="dialog" aria-labelledby="modal_datosLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <iframe src="http://afiliados2.metmedicinaprivada.com/actualizacion/principal.asp" frameborder="0" width="650" height="800"></iframe>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid at_pie02">
	<div class="container sp">
    	<h1>MEDIOS DE PAGO</h1>
    	<div class="col-xs-3 sp cont_pie02">
        	<img src="{{ asset('/img/atencion/tarjetas.png') }}" width="215" height="140" alt=""/>
            <h2>TARJETAS DE CRÉDITO</h2>
			<p>Puede adherirse haciendo <a href="" data-toggle="modal" data-target="#modal_tarjeta">click aquí</a></p>
            <h2>CUENTA BANCARIA</h2>
            <p>Puede adherirse haciendo <a href="" data-toggle="modal" data-target="#modal_cuenta">click aquí</a></p>
        </div>
        
        <!-- Modal Cuenta Bancaria -->
        <div class="modal fade" id="modal_cuenta" tabindex="-1" role="dialog" aria-labelledby="modal_cuentaLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <iframe src="http://afiliados2.metmedicinaprivada.com/debito_cuenta.asp" frameborder="0" width="450" height="800"></iframe>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Modal Tarjeta de Crédito -->
        <div class="modal fade" id="modal_tarjeta" tabindex="-1" role="dialog" aria-labelledby="modal_tarjetaLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <iframe src="http://afiliados2.metmedicinaprivada.com/debito_tarjeta.asp" frameborder="0" width="450" height="900"></iframe>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-xs-3 cont_pie02">
        	<img src="{{ asset('/img/atencion/pagomc.png') }}" width="215" height="140" alt=""/>
            <h2>INTERNET</h2>
			<p>Nuestros Afiliados que posean la Caja de Ahorros y/o Cuenta Corriente adheridas a la red Banelco (<a href="http://www.pagomiscuentas.com.ar/" target="_blank">pagomiscuentas</a>) o Link (<a href="http://www.linkpagos.com.ar/" target="_blank">linkpagos</a>) pueden abonar su cuota las 24 horas, <!--todos los días del año, desde cualquier PC conectada a Internet,--> a través de su homebanking.</p>
        </div>
        <div class="col-xs-3 cont_pie02">
        	<img src="{{ asset('/img/atencion/banelco.png') }}" width="215" height="140" alt=""/>
            <h2>RED BANELCO</h2>
			<p>Puede abonar su factura ingresando desde cualquier cajero automático de la Red Banelco.</p>
            <h2>RED LINK</h2>
			<p>Puede abonar su factura ingresando desde cualquier cajero automático de la Red Link.</p>
        </div>
        <div class="col-xs-3 cont_pie02">
        	<img src="{{ asset('/img/atencion/descarga.png') }}" width="215" height="140" alt=""/>
            <h2>OTRAS FORMAS DE PAGO</h2>
			<p>Puede bajar su resumen de cuenta haciendo <a href="http://prestaciones.metcordoba.com.ar/AGC/" target="_blank">click aquí</a> y pagar en:<br>
            • PAGO FÁCIL<br>
            • RAPIPAGO<br>
            • OFICINAS COMERCIALES <br>
			Con tarjeta Naranja, Cordobesa, Mastercard, Visa, American Express, Visa Electrón, Maestro.</p>
        </div>
    </div>
</div>