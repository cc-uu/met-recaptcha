<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Send;
use App\Mail\SendCliente;
use App\Clases\Persona\Persona;
use App\Clases\EmailTo;
use App\Clases\Contenido\Contenido;
use App\Sawubona\Sawubona;


use File;
use DB;
use App\Clases\WebService\WebService;
/**
 * Class FormulariosController.
 */
class FormulariosController extends Controller
{
  
    public function index(Request $request)
    {
      $Contenidos = new Contenido(); 
      $limite = 10; // limite de resultados devueltos.
      $offset = 0; //Variable que indica desde que posicion devuelve los resultados.
      $ContenidoCorporativo = $Contenidos->getAll(9852,$offset,$limite,'idContenido','ASC');
      
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        return view('plancorporativo.index',compact('ContenidoCorporativo'));
      }

      $Persona = new Persona();
      $segmento = 7831;
      $request->cuit = (int) str_replace('-', '', $request->cuit);
      // $persona =  $Persona->getByDni($segmento, $request->cuit) !== null ? $Persona->getByDni($segmento, $request->cuit) : new Persona();
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoPlanC($request, $segmento);
      $envioemail =  $this->sendEmailsForm($request);

      if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      } 

      return view('plancorporativo.gracias',compact('ContenidoCorporativo'));
      
     
    }

    public function contactoAfiliado(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        $Contenidos = new Contenido(); 
        $limite = 10; // limite de resultados devueltos.
        $offset = 0; //Variable que indica desde que posicion devuelve los resultados.
        $ContenidoCorporativo = $Contenidos->getAll(9852,$offset,$limite,'idContenido','ASC');
        return view('plancorporativo.index',compact('ContenidoCorporativo'));
      }

      $Persona = new Persona();
      $segmento = 581;
      // $persona = $Persona->getByDni($segmento, $request->dni) !== null ? $Persona->getByDni($segmento, $request->dni) : new Persona();
      $guardia = 1413;
      $asginador = true;
      //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoCW($request, $segmento, $guardia);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);
      // Envio de email al cliente
      // $envioemailCliente =  new SendCliente($rquest));

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

     
      $Contenidos = new Contenido(); 
      $limite = 10; // limite de resultados devueltos.
      $offset = 0; //Variable que indica desde que posicion devuelve los resultados.

      $DireccionesMaps = $Contenidos->getAll(9853,$offset,$limite,'idContenido','ASC');
      return view('contacto.gracias',compact('DireccionesMaps'));
      
    }

    public function contactoNoAfiliado(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        $Contenidos = new Contenido(); 
        $limite = 10; // limite de resultados devueltos.
        $offset = 0; //Variable que indica desde que posicion devuelve los resultados.
        $ContenidoCorporativo = $Contenidos->getAll(9852,$offset,$limite,'idContenido','ASC');
        return view('plancorporativo.index',compact('ContenidoCorporativo'));
      }

      $Persona = new Persona();
      $segmento = 3681;
      $guardia = 1413;
      $asginador = true;
       //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoGC($request, $segmento, $guardia, $asginador);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      $Contenidos = new Contenido(); 
      $limite = 10; // limite de resultados devueltos.
      $offset = 0; //Variable que indica desde que posicion devuelve los resultados.

      $DireccionesMaps = $Contenidos->getAll(9853,$offset,$limite,'idContenido','ASC');
      return view('contacto.gracias',compact('DireccionesMaps'));
    }

    public function contactoDebitoBanco(Request $request)
    {
      $Persona = new Persona();
      $segmento = 819;
       //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoDebitos($request,$segmento);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);
    
     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

    $Contenidos = new Contenido(); 
    $limite = 10; // limite de resultados devueltos.
    $offset = 0; //Variable que indica desde que posicion devuelve los resultados.

    $DireccionesMaps = $Contenidos->getAll(9853,$offset,$limite,'idContenido','ASC');
    return view('contacto.gracias',compact('DireccionesMaps'));

      
    }

    public function contactoDebitoTarjeta(Request $request)
    {

         $Persona = new Persona();
      $segmento = 819;
       //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoDebitos($request,$segmento);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      return redirect()->route('gestiongracias', ['pagina' => 'Adhesión al débito']);

      
    }

    public function contactoCambioPlan(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        $Contenidos = new Contenido(); 
        $CambioPlan = $Contenidos->getById(9854,48253);
        return view('gestiononline.cambiodeplan',compact('CambioPlan'));
      }

      $Persona = new Persona();
      $segmento = 2934;
      //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoCambioPlan($request,$segmento);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      return redirect()->route('gestiongracias', ['pagina' => 'Cambio de Plan']);
    }

    
    public function contactoContratacionSA(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        $Contenidos = new Contenido(); 
		    $ServiciosAdicionales = $Contenidos->getById(9854,48254);
		    return view('gestiononline.contrataciondeserviciosadicionales',compact('ServiciosAdicionales'));
      }

         $Persona = new Persona();
      $segmento = 2939;
       //  Guardar persona en el segmento
      $ConsultaNueva = $Persona->AgregaPersonaBySegmentoServiciosA($request,$segmento);
      // Envio de email a MET
      $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      return redirect()->route('gestiongracias', ['pagina' => 'Contratación de servicios adicionales']);

      
    }

        
    public function contactoBuscarAfiliadoByDni($dni)
    {
      //Busca afiliado por dni para cambiar datos de factura
      $Persona = new Persona();
      $segmento = 4627;
      $ConsultaAfiliado = $Persona->getByDni($segmento,$dni);

     return  response()->json($ConsultaAfiliado);
  
    }

    public function contactoBuscarAfiliadoByWS($tipo,$dni)
    {
      // busca afiliado pot itpo de documento y dni en el web service de met
      $webService = new WebService();
        //  $Persona = new Persona();
     
      $ConsultaAfiliado = $webService->getAfiliado($tipo,$dni);

    return  response()->json($ConsultaAfiliado);
    
    }


    public function contactoDardeBaja(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        $Contenidos = new Contenido(); 
		    $Baja = $Contenidos->getById(9854,48258);
		    return view('gestiononline.dardebaja',compact('Baja'));
      }

      $Persona = new Persona();
     $segmento = 7830;
           //  Guardar persona en el segmento
     $ConsultaNueva = $Persona->AgregaPersonaBySegmentoBJ($request,$segmento);
     $envioemail =  $this->sendEmailsForm($request);
     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      return redirect()->route('gestiongracias', ['pagina' => 'Dar de baja']);

      
    }

    public function contactoModificiarDatos(Request $request)
    {

      $Persona = new Persona();
     $segmento = 4627;
      //  Guardar persona en el segmento
     $ModificarDatos = $Persona->AgregaPersonaBySegmentoModDatos($request,$segmento);
    //   dd($ModificarDatos);
     $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      
      return view('mail.index',compact('enviarMail'));

      
    }

    public function contactoTrabajaEnMet(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        return view('trabajaenmet.index');
      }

      $Persona = new Persona();
      $segmento = 582;
      //  Guardar persona en el segmento
      $TrabajaenMet = $Persona->AgregaPersonaBySegmentoTrabajaMet($request,$segmento);
      //  Compruebo si trae archivo y si trae lo guardo en la carpeta storage para poder enviarlo por email
      if ($request->file('archivo')) {
              $CV = $request->file('archivo');
              $NombreArchivo = $CV->getClientOriginalName();
              $path = 'CV/'.$NombreArchivo;
              \Storage::disk('public')->put($path,  \File::get($CV));
      }

      // Comprruebo los puntos de interes y cambio el valor para el envio de email
      $nuevosValores = [];
      foreach ($request['checkPI'] as $value) {
       
        switch ($value) {
          case '40076':
            array_push($nuevosValores,'CALIDAD');
            break;
          case '40077':
            array_push($nuevosValores,'MARKETING');
            break;
          case '40078':
            array_push($nuevosValores, 'RECURSOS HUMANOS');
            break;
          case '40079':
            array_push($nuevosValores,'SISTEMAS');
            break;
          case '40080':
            array_push($nuevosValores,'VENTAS');
            break;
          case '40081':
            array_push($nuevosValores, 'AT. AL CLIENTE');
            break;
          case '40082':
            array_push($nuevosValores, 'CONTROL DE GESTIÓN');
            break;
          case '40083':
            array_push($nuevosValores,  'PRESTACIONES MÉDICAS');
            break;
          case '40085':
            array_push($nuevosValores, 'ADMINISTRACIÓN');
            break;
        
        }
      }
      // Comprruebo los Horarios de interes y cambio el valor para el envio de email
      $nuevosValoresHorarios = [];
      foreach ($request['checkP'] as $value) {
       
        switch ($value) {
          case '40086':
            array_push($nuevosValoresHorarios,'FULL TIME');
            break;
          case '40087':
            array_push($nuevosValoresHorarios,'POR LA MAÑANA');
            break;
          case '40088':
            array_push($nuevosValoresHorarios, 'POR LA TARDE');
            break;

        
        }
      }
      $request['checkP'] = $nuevosValoresHorarios;
      $request['checkPI'] = $nuevosValores;
      $envioemail =  $this->sendEmailsForm($request);

     if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }

      return view('trabajaenmet.gracias');
    }
    
    public function contactoPrestador(Request $request)
    {
      $response = Sawubona::getCaptcha($_POST);
      if (!$response) {  
        return view('prestadores.index');
      }

      $Persona = new Persona();
      $segmento = 1237;
      //  Guardar persona en el segmento
      $Prestador = $Persona->AgregaPersonaBySegmentoPrestadores($request,$segmento);
      //  Compruebo si trae archivo y si trae lo guardo en la carpeta storage para poder enviarlo por email
     if ($request->file('archivo')) {
        $CV = $request->file('archivo');
        $NombreArchivo = $CV->getClientOriginalName();
        $path = 'CV/'.$NombreArchivo;
        \Storage::disk('public')->put($path,  \File::get($CV));
      }
      $envioemail =  $this->sendEmailsForm($request);

      if ($envioemail) {
        $enviarMail = 'SU CONSULTA SE ENVIO CORRECTAMENTE. GRACIAS.';
      } else {
        $enviarMail = 'HA OCURRIDO UN ERROR. INTENTELO MAS TARDE';
      }
      
      return view('prestadores.gracias');
    }


    public function sendEmailsForm(Request $request)
    {
      $subject = '';
      //  Verifico el id del formulario solicitado y segun el id envio el email con asunto y direccion correspondientes
      switch($request['idform']){
           
            case 'plancorporativo':
              
                $subject = $request['cuit'].' - ' .'Plan Corporativo';
                $view = 'mail.fidelity';
                $to = 'ezalazar@metmedicinaprivada.com, jtissera@metmedicinaprivada.com';
            break;

            case 'contactoafiliado':
               
                $subject = $request['dni'].' - ' .'Contacto Afiliado';
                $view = 'mail.fidelity';
                $to = 'atencionalciente@metmedicinaprivada.com';
            break;
            case 'contactonoafiliado':
               
                $subject = $request['dni'].' - ' .'Contacto No Afiliado';
                $view = 'mail.fidelity';
                $to = 'comercial@metmedicinaprivada.com';
            break;

            case 'debitocuentabanco':
               
                $subject = $request['dnicobertura'].' - ' .'Debito Banco';
                $view = 'mail.fidelity';
                $to = 'atencionalcliente@metmedicinaprivada.com';
            break;
            case 'debitocuentatarjeta':
               
                $subject = $request['dnicobertura'].' - ' .'Debito Tarjeta' ;
                $view = 'mail.fidelity';
                $to = 'atencionalcliente@metmedicinaprivada.com';
            break;

            case 'cambiodeplan':
               
                $subject = $request['dnititularcobertura'].' - ' .'CAMBIO DE PLAN';
                $view = 'mail.fidelity';
                $to = 'atencionalcliente@metmedicinaprivada.com';
            break;

            case 'contratacionserviciosa':
               
                $subject =  $request['dni'].' - '.'Contratación de servicios adicionales';
                $view = 'mail.fidelity';
                $to = 'atencionalcliente@metmedicinaprivada.com';
            break;

            case 'dardebaja':
               
                $subject = $request['dni'].' - '.'SOLICITUD DE BAJA';
                $view = 'mail.fidelity';
                $to = 'atencionalcliente@metmedicinaprivada.com';
            break;
            case 'trabajaenmet':
               
                $subject = 'Trabaja en Met';
                $view = 'mail.fidelity';
                $to = 'reclutamiento@metmedicinaprivada.com';
               
            break;
            case 'prestador':
               
                $subject = 'Queres ser prestador';
                $view = 'mail.fidelity';
                $to = 'croldan@metmedicinaprivada.com, lmahl@metmedicinaprivada.com';
            break;
        
        
        }

        $oEmailTo = new EmailTo();

        $oEmailTo->from = 'metmedicinaprivada@fidelitytools.net';

        $oEmailTo->isBodyHtml = true;

        $oEmailTo->subject = $subject;
        
        //para testeo
        // print_r($to);
        // $to .= ', julieta@sawubona.com.ar , paloma@sawubona.com.ar';
        // $to = 'cristian@sawubona.com.ar';
        
        $oEmailTo->to = $to;
        $fecha = date("Y-m-d");

        $body = view('mail.fidelity',compact('request'))->render();

        $oEmailTo->body = $body;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apirest.fidelitytools.net/api/Soporte?xKey=265c092ff1813072ffeb07ec2ab84e4d",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n  \"body\": \"$oEmailTo->body\",
                                    \r\n  \"from\": \"$oEmailTo->from\",
                                    \r\n  \"isBodyHtml\": true,
                                    \r\n  \"subject\": \"$oEmailTo->subject\",
                                    \r\n  \"to\": \"$oEmailTo->to\"\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Postman-Token: 3dd02948-6597-4188-933b-995022f0b9c1",
                "cache-control: no-cache",
            ),
        ));  

        $respuesta = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        return $respuesta;
        
    }

    public function getCaptcha($post)
    {
      $captcha_response = true;
      $recaptcha = $post['g-recaptcha-response'];
      $url = 'https://www.google.com/recaptcha/api/siteverify';
      $data = array(
        'secret' => '6Ld0goQaAAAAAOKLWcN96xoLwRrBF0RovMbONJAX',
        'response' => $recaptcha
      );
      $options = array(
        'http' => array (
          'method' => 'POST',
          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
          'content' => http_build_query($data)
        )
      );
      
      $context  = stream_context_create($options);
      $verify = file_get_contents($url, false, $context);
      $captcha_success = json_decode($verify);
      $captcha_response = $captcha_success->success;

      if ($captcha_response) {  
        return true;
      } else {
        return false;
      }
    }
}
