<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Persona\Persona;
use App\Clases\Persona\PersonaWS;
use Illuminate\Support\Facades\Input;

class GestiononlineController extends Controller
{
	
	public function index() {
		date_default_timezone_set('America/Argentina/Buenos_Aires'); 
		$Contenidos = new Contenido(); 
		$AdhesionDebito = $Contenidos->getById(9854,48251);
		
		$fecha = date('d-m-Y H:i:s');  
		$mes = date('F', strtotime($fecha."+ 1 month"));
		$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  		$meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  		$nombreMes = str_replace($meses_EN, $meses_ES, $mes);
		
		return view('gestiononline.index',compact('AdhesionDebito','nombreMes'));
	}

	public function cambiodeplan() {

		$Contenidos = new Contenido(); 
		$CambioPlan = $Contenidos->getById(9854,48253);
	
		return view('gestiononline.cambiodeplan',compact('CambioPlan'));
	}

	public function contrataciondeserviciosadicionales() {

		$Contenidos = new Contenido(); 
		$ServiciosAdicionales = $Contenidos->getById(9854,48254);
	
		return view('gestiononline.contrataciondeserviciosadicionales',compact('ServiciosAdicionales'));
	}

	public function descargatufactura() {

		$Contenidos = new Contenido(); 
		$DescargaFactura = $Contenidos->getById(9854,48255);
		return view('gestiononline.descargatufactura',compact('DescargaFactura'));
	}

	public function descargatucredencial() {
		$Contenidos = new Contenido(); 
		$DescargaCredencial = $Contenidos->getById(9854,48256);
	
		return view('gestiononline.descargatucredencial',compact('DescargaCredencial'));
	}

	public function modificaciondedatosdefactura() {
		$Contenidos = new Contenido(); 
		$ModFactura = $Contenidos->getById(9854,48257);
		return view('gestiononline.modificaciondedatosdefactura',compact('ModFactura'));
	}

	public function dardebaja() {
		$Contenidos = new Contenido(); 
		$Baja = $Contenidos->getById(9854,48258);
		return view('gestiononline.dardebaja',compact('Baja'));
	}

	public function formulariosonline() {
		$Contenidos = new Contenido(); 
		$Formularios = $Contenidos->getAll(3282);
	
		return view('gestiononline.formulariosonline',compact('Formularios'));
	}

	public function mediosdepago() {
		$Contenidos = new Contenido(); 
		$mediosDepago = $Contenidos->getAll(9919,0,15,'orden','ASC');
		$leyenda = $Contenidos->getById(9854,49183);
		
		return view('gestiononline.mediosdepago',compact('mediosDepago','leyenda'));
	}

	public function autorizaciones(Request $request) {
		if ($request->isMethod('post')) {
			$xPrestador =$request->input('prestador');
			$xNombre 	=$request->input('nombre');
			$xApellido 	=$request->input('apellido');
			$xDni 		=$request->input('dni');
			$xEmail 	=$request->input('email');
			$xTurno 	=$request->input('turno');

			if (! $xEmail 
				|| ! $xNombre
				|| ! $xApellido
				|| ! $xDni
				|| ! $xPrestador
			) {
				echo '<script language="javascript">alert("Por favor complete todos los campos");</script>';
				return view('referidosafiliados.index');
			}

			$oPersona = New Persona();
			$xIdSegmentoAutorizaciones = 7921;
			$Persona = $oPersona->getByDni($xIdSegmentoAutorizaciones, $xDni);
			if (!($Persona instanceof Persona)) {
				$Persona = new Persona();
				$Persona->idPersona = 0;
				$Persona->nombre = $xNombre;
				$Persona->apellido = $xApellido;
				$Persona->dni = $xDni;
			}

			$xPerfile = 'Referidos_Afiliados';
			$xPerfilExP = NULL;
			
			// Datos personales
			$xDatosPersonales = []; 
			$xPrestadorArr 	= ['idDatoDuro' => 5535,
			 				'valor' 	=> $xPrestador];
			$xTurnoArr = ['idDatoDuro' => 55536,
			 				'valor' 	=> $xTurno];

			array_push($xDatosPersonales, $xPrestadorArr);
			array_push($xDatosPersonales, $xTurno);

			$oPersonaWS = PersonaWS::cast($oPersona);

			//Segmento correspondiente
			$oPersonaWS->segmento = ['idSegmento' => 'NzkyMQ'];

			if (!is_null($xDatosPersonales)) {
				$oPersonaWS->camposPersonales = $this->convertircamposPersonales($xDatosPersonales);
			}

			$xUrl = 'https://ws.fidelitytools.net/WebService_Fidelitytools.asmx/wsSetPersonas';
			$postData = json_encode(
				[
					'key' => '5d279adf59c07c099c16061c',
					'json' => json_encode([$oPersonaWS]),
				]
			);

			if (config('main.DEVMODE')) {
				echo '<pre>';
				return print_r(json_decode($postData));
			}

			$ch = curl_init($xUrl);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json; charset=utf-8',
				'Content-Length: ' . strlen($postData)]
			);

			$response = curl_exec($ch);
			// echo '<pre>';
			// print_r($response);die;
			$chError = curl_error($ch);
			curl_close($ch);

				//enviar mail a traves de la api
			if ($chError) {
				echo 'Error: ';
				print_r($chError);
			} else {
				$oEmailTo = new EmailTo();

				$oEmailTo->from = 'metmedicinaprivada@fidelitytools.net';

				$oEmailTo->isBodyHtml = true;

				$oEmailTo->subject = 'Nueva Autorizacion en Gestor Comercial';
				$oEmailTo->to = 'autorizaciones@metmedicinaprivada.com';
				$fecha = date("Y-m-d");

				if (isset($oPersona->dni->numero)) {
					$eDni = $oPersona->dni->numero;
				} elseif (isset($oPersona->dni)) {
					$eDni = $oPersona->dni;
				}
				$body =
					"<table style='width:650; border:0; align:center; cellpadding:0; cellspacing:0;'>
					<tr>
						<td colspan='3'>
							<img src='http://app.fidelitytools.net/images/alertas/topFidelity.jpg'>
						</td>
					</tr>
					<tr>
						<td width='50'>&nbsp;</td>
						<td width='550'>&nbsp;</td>
						<td width='50'>&nbsp;</td>
					</tr>
					<tr>
						<td width='50'>&nbsp;</td>
						<td width='550' style='font-family:Arial, sans-serif; font-size:14px; line-height:20px; color:#000;'>
							<p><strong>FECHA:</strong> $fecha <br>
							<strong>NOMBRE:</strong>$oPersona->nombre<br>
							<strong>APELLIDO:</strong>$oPersona->apellido<br>
							<strong>DNI:</strong>$eDni <br>
							<strong>EMAIL:</strong><a href='mailto:$oPersona->email' target='_blank'>$oPersona->email</a> <br>
						</td>
					<td width='50'>&nbsp;</td>
					</tr>
					<tr>
						<td width='50'>&nbsp;</td>
						<td width='550'>&nbsp;</td>
						<td width='50'>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='3'>
							<img src='http://app.fidelitytools.net/images/alertas/pieFidelity.jpg'>
						</td>
					</tr>
				</table>";
				$oEmailTo->body = $body;
				//print_r($oEmailTo);die;
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://apirest.fidelitytools.net/api/Soporte?xKey=265c092ff1813072ffeb07ec2ab84e4d",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "{\r\n  \"body\": \"$oEmailTo->body\",
					  						\r\n  \"from\": \"$oEmailTo->from\",
					  						\r\n  \"isBodyHtml\": true,
					  						\r\n  \"subject\": \"$oEmailTo->subject\",
					  						\r\n  \"to\": \"$oEmailTo->to\"\r\n}",
					CURLOPT_HTTPHEADER => array(
						"Content-Type: application/json",
						"Postman-Token: 3dd02948-6597-4188-933b-995022f0b9c1",
						"cache-control: no-cache",
					),
				));

				$respuesta = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
			}

			if ($response) {
				$xMensaje = 'La consulta se ha enviado correctamente.';
			} else {
				$xMensaje = 'Ha sucedido un error.Intentelo mas tarde';
			}

			$Parametros = ['xMensaje'	=> $xMensaje];

			return view('gestiononline.autorizaciones', compact('xMensaje'));
		}

		$Contenidos = new Contenido(); 
		$Autorizaciones = $Contenidos->getById(9854,49922);
	
		return view('gestiononline.autorizaciones',compact('Autorizaciones'));
	}

	/*
	/ Convertidor de arreglos a datos personales
	 */
	private function convertircamposPersonales($arrDatos) {
		$accumulator = [];

		if (!is_array($arrDatos) || !count($arrDatos)) {
			return $accumulator;
		}
		foreach ($arrDatos as $arrDato) {
			$temp = [];
			$temp['idCampoPersonal'] = $arrDato['idDatoDuro'];
			$temp['valor'] = $arrDato['valor'];
			$accumulator[] = $temp;
		}

		return $accumulator;
	}

	public function credencial() {
		return view('gestiononline.credencial');
	}
}