@extends('layouts.app')
@section('title', ' | Gestión Online - Contratación de servicios adicionales')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.go(-1)">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Contratación de servicios adicionales</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a class="active">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}s">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>{{$ServiciosAdicionales->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-md-10 col-lg-5 intro">
                            <h3>{{$ServiciosAdicionales->bajada}}</h3>  
                            {!!$ServiciosAdicionales->descripcion !!}             
                        </div>
                        <div class="col-12 col-md-10 col-lg-7 formulario">
                            <form class="vertical" action=" {{ route('fomulario.enviar.contratacionserviciosa') }}"  method="post" onsubmit="enviarFormulario('enviar')"> 
                                @csrf
                                <input type="hidden" name="idform" value="contratacionserviciosa">
                                <div class="form-group">
                                    <label>Número de Documento del Titular de la Cobertura *</label>
                                    <input type="text" name="dni" maxlength="8" id="dni" required>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de nacimiento del Titular *</label>
                                    <input type="date" name="fechanacimiento" id="fechanacimiento" required>
                                </div>
                                <div class="form-group">
                                    <label>Servicio adicional para *</label>
                                    <select id="tipoPlan" name="servicioA">
                                        <option value="0">SELECCIONE</option>
                                        <option value="4181">TODO EL GRUPO</option>
                                        <option value="4182">INTEGRANTE</option>
                                    </select>
                                </div>                                
                                <div class="form-group" id="dniintegranteform">
                                    <label>Número de Documento del integrante que contrata el nuevo servicio *</label>
                                    <input type="text" maxlength="8" name="dninuevointegrante" id="dninuevointegrante">
                                </div>
                                <div class="form-group">
                                    <label>Adicional *</label>
                                    <select id="btnPlanAdicional" name="adicional" required>
                                        <option value="0">SELECCIONE</option>
                                        <option value="4187">MÉDICO A DOMICILIO</option>
                                        <option value="4188">PLAN ODONTOLÓGICO</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Número de Teléfono *</label>
                                    <div>
                                        <input type="text" name="prefijo" id="prefijo" class="prefijo" required>
                                        <input type="text" name="telefono" id="telefono" class="telefono" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>E-mail *</label>
                                    <input type="email" name="email" id="email">
                                </div>
                                <h3 class="leyenda">*Datos obligatorios</h3>
                                <div class="form-group">
                                    <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                                </div>
                                    <button  id="enviar" type="submit">ENVIAR</button>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection