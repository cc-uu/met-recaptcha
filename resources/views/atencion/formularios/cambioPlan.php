<form method="post" id="2934" onsubmit="enviarFormulario('enviar')">
    
<input type="hidden" name="idSegmento" value="2934">
    <ul class="list-group list_afil">
        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-dd" name="dnititular" placeholder="NÚMERO DE DOCUMENTO DEL TITULAR DE LA COBERTURA*" id="1249">
        </li>

        <li class="list-group-item input_afil_b">
            <div class="row">
                <div class="col-xs-6">
                    <label>FECHA DE NAC. DEL TITULAR</label> 
                </div>
                <div class="col-xs-6">
                    <input type="date" class="validar form-dd" name="fechanacimiento" id="1264">
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <div class="row">
                <div class="col-xs-6">
                    <label>CAMBIO DE PLAN PARA: </label>
                </div>
                <div class="col-xs-6">
                    <select id="tipoPlan" name="cambioplan" class="validar form-pc">
                        <option value="0">SELECCIONE</option>
                        <option value="4181">TODO EL GRUPO</option>
                        <option value="4182">INTEGRANTE</option>
                    </select>
                </div>
            </div>
        </li>
        
        <div id="planIntegrante">        
            <li class="list-group-item input_afil_b">
                <input type="text" class="validar form-dd" name="dnicambio" placeholder="NRO. DE DOCUMENTO DEL INTEGRANTE QUE CAMBIA DE PLAN*" id="1256">
            </li>
        </div>

        <!-- Para grupo -->
        <li class="list-group-item input_afil_b" >
            <div class="row">
                <div class="col-xs-6">
                    <label>NUEVO PLAN: </label>
                </div>
                <div class="col-xs-6">
                    <select class="validar form-pc" name="nuevoplan">
                        <option value="0">SELECCIONE</option>
                        <option value="4178">MTA+</option>
                        <option value="4179">MTB+</option>
                        <option value="4180">MTC+</option>
                    </select>
                </div>
            </div>
        </li>

        <!--<li class="list-group-item input_afil_b" >
            <label>DOMICILIO DE ENTREGA DE NUEVA CREDENCIAL</label>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="CALLE*" name="direccion" id="Direccion">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-c" name="direccionNumero" placeholder="NÚMERO*" id="Numero">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="BARRIO*" name="barrio" id="Barrio">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar" placeholder="LOCALIDAD*" name="provincia" id="Localidad">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-c" placeholder="C.P.*" name="postal" id="CPostal">
        </li>-->

        <li class="list-group-item input_afil_b" >
            <div class="row">
                <div class="col-xs-4">
                    <input class="validarInt form-c" type="text" placeholder="PREFIJO" name="pref" id="CaractTelefono">                         
                </div>
                <div class="col-xs-8">
                    <input class="validarInt form-c" type="text" placeholder="TELÉFONO" name="telefono" id="NumberTelefono">                            
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarEmail form-c" placeholder="EMAIL*" name="email" id="E-mail">
        </li>

        <li class="list-group-item" >
            <textarea type="textarea" class="contact form-c" placeholder="MOTIVO" name="motivo" id="motivo"></textarea>
        </li>

        <li class="list-group-item">
            <input id="enviar" type="submit" class="btnEnviar" value="ENVIAR">
        </li>
    </ul>
</form>