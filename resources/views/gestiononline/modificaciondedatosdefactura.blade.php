@extends('layouts.app')
@section('title', ' | Gestión Online - Modificación de datos de factura')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 left menu-gestiones">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a class="active">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>{{$ModFactura->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-lg-5 intro">  
                            <h3>{{$ModFactura->bajada}}</h3>                            
                            {!!$ModFactura->descripcion!!}
                        </div>
                        <div class="col-12 col-lg-7 formulario">
                            <form>
                                <div class="form-group">
                                    <label>Tipo de documento *</label>
                                    <select name="tipoDni" ng-model="tipoDoc">
                                        <option class="text-uppercase" value="0">SELECCIONE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="1" class="ng-binding ng-scope">BB</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="2" class="ng-binding ng-scope">C.F.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="3" class="ng-binding ng-scope">C.I.POLICIA FED</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="4" class="ng-binding ng-scope">CUIT</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="5" class="ng-binding ng-scope">D.N.I.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="6" class="ng-binding ng-scope">L.C.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="7" class="ng-binding ng-scope">L.E.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="8" class="ng-binding ng-scope">MATRICULA</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="9" class="ng-binding ng-scope">NRO. CLIENTE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="10" class="ng-binding ng-scope">PASAPORTE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="11" class="ng-binding ng-scope">CUIL</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="12" class="ng-binding ng-scope">S.D.</option>                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Número de documento *</label>
                                    <input type="text" name="dni" id="dni" maxlength="8" required>
                                </div>
                                
                                <h3 class="leyenda">*datos obligatorios</h3>
                                <button type="button" onclick="buscarAfiliadoByDniFactura()">BUSCAR</button>
                            </form>                              

                            <!-- Resultado -->
                            <form  action=" {{ route('fomulario.enviar.modificardatos') }}"  method="post" onsubmit="enviarFormulario('enviar')">
                                @csrf
                                <input type="hidden" name="idform" value="modificardatos">
                                {{-- <div class="alert alert-success" role="alert">
                                    Estás adherido a la factura digital, la recibirás en tu email registrado. Si deseás modificar tus datos, completá el siguiente formulario.
                                </div> --}}
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="nombre" id="nombre" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" name="apellido" id="apellido" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>DNI</label>
                                    <input type="text" name="dnia" id="dnia" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" id="email" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" name="telefono" id="telefono" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Móvil</label>
                                    <input type="text" name="celular" id="celular" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Sexo</label>
                                    <select name="sexo" id="sexo">
                                        <option value="3" selected="">Seleccione</option>
                                        <option value="1">Hombre</option>
                                        <option value="0">Mujer</option>
                                    </select>
                                </div>                                
                                <h3 class="leyenda">*datos obligatorios</h3>
                                    <button  id="enviar" type="submit">ENVIAR</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection