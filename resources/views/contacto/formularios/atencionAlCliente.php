<form method="post" id="581" onsubmit="enviarFormulario('btnEnviar')">
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b">
				<input class="validar form-c" type="text" name="nombre" placeholder="NOMBRE*" id="nombre">
			</li>

			<li class="list-group-item input_afil_b">
				<input class="validar form-c" type="text" name="apellido" placeholder="APELLIDO*" id="apellido">
            </li>

            <input type="hidden" name="idSegmento" value="581">
            <!-- <input type="hidden" name="asignadorAutomatico" value="1"> -->
            <input type="hidden" name="origenWeb" value="1">
            <input type="hidden" class="form-dd" name="planConsulta" id="1272" value="<?php if (isset($xPlan)) {
	echo $xPlan;
}
?>">

			<li class="list-group-item input_afil_b">
				<input class="validar validarEmail form-c" type="text"  name="email" placeholder="EMAIL*" id="E-mail">
			</li>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b">
				<input class="validar validarInt validarDoc form-c" name="dni" type="text" placeholder="DNI*" id="Dni">
			</li>

			<li class="list-group-item input_afil_b">
				<div class="row">
					<div class="col-xs-4">
						<input class="validar validarInt form-c" type="text" name="pref3" placeholder="PREFIJO" id="CaractTelefono">
					</div>
					<div class="col-xs-8">
						<input class="validar validarInt form-c" type="text" name="movil" placeholder="TELÉFONO" id="NumberTelefono">
					</div>
				</div>
			</li>
			<span class="data_oblig">* datos obligatorios</span>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item">
				<textarea class="contact validar form-c"  name="mensaje" placeholder="MENSAJE" id="Comentarios"></textarea>
			</li>
			<li class="list-group-item">
				<input type="submit" value="ENVIAR" id="btnEnviar">
			</li>
		</ul>
	</div>
</form>