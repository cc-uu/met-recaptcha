
<img src="{{ asset('/img/atencion/bg-credencial.jpg" class="bg-credencial">
<div class="container-fluid at_bannerCredencial no-imprimir">
    <div class="container sp">
        <div class="col-xs-5 cont_atencion">
            <h1>CREDENCIAL ONLINE</h1>
            <p><!--<strong>IMPRIMÍ TU CREDENCIAL!</strong><br>-->
                Completá tus datos y obtené tu credencial online aquí. Este documento será válido acompañado con el DNI del titular.</p>
                <a href="" data-toggle="modal" data-target="#modal_credencial">OBTENER CREDENCIAL</a>
        </div>
    </div>
</div>

<div class="container-fluid at_banner01 no-imprimir">
    <div class="container sp">
        <div class="col-xs-5 cont_atencion">
            <h1>ADHESIÓN AL DÉBITO</h1>
            <p><strong>¡NO PIERDAS TIEMPO!</strong><br>
                Con tu adhesión al débito automático por cuenta bancaria o tarjeta de crédito evitás trasladarte al punto de cobranza, realizar colas y movilizarte con efectivo. Para adherirte al débito:</p>
                <a href="" data-toggle="modal" data-target="#modal_cuenta">POR CUENTA BANCARIA</a>
                <a href="" data-toggle="modal" data-target="#modal_tarjeta">POR TARJETA DE CRÉDITO</a>
        </div>
    </div>
</div>

<div class="container-fluid at_banner02 no-imprimir">
    <div class="container sp">
        <div class="col-xs-5 cont_atencion">
            <h1>CAMBIO DE PLAN</h1>
            <p><strong>Solicitá online tu cambio de plan</strong><br>
                <!--En Met contamos con opciones para cada necesidad y para cada tipo de afiliado. Si necesitás realizar un cambio de plan, elegí primero el que mejor te satisfaga y envianos el formulario completo para iniciar el trámite.--></p>
                <a href="" data-toggle="modal" data-target="#mCambioPlan">FORMULARIO</a>
                <a href="{{ asset('/planes/comparativas') }}">COMPARATIVA DE PLANES</a>
        </div>
    </div>
</div>

<div class="container-fluid at_banner03 no-imprimir">
    <div class="container sp">
        <div class="col-xs-5 cont_atencion">
            <h1>CONTRATACIÓN DE SERVICIOS ADICIONALES</h1>
            <p><strong>Tu plan, más personalizado</strong><br>
            Si estás conforme con tu plan pero necesitás más prestaciones, podés contratar servicios adicionales al mismo. Completá el formulario con tus preferencias y envialo para ajustar tu cobertura. </p>
            <a href="" data-toggle="modal" data-target="#mServAdicionales">FORMULARIO</a>
        </div>
    </div>
</div>

<div class="container-fluid at_pie no-imprimir">
    <div class="container sp">
        <!--<a href="" data-toggle="modal" data-target="#mCredencial">SOLICITUD DE CREDENCIAL</a>-->
        <a href="" data-toggle="modal" data-target="#modal_datos">MODIFICACIÓN DE DATOS PERSONALES</a>
        <a href="" data-toggle="modal" data-target="#mCCImpositiva">CAMBIO DE CONDICIÓN IMPOSITIVA</a>
    </div>
</div>

<!-- Modales .......................................................................................................................................................   -->

<!-- Credencial Provincial -->
<div ng-controller="CredencialController" ng-app="CartillaMedica" ng-init="ready()">
    <div class="modal fade no-imprimir" id="modal_credencial" tabindex="-1" role="dialog" aria-labelledby="modal_cuentaLabel" aria-hidden="true">
        <div class="modal-dialog no-imprimir" style="width: 475px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Credencial Online</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <form class="col-xs-12">
                                <ul class="list-group list_afil">
                                     <li class="list-group-item input_afil_b">
                                        <select name="tipoDni" class="form-control text-uppercase text-center" ng-model="tipoDoc">
                                            <option class="text-uppercase" value="0">TIPO DE DOCUMENTO</option>
                                            <option ng-repeat="tp in tiposDocumentos" value="{{tp.idTipoDocumento}}">{{tp.numero}}</option>
                                        </select>
                                    </li>

                                    <li class="list-group-item input_afil_b">
                                        <input name="txtDni" type="text" class="form-control only-numbers text-left" ng-model="nroDoc" placeholder="Número de Documento">
                                    </li>

                                    <li class="list-group-item">
                                        <input type="button" value="Buscar" class="form-control" ng-click="getCredencial()">
                                    </li>
                                </ul>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="imprimir">
    </div>
</div>

<!-- Cuenta Bancaria -->
<div class="modal fade no-imprimir" id="modal_cuenta" tabindex="-1" role="dialog" aria-labelledby="modal_cuentaLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <iframe src="http://afiliados2.metmedicinaprivada.com/debito_cuenta.asp" frameborder="0" width="450" height="800"></iframe>
            </div>
        </div>
    </div>
</div>

<!-- Tarjeta de Crédito -->
<div class="modal fade no-imprimir" id="modal_tarjeta" tabindex="-1" role="dialog" aria-labelledby="modal_tarjetaLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <iframe src="http://afiliados2.metmedicinaprivada.com/debito_tarjeta.asp" frameborder="0" width="450" height="900"></iframe>
            </div>
        </div>
    </div>
</div>

<!-- Cambio de Plan -->
<div class="modal fade no-imprimir" id="mCambioPlan" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="gris">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cambio de Plan</h4>
            </div>
            <div class="modal-body container-fluid" id="gris">
                <div class="modal-result">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Solicitud de Credencial -->
<div class="modal fade no-imprimir" id="mCredencial" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="gris">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Solicitud de Credenciales</h4>
            </div>
            <div class="modal-body container-fluid" id="gris">
                <div class="modal-result">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Datos personales -->
<div class="modal fade no-imprimir" id="modal_datos" tabindex="-1" role="dialog" aria-labelledby="modal_datosLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <iframe src="http://afiliados2.metmedicinaprivada.com/actualizacion_web/principal.asp" frameborder="0" width="650" height="800"></iframe>
            </div>
        </div>
    </div>
</div>

<!-- Cambio de Condicion Impositiva -->
<div class="modal fade no-imprimir" id="mCCImpositiva" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="gris">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cambio de Condición Impositiva</h4>
            </div>
            <div class="modal-body container-fluid" id="gris">
                <div class="modal-result">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Contratacion de Servicios Adicionales -->
<div class="modal fade no-imprimir" id="mServAdicionales" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="gris">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Contratación de Servicios Adicionales</h4>
            </div>
            <div class="modal-body container-fluid" id="gris">
                <div class="modal-result">
                </div>
            </div>
        </div>
    </div>
</div>