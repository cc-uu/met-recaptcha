

<div class="container menu_atonline">
	<?php if(isset($xPagina)){ ?>
		<ul class="nav nav-tabs list_contact">
			<li><span class="single">atención online</span></li>
			<li><a <?php if($xPagina == 'tramites') echo 'class="active"'; ?>href="{{ asset('/atencion/index') }}" id="single">trámites</a></li>
			<li><a <?php if($xPagina == 'descargas') echo 'class="active"'; ?>href="{{ asset('/atencion/descargas') }}" id="single">descargas</a></li>
			<li><a <?php if($xPagina == 'facturacion') echo 'class="active"'; ?>href="{{ asset('/atencion/facturacion') }}" id="single">facturación</a></li>
			<li><a <?php if($xPagina == 'preguntas') echo 'class="active"'; ?>href="{{ asset('/atencion/preguntas') }}">preguntas<br>frecuentes</a></li>
			<li><a <?php if($xPagina == 'donde') echo 'class="active"'; ?>href="{{ asset('/atencion/donde') }}" id="single">dónde estamos</a></li>
		</ul>
	<?php } ?>
</div>