@extends('layouts.app')
@section('title', ' | Planes de Salud - Comparativa')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion comparativa">   
        {{-- PLANES PLUS   --}}  
        @if ($tipo == 'plus')
            <div class="container encabezado">
                <h1>PLANES DE SALUD</h1><br>
                <a href="javascript:history.back()">
                    <img src="{{ asset('/images/volver.svg') }}"> Volver
                </a>
                <h2>Comparativa de Planes MET Plus</h2>
            </div> 

            <div class="container main planes">
                <div class="row">
                    <div class="col-4 col-lg-3 primer-columna"> 
                        <h3>Prestaciones</h3>                                       
                        <ul class="row">
                            @foreach ($MTA as $item)
                                
                        
                            <li>{{$item->titulo}}</li>
                            @endforeach
                            {{-- <li>Consultas médicas y prácticas ambulatorias en prestadores de cartilla sin topes ni límites</li>
                            <li>Reintegros (según corresponda plan)</li>
                            <li>Habitación individual o VIP (según disponible del prestador)</li>
                            <li>Exámenes complementarios de rutina y alta complejidad</li>
                            <li>  Cobertura Odontologica sin copagos</li>
                            <li> Cirugia Refractiva**</li>
                            <li> Asistencia al Viajero en Argentina y en el mundo</li>
                            <li> MET digital: credencial on line / App Exclusiva / trámites on line</li>
                            <li>Médico On Line las 24 hs.</li>      --}}
                        </ul>
                    </div>

                    <div class="col-8 col-lg-9 otras-columnas p-0">
                        <div class="d-flex">
                            <!-- Se imprime este div por cada plan que haya. siempre son 3. cada plan es un Tipo de contenido distinto -->
                            <div class="col-4 text-center plan mta">  
                                <a href="/planesdesalud/plan/18024/plan-mta">
                                    <h3>MTA +</h3>                          
                                </a>
                                <ul class="row">
                                    @foreach ($MTA as $item)
                                
                                        @if ($item->portada == true)
                                        <li> <i>   <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                        @else
                                            @if ($item->descripcion != '')
                                            <li> <p>{!!$item->descripcion!!}</p></li>
                                            @else
                                            <li> <i> <img src="{{ asset('/images/cruz.svg') }}" alt=""></i></li>
                                            @endif 
                                        @endif 
                                    @endforeach
                                    {{-- <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>                                                                                 --}}
                                        
                                </ul>
                            </div>
                            
                            <div class="col-4 text-center plan mtb"> 
                                <a href="/planesdesalud/plan/18025/plan-mtb">
                                    <h3>MTB +</h3>                           
                                </a>
                                <ul class="row">
                                    @foreach ($MTB as $item)
                                
                                    @if ($item->portada == true)
                                    <li> <i>   <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    @else 
                                        @if ($item->descripcion != '')
                                        <li> <p>{!!$item->descripcion!!}</p></li>
                                        @else
                                        <li> <i> <img src="{{ asset('/images/cruz.svg') }}" alt=""></i></li>
                                        @endif 
                                    @endif 
                                @endforeach
                                </ul>
                            </div>

                            <div class="col-4 text-center plan mtc">    
                                <a href="/planesdesalud/plan/18026/plan-mtc">
                                    <h3>MTC +</h3>                        
                                </a>
                                <ul class="row">
                                    @foreach ($MTC as $item)
                                
                                    @if ($item->portada == true)
                                    <li> <i>   <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    @else 
                                        @if ($item->descripcion != '')
                                        <li> <p>{!!$item->descripcion!!}</p></li>
                                        @else
                                        <li> <i> <img src="{{ asset('/images/cruz.svg') }}" alt=""></i></li>
                                        @endif 
                                    @endif 
                                @endforeach
                                </ul>
                            </div>

                        </div>                    
                    </div>
                    <div class="btn-cotizador">
                        <a href="http://compraonline.metmedicinaprivada.com/" target="_blank">COTIZÁ <strong>TU PLAN MET</strong></a>
                    </div>
                </div>
            </div>  
 {{-- PLANES INTERIOR   --}}
        @else

            <div class="container encabezado">
                <h1>PLANES DE SALUD</h1><br>
                <a href="javascript:history.back()">
                    <img src="{{ asset('/images/volver.svg') }}"> Volver
                </a>
                <h2>Comparativa de Planes MET Interior</h2>
            </div> 

            <div class="container main planes">
                <div class="row">
                    <div class="col-4 col-lg-3 primer-columna"> 
                        <h3>Prestaciones</h3>                                       
                        <ul class="row">
                            @foreach ($IMTA as $item)
                                
                        
                            <li>{{$item->titulo}}</li>
                            @endforeach
                            {{-- <li>Consultas médicas y prácticas ambulatorias en prestadores de cartilla sin topes ni límites</li>
                            <li>Reintegros (según corresponda plan)</li>
                            <li>Habitación individual o VIP (según disponible del prestador)</li>
                            <li>Exámenes complementarios de rutina y alta complejidad</li>
                            <li>  Cobertura Odontologica sin copagos</li>
                            <li> Cirugia Refractiva**</li>
                            <li> Asistencia al Viajero en Argentina y en el mundo</li>
                            <li> MET digital: credencial on line / App Exclusiva / trámites on line</li>
                            <li>Médico On Line las 24 hs.</li>      --}}
                        </ul>
                    </div>

                    <div class="col-8 col-lg-9 otras-columnas p-0">
                        <div class="d-flex">
                            <!-- Se imprime este div por cada plan que haya. siempre son 3. cada plan es un Tipo de contenido distinto -->
                            <div class="col-4 text-center plan mta">  
                                <a href="/planesdesalud/plan/49772/plan-mta">
                                    <h3>MTA </h3>                          
                                </a>
                                <ul class="row">
                                    @foreach ($IMTA as $item)
                                
                                        @if ($item->portada == true)
                                        <li> <i>   <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                        @else
                                            @if ($item->descripcion != '')
                                            <li> <p>{!!$item->descripcion!!}</p></li>
                                            @else
                                            <li> <i> <img src="{{ asset('/images/cruz.svg') }}" alt=""></i></li>
                                            @endif 
                                        @endif 
                                    @endforeach
                                    {{-- <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    <li> <i> <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>                                                                                 --}}
                                        
                                </ul>
                            </div>
                            
                            <div class="col-4 text-center plan mtb"> 
                                <a href="/planesdesalud/plan/22138/plan-mtb">
                                    <h3>MTB</h3>                           
                                </a>
                                <ul class="row">
                                    @foreach ($IMTB as $item)
                                
                                    @if ($item->portada == true)
                                    <li> <i>   <img src="{{ asset('/images/tilde.svg') }}" alt=""></i></li>
                                    @else 
                                        @if ($item->descripcion != '')
                                        <li> <p>{!!$item->descripcion!!}</p></li>
                                        @else
                                        <li> <i> <img src="{{ asset('/images/cruz.svg') }}" alt=""></i></li>
                                        @endif 
                                    @endif 
                                @endforeach
                                </ul>
                            </div>


                        </div>                    
                    </div>
                    <div class="btn-cotizador">
                        <a href="http://compraonline.metmedicinaprivada.com/" target="_blank">COTIZÁ <strong>TU PLAN MET</strong></a>
                    </div>
                </div>
            </div>  
        @endif

        <div class="container foot">

@endsection