<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Persona\Persona;

class mejorPlanSaludController extends Controller
{
    public function index(Request $request) {	

		if ($request->isMethod('post')) {
			$SabePerfil = $_SERVER['REQUEST_URI'];
			$xNombre 		= $request->input('nombre');
			$xApellido 		= $request->input('apellido');
			$xDni 			= $request->input('dni');
			$xEmail 		= $request->input('email');
			$xPref1 		= $request->input('pref1');
			$xTelefono 		= $request->input('telefono');
			$xPref3 		= $request->input('pref3');
			$xObservaciones = $request->input('observaciones');

			if (! $xEmail 
				|| ! $xNombre
				|| ! $xApellido
				|| ! $xPref1
				|| ! $xDni
			) {
				echo '<script language="javascript">alert("Por favor complete todos los campos");</script>';
				$utm_source = isset($_GET['utm_source']) ? $_GET['utm_source'] : null;
				return view('mejorplansalud.index', compact('utm_source'));
			}

			$oPersona = New Persona();
			$xIdSegmentoGCClientes = 3681;
			$Persona = $oPersona->getByDni($xIdSegmentoGCClientes, $xDni);
			if (!($Persona instanceof Persona)) {
				$Persona = new Persona();
				$Persona->idPersona = 0;
				$Persona->nombre = $xNombre;
				$Persona->apellido = $xApellido;
				$Persona->dni = $xDni;
			}
			$Persona->observaciones = $xObservaciones;
			$Persona->email = $xEmail;
			$Persona->pref3 = $xPref1;
			$Persona->movil = $xTelefono;
			$xPerfile = 'Referidos_Afiliados';
			$xPerfil = $SabePerfil;
			
			$find = ['lavoz', 'noa', 'redes', 'adwords', 'cadena3'];
			$xPerfile = 'DESCONOCIDO';
			foreach ($find as $v) {
				if (strpos($xPerfil, $v) === FALSE) {
					continue;
				}

				$xPerfile = strtoupper($v);
				break;
			}

			if ($request->input('utm_source') !== null ) {
				$xPerfile = strtoupper($request->input('utm_source'));
			}

			//Busqueda para saber si es padre o estudiante
			$xPerfilExP = NULL;
			$EstuxPadre = ['estudiantes', 'padre'];
			foreach ($EstuxPadre as $ExP) {
				if (strpos($SabePerfil, $ExP) === FALSE) {
					continue;
				}

				$xPerfilExP = strtoupper($ExP);
				break;
			}
			
			$NuevaC = $oPersona->enviarAlAsignadorAutomatico($Persona, $xPerfile, $xPerfilExP);
			if ($NuevaC) {
				$xMensaje = 'La consulta se ha enviado correctamente.';
			} else {
				$xMensaje = 'Ha sucedido un error.Intentelo mas tarde';
			}

			$Parametros = ['xMensaje'	=> $xMensaje,
						   'xPerfile'	=> $xPerfile];

			return view('mejorplansalud.gracias', compact('Parametros','xMensaje', 'xPerfile'));

		}

		$utm_source = isset($_GET['utm_source']) ? $_GET['utm_source'] : null;
		return view('mejorplansalud.index', compact('utm_source'));
	}	
}
