  
//	Otras variables
var xIdioma = 'es';
var xSitio = {idSitio: 106};

var vFlags = new Array();
var xIndexFlag = 0;
var xEnvios = 0;

function Formulario(xFormulario, xTitulo, xModal, emailTo)
{	
	var self = this;
	var formulario = xFormulario;

	self.emailTo      = emailTo;
	self.modal        = $('#' + xModal);
	self.titulo       = $('#' + xModal + ' .modal-title');
	self.contenedor   = $('#' + xModal + ' .modal-result');
	self.mjeEspera    = 'Por favor espere...';
	self.mjeRespuesta = 'Gracias por contactarse con nosotros.';
	self.mjeError     = 'Ocurrio un error, por favor intente nuevamente mas tarde';

    self.validar = function()
    {
        try
        {
			that        = this;
			that.valida = true;

			var xElementos = $(formulario)[0].elements;
			var xValidar   = new Validar();

            $.each(xElementos, function()
                {
                    if( $(this).hasClass('validar') )
                        if( !xValidar.vacio(this) )
                            that.valida = false;

                    if( $(this).hasClass('validarInt') )                
                        if( !xValidar.numero(this) )
                            that.valida = false;

                    if( $(this).hasClass('validarEmail') )
                        if( !xValidar.email(this) )
                            that.valida = false;

                    if( $(this).hasClass('validarDoc') )
                        if(!xValidar.documento(this))
                            that.valida = false;
                        
                }
            );

			return that.valida;
		}
		catch(e){
			console.log('Formulario.validar');
			console.log(e);
		}
	}

    self.clear = function()
    {
        try
        {
			$('input[type=text]').each(function(index, elemento){
				$(elemento).val('');
			});
			$('input[type=date]').each(function(index, elemento){
				$(elemento).val('0000-00-00');
			});
			$('select').each(function(index, elemento){
				$(elemento).val('0');
			});
			$('input[type=radio]').each(function(index, elemento){
				$(elemento).attr('checked', false);
			});
			$('input[type=checkbox]').each(function(index, elemento){
				$(elemento).attr('checked', false);
			});
			$('input[type=file]').each(function(index, elemento){
				$(elemento)[0].files.length = 0;
			});
			$('select').each(function(index, elemento){
				$(elemento).val(0);
			});
			$('textarea').each(function(index, elemento){
				$(elemento).val('');
			});
		}
		catch(e){
			console.log('Formulario.clearForm');
		}
	}

    self.send = function()
    {
        try
        {
            if( self.validar() )
            {
				self.titulo.html(xTitulo)
				self.contenedor.addClass('loading');
				self.contenedor.html('<p>' + self.mjeEspera + '</p>');
				self.modal.modal();
				
				var xPersona = new Persona('es', self.emailTo);
				xPersona.setPersona(formulario);
                if( document.getElementById('fileCurriculum') )
                {
                    var fileExtension = "";
                    //función que observa los cambios del campo file y obtiene información
              
                    //obtenemos un array con los datos del archivo
                    var file = $("#fileCurriculum")[0].files[0];

                    if( file != null )
                    {
                        //obtenemos el nombre del archivo
                        var fileName = file.name;
                        //obtenemos la extensión del archivo
                        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                        //obtenemos el tamaño del archivo
                        var fileSize = file.size;
                        //obtenemos el tipo de archivo image/png ejemplo
                        var fileType = file.type;
                        //mensaje con la información del archivo
                    }
                }

                //información del formulario
                var formData = new FormData(formulario);
                //hacemos la petición ajax  
                $.ajax(
                    {
                        url: baseUrl + 'contacto/send',  
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: formData,          
                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
        
                        //una vez finalizado correctamente
                        complete: function(response){
                            console.log('se cargo'); 
                            self.success(response.statusText);
                        }
                        //si ha ocurrido un error
                        /*    error: function(data){
                            console.log('NO se cargo');
                        }*/
                    }
                );
			}
		}
		catch(e){
			console.log('Formulario.send');
			console.log(e);
		}
	}

    self.success = function(result)
    {
        try
        {
			//if(result.mensajeJson.mensaje == 'OK 1' || result.mensajeJson.mensaje == 'OK 2'){
            if( result == 'OK' )
            {
                //$("#btnEnviar").hide();
				self.contenedor.html('<p>' + self.mjeRespuesta + '</p>');
				self.contenedor.removeClass('loading');
				self.clear();
			}
            if( result.data.personas[0] != null )
            { }
            else
            {
				self.error();
			}
		}
		catch(e){
			console.log('Formulario.success');
			console.log(e);
		}
	}

    self.error = function(error)
    {
        try
        {
			self.contenedor.removeClass('loading');
			self.contenedor.html('<p>' + self.mjeError + '</p>');
			console.log(error);
		}
		catch(e){
			console.log('Formulario.error');
			console.log(e);
		}
	}
}