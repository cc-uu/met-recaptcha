<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Persona\Persona;

class PlanesdesaludController extends Controller
{
	public function index() {

		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
	
		$PlanesSalud = $Contenidos->getAll(3276,$offset,$limite,'orden','ASC');

		$titulo = "plan-mta"; 
		$id = 18024; 
		return view('planesdesalud.index',compact('PlanesSalud', 'titulo', 'id'));
	}

	public function plandesplegado($id = 18024, $titulo = "mta") {

		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
	
		$PlanesSalud = $Contenidos->getAll(3276,$offset,$limite,'orden','ASC');
		$Plan = $Contenidos->getById(3276,$id);
		
		switch ($id) {
			case 18024:
				$clavecss = 'mta';
		       break;
			case 22138:
				$clavecss = 'mtb';
				break;
			case 18025 :
				$clavecss = 'mtb';
				break;
			case 18026:
				$clavecss = 'mtc';
				break;			
			default:
			$clavecss = 'mta';
			$titulo = "plan-mta";
				break;
		}

		return view('planesdesalud.plandesplegado',compact('PlanesSalud','Plan','clavecss', 'titulo', 'id'));
	}

	public function plandesplegadogracias($id, $titulo, $gracias) {

		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
	
		$PlanesSalud = $Contenidos->getAll(3276,$offset,$limite,'orden','ASC');
		$Plan = $Contenidos->getById(3276,$id);
		
		switch ($id) {
			case 18024:
				$clavecss = 'mta';
		       break;
			case 22138:
				$clavecss = 'mtb';
				break;
			case 18025 :
				$clavecss = 'mtb';
				break;
			case 18026:
				$clavecss = 'mtc';
				break;			
			default:
			$clavecss = 'mta';
				break;
		}

		return view('planesdesalud.gracias',compact('PlanesSalud','Plan','clavecss', 'titulo', 'id', 'gracias'));
	}

	public function plandesplegadoForm(Request $request) {
		$persona = new Persona();
		$persona->AgregaPersonaPlanes($request, 3681, 'Pagina_Web');
		// print_r($request->dni);die();
		$id = $request->id;
		$titulo = $request->titulo;
		
		return redirect()->route('plandesplegadoFormGracias', ['id' => $id, 'titulo' => $titulo, 'gracias' => 'gracias']);
	}

	public function comparativa($tipo = '') {

		$Contenidos = new Contenido(); 
		$limite = 20; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.

		switch ($tipo) {
			case 'plus':
				$MTA = $Contenidos->getAll(8066,$offset,$limite,'orden','ASC');
				$MTB = $Contenidos->getAll(8067,$offset,$limite,'orden','ASC');
				$MTC = $Contenidos->getAll(8068,$offset,$limite,'orden','ASC');
				
				return view('planesdesalud.comparativa',compact('MTA','MTB','MTC','tipo'));
				break;

			case 'interior':
				$IMTA = $Contenidos->getAll(8146,$offset,$limite,'orden','ASC'); // Pan interior
				$IMTB = $Contenidos->getAll(8147,$offset,$limite,'orden','ASC'); // Pan interior

				return view('planesdesalud.comparativa',compact('IMTA','IMTB','tipo'));
					break;
			default:
				$tipo = 'plus';
				$MTA = $Contenidos->getAll(8066,$offset,$limite,'orden','ASC');
				$MTB = $Contenidos->getAll(8067,$offset,$limite,'orden','ASC');
				$MTC = $Contenidos->getAll(8068,$offset,$limite,'orden','ASC');

				return view('planesdesalud.comparativa',compact('MTA','MTB','MTC','tipo'));
				break;
		}

	
		

		
	}
}