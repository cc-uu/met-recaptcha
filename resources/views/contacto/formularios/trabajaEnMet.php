<form method="post" id="582" onsubmit="enviarFormulario('btnEnviar')">
	<div class="col-xs-3">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b" >
				<input type="text" name="nombre" class="validar form-c" placeholder="NOMBRE*" id="Nombre">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" name="apellido" class="validar form-c" placeholder="APELLIDO*" id="Apellido">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" name="email" class="validar validarEmail form-c" placeholder="EMAIL*" id="E-mail">
			</li>
			<input type="hidden" name="idSegmento" value="582">

			<li class="list-group-item input_afil_b" >
				<input type="text" name="provincia" class="form-c" placeholder="PROVINCIA" id="Provincia">
			</li>

			<li class="list-group-item input_afil_b" >
				<div class="row">
					<div class="col-xs-4">
						<input class="validarInt form-c" name="pref3" type="text" placeholder="PREFIJO" id="CaractTelefono">
					</div>
					<div class="col-xs-8">
						<input class="validarInt form-c" name="movil" type="text" placeholder="TELÉFONO" id="NumberTelefono">
					</div>
				</div>
			</li>
			<span class="data_oblig">* datos obligatorios</span>
		</ul>
	</div>
	<div class="col-xs-3">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_02" >
				<h2>PUESTO DE INTERÉS</h2>
			</li>
			<li class="list-group-item input_afil_02"><label>CALIDAD</label><input class="form-p" name="checkPI[]" value="40076" type="checkbox" id="40076"></li>
			<li class="list-group-item input_afil_02"><label>MARKETING</label><input class="form-p" name="checkPI[]" value="40077" type="checkbox" id="40077"></li>
			<li class="list-group-item input_afil_02"><label>RECURSOS HUMANOS</label><input class="form-p" name="checkPI[]" value="40078" type="checkbox" id="40078"></li>
			<li class="list-group-item input_afil_02"><label>SISTEMAS</label><input class="form-p" name="checkPI[]" value="40079" type="checkbox" id="40079"></li>
		</ul>
	</div>
	<div class="col-xs-3">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_02"><label>VENTAS</label><input class="form-p" name="checkPI[]" value="40080" type="checkbox" id="40080"></li>
			<li class="list-group-item input_afil_02"><label>AT. AL CLIENTE</label><input class="form-p" name="checkPI[]" value="40081" type="checkbox" id="40081"></li>
			<li class="list-group-item input_afil_02"><label>CONTROL DE GESTIÓN</label><input class="form-p" name="checkPI[]" value="40082" type="checkbox" id="40082"></li>
			<li class="list-group-item input_afil_02"><label>PRESTACIONES MÉDICAS</label><input class="form-p" name="checkPI[]" value="40083" type="checkbox" id="40083"></li>
			<li class="list-group-item input_afil_02"><label>ADMINISTRACIÓN</label><input class="form-p" name="checkPI[]" type="checkbox" value="40085" id="40085"></li>
		</ul>
	</div>
	<div class="col-xs-3">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_02">
				<h2>PREFERENCIA</h2>
			</li>
			<li class="list-group-item input_afil_02"><label>FULL TIME</label><input class="form-pr" name="checkP[]" type="checkbox" value="40086" id="40086"></li>
			<li class="list-group-item input_afil_02"><label>POR LA MAÑANA</label><input class="form-pr" name="checkP[]" type="checkbox" value="40087" id="40087"></li>
			<li class="list-group-item input_afil_02"><label>POR LA TARDE</label><input class="form-pr" name="checkP[]" type="checkbox" value="40088" id="40088"></li>

			<li class="list-group-item">
                <div class="fileUpload btn btn-primary">
                    <span>ADJUNTAR CV</span>
                    <input type="file" class="upload" name="archivo" id="fileCurriculum" />
                </div>
			</li>

			<li class="list-group-item">
				<input type="submit" value="ENVIAR" id="btnEnviar">
			</li>
		</ul>
	</div>
</form>