@extends('layouts.app')
@section('title', ' | Wellness')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion wellness">     
        <div class="container encabezado-wellness">
            <h1>Planes Wellness</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <button class="chat">Chat <span>Online</span></button>
        </div>
         
        <div class="container main">
          <div class="row">
              
              <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-wellness" id="menu-welness">
              <h2><img src="{{ asset('/images/wellness-logo.png') }}"></h2>
                      <ul>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8332 ,'titulo' => str_slug('premium') ]) }}">PREMIUM</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8333 ,'titulo' => str_slug('deluxe') ]) }}">DELUXE</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8334 ,'titulo' => str_slug('combinado') ]) }}">COMBINADO</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8335 ,'titulo' => str_slug('express 1') ]) }}">EXPRESS 1</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8337 ,'titulo' => str_slug('express 2') ]) }}">EXPRESS 2</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 8338 ,'titulo' => str_slug('express 3') ]) }}">EXPRESS 3</a>
                          </li>
                           <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 10743 ,'titulo' => str_slug('Depilación Full') ]) }}">MODULO DEPILACIÓN FULL</a>
                          </li>
                           <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 10741 ,'titulo' => str_slug('corporal 1') ]) }}">MODULO CORPORAL 1 (Ideal Post- Parto)</a>
                          </li>
                           <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 10740 ,'titulo' => str_slug('corporal 2') ]) }}">MODULO CORPORAL 2</a>
                          </li>
                          <li>
                              <a href="{{ route('wellnessdesplegado', ['id' => 10744 ,'titulo' => str_slug('corporal 2') ]) }}">MODULO FACIAL FULL</a>
                          </li>
                      </ul>
                      <button class="btn-cotiza">Cotizá</br><strong>tu plan wellness</strong></button>
              </div>
              <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right content-wellness">
              <h2 class="h2-wellness">{{$tituloSelect}}</h2>
                      <div class="row m-0">
                      <div class="row m-0 content-top">
                        <h3>MÁS COBERTURA, MÁS SERVICIOS, MÁS TRANQUILIDAD.</h3>
                      </div>
                    @if($PlanesWellnes)
                      @foreach ($PlanesWellnes as $item)
                      
                        @if($item != end($PlanesWellnes))
                                <div class="row m-0 descripcion">
                                    <div class="col-12 col-lg-3 brown">
                                                <div class="col-12">{{$item->titulo}}</div>
                                    </div>
                                            <div class="col-12 col-lg-9">
                                            <div class="col-4 desc-content">{{$item->bajada}}</div>
                                            <div class="col-4 desc-content">{{$item->subtitulo}} </div>
                                            <div class="col-4 desc-content">{{$item->tags}}</div>
                                            </div>

                                </div>
                            @endif
                    
                {{-- <div class="row m-0 descripcion">
                      <div class="col-12 col-lg-3 brown">
                                  <div class="col-12">Radiofrecuencia Multipolar</div>
                      </div>
                              <div class="col-12 col-lg-9">
                              <div class="col-4 desc-content">Freeze by venus</div>
                              <div class="col-4 desc-content">3 sesiones</div>
                              <div class="col-4 desc-content">1 Zona</div>
                              </div>

                </div>
                <div class="row m-0 descripcion">
                      <div class="col-12 col-lg-3 brown">
                                  <div class="col-12">Remodelacion + Reducción Corporal + Tonificacion Corporal</div>
                      </div>
                              <div class="col-12 col-lg-9">
                              <div class="col-4 desc-content">Termoeslim + Bodyter</div>
                              <div class="col-4 desc-content">12 sesiones de c/u</div>
                              <div class="col-4 desc-content">Corporal</div>
                              </div>

                </div>
                <div class="row m-0 descripcion">
                      <div class="col-12 col-lg-3 brown">
                                  <div class="col-12">Presoterapia - Tratamiento Linfático</div>
                      </div>
                              <div class="col-12 col-lg-9">
                              <div class="col-4 desc-content">Freeze by venus</div>
                              <div class="col-4 desc-content">12 sesiones</div>
                              <div class="col-4 desc-content">Corporal</div>
                              </div>

                </div>
                <div class="row m-0 descripcion">
                      <div class="col-12 col-lg-3 brown">
                                  <div class="col-12">Masoterapia</div>
                      </div>
                              <div class="col-12 col-lg-9">
                              <div class="col-4 desc-content">Mesoterapia</div>
                              <div class="col-4 desc-content">6 sesiones</div>
                              <div class="col-4 desc-content">1 Zona</div>
                              </div>

                </div>
                <div class="row m-0 descripcion">
                      <div class="col-12 col-lg-3 brown">
                                  <div class="col-12">Plan Nutricional</div>
                      </div>
                              <div class="col-12 col-lg-9">
                              <div class="col-4 desc-content">Consulta</div>
                              <div class="col-4 desc-content">6 sesiones</div>
                              <div class="col-4 desc-content">4 Consultas más dieta</div>
                              </div>

                </div> --}}
             
                    
                
                     
                
                     
                        @if($item === end($PlanesWellnes))
                            <div class="col-12 wellness-precios">
                            <div class="col-7 wellness-cuotas">{{$item->titulo}}</div>
                            <div class="col-5 wellness-precio"><p>{{$item->tags}}</p></div>
                            </div>
                        @endif
                     @endforeach
                @endif

</div>
</div>

<div class="formulario-planes pink col-12">
    @if ($gracias == null)
      <form class="vertical" action="{{route('wellnessdesplegadoForm')}}" method="POST" onsubmit="enviarFormulario('enviar')">
          @csrf
          <input hidden="hidden" id="formulario" name="origen" value="{{$titulo}}">
          <input hidden="hidden" id="id" name="id" value="{{$id}}">
          <h3>Solicitud de contacto</h3>
          <div class="row m-0">
              <div class="col-12 col-sm-4">
                  <div class="form-group">
                      <label>Nombre *</label>
                      <input type="text" name="nombre" id="nombre" required>
                  </div>
                  <div class="form-group">
                      <label>Apellido *</label>
                      <input type="text" name="apellido" id="apellido" required>
                  </div>
                  <div class="form-group">
                      <label>E-mail *</label>
                      <input type="text" name="email" id="email" required>
                  </div>
              </div>
              <div class="col-12 col-sm-4">
                  <div class="form-group">
                      <label>DNI *</label>
                      <input type="text" name="dni" id="dni" required>
                  </div>
                  <div class="form-group">
                      <label>Teléfono *</label>
                      <div>                                        
                          <input type="text" name="prefijo" id="prefijo" class="prefijo" required>                                        
                          <input type="text" name="telefono" id="telefono" class="telefono" required>
                      </div>
                  </div>
                  <h3 class="leyenda">*Datos obligatorios</h3>
              </div>
              <div class="col-12 col-sm-4">
                  <div class="form-group">
                      <label>Mensaje</label>
                      <textarea name="mensaje" id="" cols="30" rows="10"></textarea>
                  </div>
                    <div class="form-group">
                        <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                    </div>
                  
                  <button id="enviar" type="submit">ENVIAR</button>
              </div>
          </div>
      </form>
    @else
     <!-- En página gracias borrar el form y reemplazarlo por esto: -->
    <div class="mensaje-gracias">
        <span>"Gracias. A la brevedad <br>nos contactaremos con usted"</span>
    </div>
     @endif
</div>

          </div>
        </div>

        <div class="container foot">

@endsection