<?php $this->renderPartial('/atencion/menu', array('xPagina' => 'descargas')); ?>

<div class="container-fluid descargas_top">
    <div class="container sp">
        <div class="col-xs-5 cont_atencion">
            <h1>FORMULARIOS ONLINE</h1>
            <p>Para agilizar diversas gestiones, MET brinda a sus socios la opción de descargar on line formularios.</p>
        </div>
    </div>
</div>


<?php if(isset($vDescargas)){
    if($vDescargas){
        foreach ($vDescargas as $xDescarga) { 
            if(!empty($xDescarga->archivos)){ ?>
                <div class="container-fluid modulo_descarga">
                   <div class="container sp">
            	       <div class="col-xs-10">
                	       <a data-toggle="collapse" href="#<?php echo $xDescarga->idContenido ?>" aria-expanded="false" aria-controls="descripcion_descarga">
                                <h2><?php echo $xDescarga->titulo; ?></h2>
                            </a>
                        </div>
                        <div class="col-xs-2">
                	       <a class="btn_desc" href="<?php echo $xDescarga->archivos[0]->path; ?>" target="_blank">DESCARGAR</a>
                        </div>
                    </div>
                </div>

                <div class="container-fluid modulo_descrip collapse" id="<?php echo $xDescarga->idContenido ?>">
                   <div class="container sp">
    	               <div class="col-xs-10">
    		              <p><?php echo $xDescarga->descripcion; ?></p>
                        </div>
                    </div>
                </div>
<?php       }
        }
    }
} ?>