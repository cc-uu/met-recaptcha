@extends('layouts.app')
@section('title', ' | Contacto Gracias')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion contacto gracias">     
        <div class="container encabezado">
            <h1>CONTACTO</h1><br>
            <a href="javascript:history.go(-1)">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-6 left">
                    
                    <div class="nav sucursales flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach ($DireccionesMaps as $item)
                            
                      
                        <a class="@if ($loop->first) active @else  @endif" id="v-pills-{{$item->idContenido}}-tab" data-toggle="pill" href="#v-pills-{{$item->idContenido}}" role="tab" aria-selected="true">
                        <span>{{$item->titulo}}</span>
                            {!!$item->descripcion!!}
                        </a>

                        @endforeach
                        {{-- <a class="" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-selected="false">
                            <span>Cerro de las Rosas</span>
                            <p>Av Rafael Nuñez 5021. Local 1 Atención L a V 9 a 18 hs.</p>
                        </a>
                        <a class="" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-selected="false">
                            <span>Villa Allende</span>
                            <p>Río de Janeiro 281 Local 2. Atención L a V 9 a 13 hs. / 14 a 18 hs.</p>
                        </a>                         --}}
                    </div>
                
                    <div class="tab-content" id="v-pills-tabContent">
                        @foreach ($DireccionesMaps as $item)
                        <div class="tab-pane fade show @if ($loop->first) active @else  @endif"" id="v-pills-{{$item->idContenido}}" role="tabpanel">
                            {!!$item->detalle!!}
                        </div>
                        @endforeach
                        {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2864.9793587660242!2d-64.24098124623977!3d-31.35749352382665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432994b232b6335%3A0x45d94940a716603d!2sCentro%20M%C3%A9dico%20MET%20CERRO!5e0!3m2!1ses!2sar!4v1583499240708!5m2!1ses!2sar" width="100%" height="348" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2027.2967471827806!2d-64.29298680783972!3d-31.290112737896955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94329d1e5d10af2b%3A0x913178cf4af4e6e5!2sMET%20Medicina%20Privada!5e0!3m2!1ses!2sar!4v1583499338915!5m2!1ses!2sar" width="100%" height="348" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>                         --}}
                    </div>
                        
                </div>
                <div class="col-12 col-sm-6 right"> 
                    <div class="mensaje-gracias">
                        <span>"Gracias. A la brevedad <br>nos contactaremos con usted"</span>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection