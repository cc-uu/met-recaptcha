@extends('layouts.app')
@section('title', ' | Links de Interés')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion link-interes">     
        <div class="container encabezado">
            <h1>LINKS DE INTERÉS</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 right">
                    <div class="resultante row">
                        
                        <div class="col-12">
                            <h2 class="title-section">INGRESO DIRECTO A SITIOS ÚTILES Y
                                PÁGINAS DE INTERÉS</h2>
                        </div>
                            @foreach ($linkInteres as $item)
                                
                            
                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="//{{$item->link}}" target="_blank">
                                        <h2>{{Str::upper($item->titulo)}}</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>
                        @endforeach
{{-- 
                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>SANATORIO ALLENDE</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>PAGO MIS CUENTAS</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>PAGO FÁCIL</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>RAPIPAGO</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>ASSIST CARD</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>DENTIS</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>EMI</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>MEDICUS</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>CERTIFICACIÓN NEGATIVA</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>DESCARGUE SU CUIL</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>AFIP</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div>

                        <div class="acordion-resultantes col-lg-6">
                            <div class="modulo-resultante">
                                <div class="card-header">                                
                                    <a href="#">
                                        <h2>CONSULTE SU OBRA SOCIAL ACTUAL</h2> 
                                        <span>Ir al sitio</span> 
                                    </a>                                  
                                </div>
                            </div>                          
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection