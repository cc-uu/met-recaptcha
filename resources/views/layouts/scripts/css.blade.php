<link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('/css/bootstrap-grid.css') }}">
<link rel="stylesheet" href="{{ asset('/css/bootstrap-reboot.css') }}">

<!-- Agregado para chat de Tomnit --> 
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>

<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">