<?php if (!empty($xPerfile) && $xPerfile=="GOOGLE") {?>
    <!-- código de conversión de adwords agregado el 02/03/18 -->
    <script>
      fbq('track', 'Lead');
    </script>

    <?php //$this->renderPartial('/layouts/script/css'); ?>

        <div class="container-fluid fondo-deg ">
        <!-- //Codigo de conversion -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 822039997;
    var google_conversion_label = "dZC9CLW473oQvav9hwM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/822039997/?label=dZC9CLW473oQvav9hwM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
<?php }?>

<!-- Codigo de analitycs -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23691022-1']);
  _gaq.push(['_setDomainName', 'metmedicinaprivada.com']);
  _gaq.push(['_trackPageview']);



  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.3.min.js') }}"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MET · Plan MTI</title>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos-landing-mti.css') }}" />
<script type="text/javascript" src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/piecu.min.js') }}"></script>

@include('layouts.headLanding')

  <!-- Slide Head-->
  <div class="container-fluid p-0">
    <div class="wrap-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-md-6">
            <div class="wrap-logo">
              <img src="{{ asset('images/landing-mti/logo.png') }}" alt="">
            </div>
          </div>
          <div class="col-sm-7 col-md-6 float-right d-none d-sm-block">
            <div class="wrap-form">
              <p>Dejanos tus datos en el formulario de
                contacto y te llamamos hoy mismo.</p>
                <form>
                    <p>
                        <?php if (!empty($xMensaje)) {?>
                        <div class="col-xs-12 col-md-11 col-lg-11 mensaje"><?php echo $xMensaje; ?></div>
                        <?php }?>
                    </p>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Form Mobile-->
  <div class="container-fluid d-block d-sm-none form-mobile">
    <div class="row">
      <div class="col-12">
        <div class="wrap-form">
          <p>Dejanos tus datos en el formulario de
            contacto y te llamamos hoy mismo.</p>
            <form>
                <p>
                    <?php if (!empty($xMensaje)) {?>
                    <div class="col-xs-12 col-md-11 col-lg-11 mensaje"><?php echo $xMensaje; ?></div>
                    <?php }?>
                </p>
            </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Info plan-->
  <div class="bg-plan">
    <div class="container">
      <div class="row">
        <div class="col-12">
            <p>Accedé a un plan que <span>cubre al 100% todas las necesidades </span>que puedan surgir en <span>cualquier internación</span></p>
        </div>
      </div>
    </div>
  </div>

  <!-- Icons plan-->
  <div class="container p-4">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-1.png') }}">
          <p> <strong>Clínicas, Sanatorios y Profesionales </strong> de primer nivel</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-2.png') }}">
          <p><strong>Atención telefónica personalizada y online</strong></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-3.png') }}">
          <p><strong>Club de
            Beneficios MET</strong></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-6 border-icon">
        <div class="wrap-icons text-center">
          <img src="{{ asset('images/landing-mti/icon-4.png') }}">
          <p>App <strong>MET MOBILE </strong>para tu celular.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-5 offset-lg-1 col-md-6">
          <h6>COBERTURA EN INTERNACIONES</h6>
          <ul>
            <li>Clínicas</li>
            <li>Terapia intensiva intermedia</li>
            <li>UTI Neonatal/ Internación pediátrica</li>
            <li>Quirúrgicas</li>
            <li>Urgencias</li>
            <li>Controles obstétricos y estudios de tercer trimestre de embarazo</li>
            <li>Hemodinamia diagnóstica y terapéutica</li>
            <li>Cirugía cardiovascular con circulación extracorpórea</li>
            <li>Neurocirugía</li>
            <li>Hemodiálisis</li>
          </ul>
        </div>
        <div class="col-lg-5 offset-lg-1 col-md-6">
          <h6>PRESTADORES</h6>
          <ul>
            <li><strong>Sanatorio Allende (Nva. Cba.)</strong></li>
            <li><strong>Sanatorio Allende (Cerro)</strong></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="piecu" data-color="color"></div>
  </footer>

  <body>
</html>
