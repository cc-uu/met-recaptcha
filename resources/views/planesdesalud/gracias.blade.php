@extends('layouts.app')
@section('title', ' | Planes de Salud')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')



    <div class="container-fluid fdo-seccion planes">     
        <div class="container encabezado">
            <h1>PLANES DE SALUD</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-4 p-0 left">
                    <ul class="planes-plus">
                        <li>
                            <a>Planes MET Plus</a>
                        </li>
                        @foreach ($PlanesSalud as $item)
                            @if ( $item->portada )
                                
                            <li data-tab="{{$item->idContenido}}">
                                <a href="{{ route('plandesplegado', ['id' => $item->idContenido ,'titulo' => str_slug($item->titulo) ]) }}">{{$item->titulo}}</a>
                            </li>
                                
                            @endif
                        @endforeach
                        <li>
                            <a href="{{route('plancomparativo',['tipo' => 'plus'])}}">Comparativa de planes MET Plus</a>
                        </li>
                    
                    </ul>
                    <ul class="planes-interior">
                        <li>
                            <a>Planes MET Interior</a>
                        </li>
                        @foreach ($PlanesSalud as $item)
                            @if ( ! $item->portada && ($item->idContenido != 18030 &&  $item->idContenido != 18032) )
                                
                                <li data-tab="{{$item->idContenido}}">
                                <a href="{{ route('plandesplegado', ['id' => $item->idContenido ,'titulo' => str_slug($item->titulo) ]) }}">{{$item->titulo}}</a>
                                </li>
                                
                            @endif
                         @endforeach                
                        <li>
                            <a href="{{route('plancomparativo',['tipo' => 'interior'])}}">Comparativa de planes MET Interior</a>
                        </li>
                    </ul>
                    <a href="http://compraonline.metmedicinaprivada.com/" target="_blank" class="btn-cotizar">COTIZÁ<br><strong>TU PLAN MET</strong></a>
                </div>
           
                <div class="col-12 col-sm-8 p-0 right plan {{$clavecss}}" >
                 
                  
                        <h2>{{$Plan->titulo}}</h2>
                        <h3>{{$Plan->bajada}}</h3>
                        <div class="contenido-plan">
                            <div class="col-12 col-lg-6 columna">
                                @php
                                    $descripciones = explode("<hr />", $Plan->descripcion);
                                @endphp
                            
                                {!! $descripciones[0] !!}
                                {{-- <p>2 Centros Médicos MET: exclusivos para afiliados.</p>
                                <p>Consultas médicas en prestadores de cartilla, sin tope / sin límites.</p>
                                <p>Internaciones programadas o de urgencia.</p>
                                <p>Internaciones psiquiátricas en habitación VIP.</p>
                                <p>Habitación individual o VIP (según disponibilidad del prestador).</p>
                                <p>Exámenes complementarios de rutina y alta complejidad.</p>
                                <p>Reintegros en consultas y prácticas ambulatorias,</p>
                                <p>Odontología y Ópticas**</p>
                                <p>Cobertura en vacunas del calendario oficial.</p>
                                <p>Descartables en Ambulatorio.</p>
                                <p>Cirugía Refractiva**</p>
                                <p>Tratamiento Esclerosante**</p>
                                <p>Servicio de urgencias y emergencias las 24 hs.</p>
                                <p>Prestadores exclusivos para MTA+.</p>
                                <p>Estudios de baja, mediana y alta complejidad.</p>
                                <p>Odontología general.</p> --}}
                            </div>
                        
                            <div class="col-12 col-lg-6 columna">
                            
                                {!! $descripciones[1] !!}
                                {{-- <p>Cobertura en maternidad y asistencia al recién nacido, PMI.</p>
                                <p>Ecografía 4D en el PMI**</p>
                                <p>Cobertura en leches medicamentosas y maternizadas**</p>
                                <p>Tratamientos auxiliares:<br>
                                Psicología • 40 sesiones **<br>
                                Kinesiología • 35 sesiones<br>
                                Fonoaudiología • 35 sesiones</p>
                                <p>Hemodiálisis.</p>
                                <p>Asistencia al viajero a partir de los 100 km. de residencia.</p>
                                <p>Cobertura nacional e internacional.</p>
                                <p>Trasplantes</p>
                                <p>Drogas oncológicas.</p>
                                <p>Prótesis y Ortesis nacionales.</p>
                                <p>Tratamiento Radiante.</p>
                                <p>Seguro de Continuidad**</p>
                                <p>Ayuda económica por sepelio**</p>
                                <p>**(Cobertura según normas MET).</p> --}}
                            </div>
                        </div>
                     
                  
                </div>

                <div class="formulario-planes col-12">
                    @if ($gracias == null)
                        <form class="vertical" action="{{route('plandesplegadoForm')}}" method="POST" onsubmit="enviarFormulario('enviar')">
                            @csrf
                            <input name="origen" value="{{$titulo}} @if($id == 18024) plus @else interior @endif">
                            <input name="id" value="{{$id}}">
                            <input name="titulo" value="{{$titulo}}">
                            <h3>Solicitud de contacto</h3>
                            <div class="row m-0">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <label>Nombre *</label>
                                        <input type="text" name="nombre" id="nombre" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Apellido *</label>
                                        <input type="text" name="apellido" id="apellido" required>
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail *</label>
                                        <input type="text" name="email" id="email" required>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <label>DNI *</label>
                                        <input type="text" name="dni" id="dni" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Teléfono *</label>
                                        <div>                                        
                                            <input type="text" name="prefijo" id="prefijo" class="prefijo" required>                                        
                                            <input type="text" name="telefono" id="telefono" class="telefono" required>
                                        </div>
                                    </div>
                                    <h3 class="leyenda">*Datos obligatorios</h3>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <label>Mensaje</label>
                                        <textarea name="mensaje" id="" cols="30" rows="10"></textarea>
                                    </div>
                                    
                                    <button  id="enviar" type="submit">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    @else
                    <div id="gracias" class="mensaje-gracias">
                        <span>"Gracias. A la brevedad <br>nos contactaremos con usted"</span>
                    </div>
                    @endif
                  
                </div>
           
            </div>
        </div>  
        <div class="container foot">
     
@endsection

