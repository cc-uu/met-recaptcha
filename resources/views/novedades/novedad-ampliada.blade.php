@extends('layouts.app')
@section('title', ' | Novedades')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion contacto novedades">     
        <div class="container encabezado">
            <h1>NOVEDADES</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div>
        <div class="container main">
            <div class="row ampliada">
                <div class="col-xl-6 col-lg-7 col-md-7 col-sm-8 left"> 
                    <div class="ampliada-head">
                        
                        <div class="contenedor-imagen">
                            @if($NovedadesById->imagenes)
                            <img src="{{ asset($NovedadesById->imagenes[0]->path) }}" alt="">
                            @else
                            <img src="{{ asset('../images/novedad.jpg') }}" alt="">
                            @endif
                        </div>
                        <div class="compartir">
                           @php
                           $NovedadesById->printShared($NovedadesById->getLink('novedades'));
                           @endphp
                            {{-- <a href=""> <img src="{{ asset('../images/iconos/facebook.svg') }}" alt=""></a>
                            <a href=""> <img src="{{ asset('../images/iconos/twitter.svg') }}" alt=""></a> --}}
                        </div>
                    </div>
                    
                    <div class="info-ampliada">
                        <h6>{{$NovedadesById->titulo}}</h6>

                        {!! $NovedadesById->descripcion !!}
                        {{-- <p>
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et 
                            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit 
                            amet, consetetur sadipscing elitr, sed diam nonumy eirmod 
                        </p>
                        <p>
                            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et 
                            justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum
                             dolor sit amet.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam 
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem 
                            ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy 
                            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et 
                            accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est 
                            Lorem ipsum dolor sit amet.
                                                    
                        </p> --}}
                    </div>
                </div>

                <div class="col-xl-3 offset-xl-3 col-lg-4 offset-lg-1 col-md-4 offset-md-1 col-sm-4 offset-sm-0 menu-ampliada"> 
                    <ul><h6>Más novedades</h6>
                        @php
                        $i = 0;
                        @endphp 
                        @foreach ($Novedades as $item)
                                @if($i > 2)
                                @break
                                @endif
                                @if($item->idContenido != $NovedadesById->idContenido )                             
                                    <li><a href="{{ route('novedaDesplegada', ['id' => $item->idContenido ,'titulo' => $item->titulo ]) }}">{{$item->titulo}}</a></li>
                                    @php
                                    $i++;
                                    @endphp 
                                @endif

                                
                            
                         
                        @endforeach
                       
                        {{-- <li><a href="">Por ser afiliado MET, tenés un nuevo servicio: MÉDICO ON </a></li>
                        <li><a href="">CON MET TENES MÉDICO ON LINE</a></li> --}}
                    </ul>
                </div>

                
                
            </div>
        </div>


    <div class="container foot">
        
@endsection