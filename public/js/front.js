$(document).ready(function($) {



    var alterClass = function() {
        var vv = document.body.clientWidth;

        if (vv < 575) {
            $('#menu-beneficio').addClass('collapse');
        } else if (vv >= 576) {
            $('#menu-beneficio').removeClass('collapse');
        };
    };

    $(window).resize(function() {
        alterClass();
    });

    alterClass();
    });

    $(".mapa-2").hide()
    $('.btn-mapa1').click(function() {
        $(this).toggleClass("active")
        $(".mapa-2").hide()
        $(".mapa-1").fadeIn()
    });

    $('.btn-mapa2').click(function() {
        $(this).toggleClass("active")
        $(".mapa-1").hide()
        $(".mapa-2").fadeIn()
    });

    $('#modal-popup').modal('show');

    if($(".plan").length){
         $('html, body').animate({
         scrollTop: $(".plan").offset().top
         }, 2000);
    }

    $('.buscador').each(function() {
        $(this).find('input').keypress(function(e) {
            // Enter pressed?
            if(e.which == 10 || e.which == 13) {
                this.form.submit();
            }
        });
    });


function enviarFormulario(boton){
    $("#loading").trigger("click");
    $("#"+boton).text('ENVIANDO...');
    $("#"+boton).prop('disabled', true);
    // $('#'+formulario).submit();
};
 
//FUNCIONES AGREGADAS POR ANDRESz

function tipoBusqueda(valor) {
    var resultados = document.getElementById("accordionResultantes");
    if (resultados) {
        resultados.innerHTML = '';
    }

    switch (valor) {
        case 'institucion':
            document.getElementById('profesional').style.display = 'none';
            document.getElementById('institucion').style.display = '';
            document.getElementById('especialidad').style.display = '';
            break;
        case 'profesional':
            document.getElementById('institucion').style.display = 'none';
            document.getElementById('profesional').style.display = '';
            document.getElementById('especialidad').style.display = '';
            break;
        case 'farmacia':
            document.getElementById('institucion').style.display = 'none';
            document.getElementById('profesional').style.display = 'none';
            document.getElementById('especialidad').style.display = 'none';
            break;
        case 'optica':
            document.getElementById('institucion').style.display = 'none';
            document.getElementById('profesional').style.display = 'none';
            document.getElementById('especialidad').style.display = 'none';
            break;
        case 'odontologia':
            document.getElementById('institucion').style.display = 'none';
            document.getElementById('profesional').style.display = 'none';
            document.getElementById('especialidad').style.display = 'none';
            break;
        default:
            break;
    }

}

function tomarvaloresBusqueda() {

    //scroll a los resultados
     $('html, body').animate({
     scrollTop: $(".resultante").offset().top
     }, 2000);
  
    var institucion = document.getElementById('institucioninput').value;
    var profesional = document.getElementById('profesionalinput').value;
    var especialidad = document.getElementById('especialidadselect').value;
    var localidad = document.getElementById('localidadselect').value;
    var plan = document.getElementById('planselect').value;
    var tipobusqueda = document.getElementById('tipoBusqueda').value;

    if (!institucion || 0 === institucion.length) {
        institucion = 'null';

    }
    if (!profesional || 0 === profesional.length) {
        profesional = 'null';

    }
    if (!especialidad || 0 === especialidad.length) {
        especialidad = 'null';

    }


    switch (tipobusqueda) {
        case 'institucion':
            getInstituciones(institucion, especialidad, localidad, plan)
            break;

        case 'profesional':
            getProfesionales(profesional, especialidad, localidad, plan)
            break;

        case 'farmacia':
            getFarmacias(localidad, plan)
            break;

        case 'optica':
            getOpticas(localidad, plan)
            break;

        case 'odontologia':
            getOdotologia(localidad, plan)
            break;
        default:
            break;
    }

}




function getInstituciones(institucion = null, especialidad = null, localidad = null, plan = null, offset = 0, limit = 10) {
    // // Solicitud GET
    // Control para verificar que este en especialidad y limpiar la div si no esta paginando
    var resultados = document.getElementById("accordionResultantes");
    if (resultados && offset < 10) {
        resultados.innerHTML = '';
    }

    if (institucion == "") { institucion = "null" };
    if (especialidad == "") { especialidad = "null" };
    if (localidad == "") { localidad = "null" };
    if (plan == "") { plan = "null" };

    //  Peticion get al controlador de web service para traer los datos

    var especialidadNombre = especialidad;
    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getInstituciones/"+
        institucion + '/' +
        especialidad + '/' +
        localidad + '/' +
        plan + '/' +
        offset + '/' +
        limit+"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };


    $.ajax(settings).done(function (response) {
        
        this.data = response[0];

        if (response[0] == null) {
            var div = document.createElement("div");
            div.className = "modulo-resultante";
            div.innerHTML = '<div class="card-header">' +
                '<a data-toggle="collapse" data-target="#collapse">' +
                '<h2>NO SE ENCONTRARON RESULTADOS</h2>' +
                '</a>' +
                '</div>'
            resultados.appendChild(div);
        }
        else{
            this.data.forEach((item) => {

                // Controlo estar en la solapa de especialidades y completo
                if (resultados) {
                    var div = document.createElement("div");
                    div.className = "modulo-resultante";
                    div.innerHTML = '<div class="card-header" id="' + item.id + '">' +
                        '<a data-toggle="collapse" onclick=getProfesionalesByInstitucion("' + especialidadNombre + '","' + item.id + '") data-target="#collapse' + item.id + '">' +
                        '<h2>' + item.nombre + '</h2>' +
                        '<p>' + item.direccion + '<br>' +
                        item.telefono + '<br>' +
                        item.localidad + '</p>' +
                        '<span>Ver más</span>' +
                        '</a>' +
                        '</div>' +
                        '<div id="collapse' + item.id + '" class="collapse" aria-labelledby="' + item.id + '" data-parent="#accordionResultantes">' +
                        '<div class="card-body" id="profesionales' + item.id + '">' +
                        '</div>' +
                        '</div>';

                    resultados.appendChild(div);

                }
                // Si no estoy en especialidades muestro los marcadores en el mapa
                else {

                    var direccionMaps = item.direccion + ',' + item.localidad;
                    obtenerCoordenadas(direccionMaps, item.nombre, item.telefono)
                }
            });
            if (resultados) {
                // Controlo si existe el boton de paginacion o ver mas y lo elimino
                if (document.getElementById("paginacion")) {
                    var eliminar = document.getElementById("paginacion");
                    resultados.removeChild(eliminar);
                }

                var newOffset = offset + 10;
                // Controlo controlo que los resultados sean mas de 10 sino no muestro el boton
                if (response[0].length >= 9) {
                    var urlPaginacion = document.createElement("div");
                    urlPaginacion.id = 'paginacion';
                    urlPaginacion.innerHTML = '<button type="button" id="cargarmas" onclick=getInstituciones("' + institucion + '","' + especialidad + '","' + localidad + '","' + plan + '",' + newOffset + ',' + limit + ')>Cargar Más</button>'

                    resultados.appendChild(urlPaginacion);
                }
            }
        }

    });
}


function getProfesionalesByInstitucion(especialidad, cuit) {
    // // Solicitud GET
    var cuitFinal = cuit.slice(5);
    var resultados = document.getElementById('profesionales' + cuit);
    if (resultados) {
        resultados.innerHTML = '';
    }

   especialidad =  decodeURI(especialidad);

    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getProfesionalesByInstitucion/"+
        especialidad + '/' +
        'CUIT/' +
        cuitFinal+ '/' +"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };

    $.ajax(settings).done(function (response) {
        
        this.data = response[0];

        this.data.forEach((item) => {
            var p = document.createElement("p");
            p.innerHTML = ' - ' + item.nombre;

            resultados.appendChild(p);
        });
    });
}

function getProfesionales(profesional = null, especialidad = null, localidad = null, plan = null, offset = 0, limit = 10) {
    // // Solicitud GET
    // Control para verificar que este en especialidad y limpiar la div si no esta paginando
    var resultados = document.getElementById("accordionResultantes");
    if (resultados && offset < 10) {
        resultados.innerHTML = '';
    }

    if (profesional == "") { profesional = "null" };
    if (especialidad == "") { especialidad = "null" };
    if (localidad == "") { localidad = "null" };
    if (plan == "") { plan = "null" };

    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getProfesionales/"+
        profesional + '/' +
        especialidad + '/' +
        localidad + '/' +
        plan + '/' +
        offset + '/' +
        limit+"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };

    $.ajax(settings).done(function (response) {
        
        this.data = response[0];

        if (response[0] == null) {
            var div = document.createElement("div");
            div.className = "modulo-resultante";
            div.innerHTML = '<div class="card-header">' +
                '<a data-toggle="collapse" data-target="#collapse">' +
                '<h2>NO SE ENCONTRARON RESULTADOS</h2>' +
                '</a>' +
                '</div>'
            resultados.appendChild(div);
        }
        else{
             this.data.forEach((item) => {
                if (resultados) {
                    var div = document.createElement("div");
                    div.className = "modulo-resultante";
                    div.innerHTML = '<div class="card-header" id="' + item.id + '">' +
                        '<a data-toggle="collapse" data-target="#collapse' + item.id + '">' +
                        '<h2>' + item.nombre + '</h2>' +
                        '<p>' + item.especialidades[0] + '</p>' +
                        '<span>Ver más</span>' +
                        '</a>' +
                        '</div>' +
                        '<div id="collapse' + item.id + '" class="collapse" aria-labelledby="' + item.id + '" data-parent="#accordionResultantes">' +
                        '<div class="card-body" id="clinicas' + item.id + '">' +
                        '</div>' +
                        '</div>';
                    resultados.appendChild(div);
                    //cargar las clinicas
                    var clinicas = document.getElementById('clinicas' + item.id);
                    if (item.prestadores != null) {
                        item.prestadores.forEach((prestadores) => {
                            var divclinicas = document.createElement("p");
                            divclinicas.innerHTML = ' - ' + prestadores.nombre + ' <br> ' + prestadores.direccion + ' <br> ' + prestadores.telefono;
                            clinicas.appendChild(divclinicas);

                        });
                    }
                } else {

                    item.prestadores.forEach((prestadores) => {

                        var direccionMaps = prestadores.direccion + ',' + prestadores.localidad;
                        var nombreClinica = prestadores.nombre;
                        obtenerCoordenadasProfesionales(direccionMaps, nombreClinica, item.nombre, item.especialidades)
                    });

                }

            });

            if (resultados) {
                // Controlo si existe el boton de paginacion o ver mas y lo elimino
                if (document.getElementById("paginacion")) {
                    var eliminar = document.getElementById("paginacion");
                    resultados.removeChild(eliminar);
                }

                var newOffset = offset + 10;
                // Controlo controlo que los resultados sean mas de 10 sino no muestro el boton

                if (response[0].length >= 9) {
                    var urlPaginacion = document.createElement("div");

                    urlPaginacion.innerHTML = '<button type="button" onclick=getProfesionales("' + profesional + '","' + especialidad + '","' + localidad + '","' + plan + '",' + newOffset + ',' + limit + ')>Cargar Más</button>'

                    resultados.appendChild(urlPaginacion);
                }
            }

        }

        

    });
}


function getFarmacias(localidad = null, plan = null, offset = 0, limit = 10) {
    // // Solicitud GET
    // Control para verificar que este en especialidad y limpiar la div si no esta paginando
    var resultados = document.getElementById("accordionResultantes");
    if (resultados && offset < 10) {
        resultados.innerHTML = '';
    }

    if (localidad == "") { localidad = "null" };
    if (plan == "") { plan = "null" };


    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getFarmacias/null/null/"+
        localidad + '/' +
        plan + '/' +
        offset + '/' +
        limit+"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };


    $.ajax(settings).done(function (response) {
        this.data = response[0];

        if (response[0] == null) {
            var div = document.createElement("div");
            div.className = "modulo-resultante";
            div.innerHTML = '<div class="card-header">' +
                '<a data-toggle="collapse" data-target="#collapse">' +
                '<h2>NO SE ENCONTRARON RESULTADOS</h2>' +
                '</a>' +
                '</div>'
            resultados.appendChild(div);
        }
        else{
            this.data.forEach((item) => {

                    if (resultados) {
                        var div = document.createElement("div");
                        div.className = "modulo-resultante";
                        div.innerHTML = '<div class="card-header" id="' + item.id + '">' +
                            '<a data-toggle="collapse"  data-target="#collapse' + item.id + '">' +
                            '<h2>' + item.nombre + '</h2>' +
                            '<p>' + item.direccion + '<br>' +
                            item.telefono + '<br>' +
                            item.localidad + '</p>' +

                            '</a>' +
                            '</div>';

                        resultados.appendChild(div);
                    } else {
                        var direccionMaps = item.direccion + ',' + item.localidad;
                        obtenerCoordenadas(direccionMaps, item.nombre, item.telefono)
                    }
                });

                if (resultados) {
                    // Controlo si existe el boton de paginacion o ver mas y lo elimino
                    if (document.getElementById("paginacion")) {
                        var eliminar = document.getElementById("paginacion");
                        resultados.removeChild(eliminar);
                    }

                    var newOffset = offset + 10;
                    // Controlo controlo que los resultados sean mas de 10 sino no muestro el boton
                    if (response[0].length >= 9) {
                        var urlPaginacion = document.createElement("div");

                        urlPaginacion.innerHTML = '<button type="button" onclick=getFarmacias("' + localidad + '","' + plan + '",' + newOffset + ',' + limit + ')>Cargar Más</button>'

                        resultados.appendChild(urlPaginacion);
                    }
                }

        }
    });
    //  Peticion get al controlador de web service para traer los datos
}



function getOpticas(localidad = null, plan = null, offset = 0, limit = 10) {
    // // Solicitud GET
    // Control para verificar que este en especialidad y limpiar la div si no esta paginando
    var resultados = document.getElementById("accordionResultantes");
    if (resultados && offset < 10) {
        resultados.innerHTML = '';
    }

    if (localidad == "") { localidad = "null" };
    if (plan == "") { plan = "null" };


    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getOpticas/null/null/"+
        localidad + '/' +
        plan + '/' +
        offset + '/' +
        limit+"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };

     $.ajax(settings).done(function (response) {
        this.data = response[0];

        if (response[0] == null) {
            var div = document.createElement("div");
            div.className = "modulo-resultante";
            div.innerHTML = '<div class="card-header">' +
                '<a data-toggle="collapse" data-target="#collapse">' +
                '<h2>NO SE ENCONTRARON RESULTADOS</h2>' +
                '</a>' +
                '</div>'
            resultados.appendChild(div);
        }
        else{
            this.data.forEach((item) => {
                    if (resultados) {
                        var div = document.createElement("div");
                        div.className = "modulo-resultante";
                        div.innerHTML = '<div class="card-header" id="' + item.id + '">' +
                            '<a data-toggle="collapse"  data-target="#collapse' + item.id + '">' +
                            '<h2>' + item.nombre + '</h2>' +
                            '<p>' + item.direccion + '<br>' +
                            item.telefono + '<br>' +
                            item.localidad + '</p>' +

                            '</a>' +
                            '</div>';

                        resultados.appendChild(div);

                    } else {
                        var direccionMaps = item.direccion + ',' + item.localidad;
                        obtenerCoordenadas(direccionMaps, item.nombre, item.telefono)
                    }
                });

                if (resultados) {
                    // Controlo si existe el boton de paginacion o ver mas y lo elimino
                    if (document.getElementById("paginacion")) {
                        var eliminar = document.getElementById("paginacion");
                        resultados.removeChild(eliminar);
                    }

                    var newOffset = offset + 10;
                    // Controlo controlo que los resultados sean mas de 10 sino no muestro el boton
                    if (response[0].length >= 9) {
                        var urlPaginacion = document.createElement("div");

                        urlPaginacion.innerHTML = '<button type="button" onclick=getOpticas("' + localidad + '","' + plan + '",' + newOffset + ',' + limit + ')>Cargar Más</button>'

                        resultados.appendChild(urlPaginacion);
                    }
                }
            }
    });
}





function getOdotologia(localidad = null, plan = null, offset = 0, limit = 10) {
    // // Solicitud GET
    // Control para verificar que este en especialidad y limpiar la div si no esta paginando
    var resultados = document.getElementById("accordionResultantes");
    if (resultados && offset < 10) {
        resultados.innerHTML = '';
    }

    if (localidad == "") { localidad = "null" };
    if (plan == "") { plan = "null" };

    var settings = {
      "url": "http://webservice.metmedicinaprivada.com/especialidad/getOdontologia/null/null/"+
        localidad + '/' +
        plan + '/' +
        offset + '/' +
        limit+"?callback=JSON_CALLBACK",
      "method": "GET",
      "timeout": 0,
      "dataType": "jsonp",
    };

    $.ajax(settings).done(function (response) {
        this.data = response[0];

        if (response[0] == null) {
            var div = document.createElement("div");
            div.className = "modulo-resultante";
            div.innerHTML = '<div class="card-header">' +
                '<a data-toggle="collapse" data-target="#collapse">' +
                '<h2>NO SE ENCONTRARON RESULTADOS</h2>' +
                '</a>' +
                '</div>'
            resultados.appendChild(div);
        }
        else{

        this.data.forEach((item) => {
                if (resultados) {
                    var div = document.createElement("div");
                    div.className = "modulo-resultante";
                    div.innerHTML = '<div class="card-header" id="' + item.id + '">' +
                        '<a data-toggle="collapse"  data-target="#collapse' + item.id + '">' +
                        '<h2>' + item.nombre + '</h2>' +
                        '<p>' + item.direccion + '<br>' +
                        item.telefono + '<br>' +
                        item.localidad + '</p>' +

                        '</a>' +
                        '</div>';

                    resultados.appendChild(div);
                } else {
                    var direccionMaps = item.direccion + ',' + item.localidad;
                    obtenerCoordenadas(direccionMaps, item.nombre, item.telefono)
                }
            });

            if (resultados) {
                // Controlo si existe el boton de paginacion o ver mas y lo elimino
                if (document.getElementById("paginacion")) {
                    var eliminar = document.getElementById("paginacion");
                    resultados.removeChild(eliminar);
                }

                var newOffset = offset + 10;
                // Controlo controlo que los resultados sean mas de 10 sino no muestro el boton
                if (response[0].length >= 9) {
                    var urlPaginacion = document.createElement("div");

                    urlPaginacion.innerHTML = '<button type="button" onclick=getOdotologia("' + localidad + '","' + plan + '",' + newOffset + ',' + limit + ')>Cargar Más</button>'

                    resultados.appendChild(urlPaginacion);
                }
            }
        }
    });
}



var map;
var latitud;
var longitud;
var lati = sessionStorage.getItem('lati');
var long = sessionStorage.getItem('long');
var markers = [];
var inicio;
var directionsDisplay;
var infowindow;


function MarcarMapa() {

    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    if (position != "position is not defined" && !lati && !long) {
        lati = position.coords.latitude;
        long = position.coords.longitude;

        sessionStorage.setItem('lati', lati);
        sessionStorage.setItem('long', long)
    }
    initMap(lati, long);
}


function setMapOnAll(map) {
    for (let d = 0; d < markers.length; d++) {
        markers[d].setMap(map);

    }

}

function initMap(lat, long) {
    inicio = { lat: +lat, lng: +long };
    if (lat == null) {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-31.3762086, -64.2260149), 
            zoom: 8
        });
    }
    else{
        map = new google.maps.Map(document.getElementById('map'), {
            center: inicio,
            zoom: 12
        });
    }


    var inicioMarker = new google.maps.Marker({
        position: inicio,
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
        map: map,
        title: 'Usted Esta Aqui'
    });
}


function obtenerCoordenadas(direccion, nombre, telefono) {
    // Creamos el objeto geodecoder
    var geocoder = new google.maps.Geocoder();
    setMapOnAll(null);
    address = direccion;
    if (address != '') {
        // Llamamos a la función geodecode pasandole la dirección que hemos introducido en la caja de texto.
        geocoder.geocode({ 'address': address }, function(results, status) {
            if (status == 'OK') {
                // Mostramos las coordenadas obtenidas en el p con id coordenadas
                // alert('Coordenadas:   ' + results[0].geometry.location.lat() + ', ' + results[0].geometry.location.lng());
                // Posicionamos el marcador en las coordenadas obtenidas

                var myLatLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                var inicios = new google.maps.LatLng(inicio);

                var marker = [];
                marker[nombre] = new google.maps.Marker({ position: myLatLng, map: map, title: nombre });;

                google.maps.event.addListener(marker[nombre], 'click', function() {
                    infowindow = new google.maps.InfoWindow();
                    infowindow.setContent(nombre + '<br>' + telefono + '<br>' + direccion);
                    infowindow.open(map, marker[nombre]);
                });
                markers.push(marker[nombre]);

                // Centramos el map en las coordenadas obtenidas
                map.setCenter({lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()});
                map.setZoom(12);

                // infoWindow.setPosition(results[0].geometry.location);
                // infoWindow.setCenter(infoWindow.getPosition());
                // agendaForm.showMapaEventForm();
            }
        });
    }
}

function obtenerCoordenadasProfesionales(direccion, nombreClinica, nombre, especialidad) {
    // Creamos el objeto geodecoder
    var geocoder = new google.maps.Geocoder();
    setMapOnAll(null);
    address = direccion;
    if (address != '') {
        // Llamamos a la función geodecode pasandole la dirección que hemos introducido en la caja de texto.
        geocoder.geocode({ 'address': address }, function(results, status) {
            if (status == 'OK') {
                // Mostramos las coordenadas obtenidas en el p con id coordenadas
                // alert('Coordenadas:   ' + results[0].geometry.location.lat() + ', ' + results[0].geometry.location.lng());
                // Posicionamos el marcador en las coordenadas obtenidas

                var myLatLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                var inicios = new google.maps.LatLng(inicio);
                var marker = [];
                marker[nombreClinica] = new google.maps.Marker({ position: myLatLng, map: map, title: nombreClinica });;


                google.maps.event.addListener(marker[nombreClinica], 'click', function() {
                    infowindow = new google.maps.InfoWindow();
                    infowindow.setContent(nombreClinica + '<br>' + direccion + '<hr>' + especialidad + '<hr><br>' + nombre);
                    infowindow.open(map, marker[nombreClinica]);


                });
                markers.push(marker[nombreClinica]);

                // Centramos el map en las coordenadas obtenidas
                map.setCenter({lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()});
                map.setZoom(12);


                // infoWindow.setPosition(results[0].geometry.location);
                // infoWindow.setCenter(infoWindow.getPosition());
                // agendaForm.showMapaEventForm();
            }
        });
    }
}

function BuscarDireccion() {

    var geocoder = new google.maps.Geocoder();
    var dir = document.getElementById('autocomplete').value

    geocoder.geocode({ 'address': dir }, function(results, status) {
        if (status === 'OK') {
            var resultados = results[0].geometry.location;

            sessionStorage.setItem('lati', resultados.lat());
            sessionStorage.setItem('long', resultados.lng());
            long = sessionStorage.getItem('long');
            lati = sessionStorage.getItem('lati');

            initMap(sessionStorage.getItem('lati'), sessionStorage.getItem('long'));
            tomarvaloresBusqueda();
            map.setCenter({ lat: resultados.lat(), lng: resultados.lng() });

        } else {
            var mensajeError = "";
            if (status === "ZERO_RESULTS") {
                mensajeError = "No hubo resultados para la dirección ingresada.";
            } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                mensajeError = "Error general del mapa.";
            } else if (status === "INVALID_REQUEST") {
                mensajeError = "Ingrese una direccion.";
            }
            alert(mensajeError);
        }

    });
}


$( "#autocomplete" ).keypress(function() {
    var input = document.getElementById('autocomplete');
    var autocomplete = new google.maps.places.Autocomplete(input);
    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
      map: map,
      anchorPoint: new google.maps.Point(0, -29)
    });
});

$( "#tipoPlan" ).change(function() {
      if (this.value == "4181") {
        $( "#dniintegranteform" ).hide(500); 
      }
      else{
        $( "#dniintegranteform" ).show(500); 
      }
      
});



// Centros medicos

function buscarhorarios() {

    // // Solicitud GET
    var especialidadesId = document.getElementById("especialidad").value;
    var modalidad = document.getElementById("modalidad").value;
    var profesional = document.getElementById("profesional").value;
    var idcatalogo = document.getElementById("idcatalogo").value;
    var especialidadSeleccionado = document.getElementById("especialidadSeleccionado");
    var descripcionTabla = document.getElementById("descripcionTabla");
    var tablahorarios = document.getElementById("tablahorarios");

    if (tablahorarios.tBodies.length > 0) {
        tablahorarios.tBodies[0].remove();
    }

    $( ".reciclable" ).each(function( index ) {
        this.remove();    
    });

    $( ".borrarDescripcionTabla" ).each(function( index ) {
        this.remove();   
    });

    var especialidad = "";

    if (profesional && profesional != null) {
        axios.get('centrosmedicos/profesional/' +
            idcatalogo + '/' +
            profesional).
        then((response) => {

            this.data = response.data;
            //lleno con la especialidad
            if (response.data == 'nulo') {

                alert('No se encontraron resultados con ese nombre. Pruebe con otra busqueda');
                document.getElementById("profesional").value = '';


           } else {
                this.data.forEach((item) => {
                    especialidadSeleccionado.innerHTML = item.categoria.nombre;
                    especialidad = item.categoria.nombre;

                    var existeDia = false;
                    item.listaProductosDep.forEach((dias) => { 
                        if (modalidad == "presencial" &&  dias.especificacion != "" ) {
                            existeDia = true;
                        }
                        if (modalidad == "telefonico" && dias.marca != "" ) {
                            existeDia = true;
                        }
                    });

                    if (existeDia) {
                        var tabla = '<tr><th class="reciclable" scope="row"><p>' + item.titulo + '</p></th>';

                        var DiasDisponibles = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES"];

                        for (var i = 0; i < DiasDisponibles.length; i++) {
                            var dia = DiasDisponibles[i];
                            var existeDia = false;
                            item.listaProductosDep.forEach((dias) => { 
                                if (dia == dias.titulo.toUpperCase()) {
                                    existeDia = true;
                                    if (modalidad == "presencial") {
                                            tabla += ('<td class="reciclable"><p>' + dias.especificacion + '</p></td>');
                                    }
                                    else{
                                        tabla += ('<td class="reciclable"><p>' + dias.marca + '</p></td>');
                                    }
                                };
                            });

                            if (existeDia == false) {
                                tabla += ('<td class="reciclable"><p> </p></td>');
                            }
                        }    
                        tabla += '</tr>';
                        tablahorarios.innerHTML += tabla;
                    };

                });

                if (modalidad == "presencial") {
                    //aqui va el añadido de descripcion
                    axios.get('contenido/'+especialidad).
                    then((response) => {
                        this.data = response.data;
                        if (response.data != 'nulo') {
                            var descripcion = '<div class="borrarDescripcionTabla">' + response.data.descripcion + '</div>';
                            descripcionTabla.innerHTML += descripcion;
                        }
                    }).catch((e) => {
                        console.log(e);
                    });
                 }
            }

        }).catch((e) => {
            console.log(e);
        });



    } else {

        //hago una llamada para traer los horarios por especialidad
        axios.get('centrosmedicos/especialidad/' +
            especialidadesId + '/' +
            idcatalogo).
        then((response) => {

            this.data = response.data;
            //lleno con la especialidad
            if (response.data == 'nulo') {

                alert('No se encontraron resultados. Pruebe con otra busqueda')

            } else {
                this.data.forEach((item) => {
                    especialidadSeleccionado.innerHTML = item.categoria.nombre;
                    especialidad = item.categoria.nombre;

                    var existeDia = false;
                    item.listaProductosDep.forEach((dias) => { 
                        if (modalidad == "presencial" &&  dias.especificacion != "" ) {
                            existeDia = true;
                        }
                        if (modalidad == "telefonico" && dias.marca != "" ) {
                            existeDia = true;
                        }
                    });

                    if (existeDia) {
                        var tabla = '<tr><th class="reciclable" scope="row"><p>' + item.titulo + '</p></th>';

                        var DiasDisponibles = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES"];

                        for (var i = 0; i < DiasDisponibles.length; i++) {
                            var dia = DiasDisponibles[i];
                            var existeDia = false;
                            item.listaProductosDep.forEach((dias) => { 
                                if (dia == dias.titulo.toUpperCase()) {
                                    existeDia = true;
                                    if (modalidad == "presencial") {
                                            tabla += ('<td class="reciclable"><p>' + dias.especificacion + '</p></td>');
                                    }
                                    else{
                                        tabla += ('<td class="reciclable"><p>' + dias.marca + '</p></td>');
                                    }
                                };
                            });

                            if (existeDia == false) {
                                tabla += ('<td class="reciclable"><p> </p></td>');
                            }
                        }    
                        tabla += '</tr>';
                        tablahorarios.innerHTML += tabla;
                    };

                });


                //aqui va el añadido de descripcion
                if (modalidad == "presencial") {
                    axios.get('contenido/'+especialidad).
                    then((response) => {
                        this.data = response.data;
                        if (response.data != 'nulo') {
                            var descripcion = '<div class="borrarDescripcionTabla">' + response.data.descripcion + '</div>';
                            descripcionTabla.innerHTML += descripcion;
                        }
                    }).catch((e) => {
                        console.log(e);
                    });
                }

            }


        }).catch((e) => {
            console.log(e);

        });

    }

}

function buscarAfiliadoByCredencial() {
    var dni = document.getElementById("dni").value;
    var tipo = document.getElementById("tipoDni").value;
    //hago una llamada para traer los horarios por especialidad{
        // console.log(response.data);
        var settings = {
          "url": "http://webservice.metmedicinaprivada.com/credencial/getCredencial/"+tipo+"/"+dni+"/1/?callback=JSON_CALLBACK",
          "method": "GET",
          "timeout": 0,
          "dataType": 'jsonp',
        };

        $.ajax(settings).done(function (response) {
            this.data = response[0];
            if (response[0].nroCarnet != null) {
                document.getElementById('nafiliado').value = this.data.nroCarnet;
                document.getElementById('nombreapellido').value = this.data.nombre;
                document.getElementById('dnia').value = this.data.documento.numero;
                document.getElementById('tipoa').value = this.data.tipoAfiliado;
                document.getElementById('plan').value = this.data.planAfiliado;
                var adicionales = "";
                // console.log(this.data.medDomicilio);
                // console.log(this.data.odontologia);
                // console.log(this.data.pmi);
                if(this.data.medDomicilio != 'NO')
                    adicionales = adicionales + 'Médico a Domicilio | ';

                if(this.data.odontologia != 'NO')
                    adicionales = adicionales + 'Odontología | ';

                if(this.data.pmi != 'NO')
                    adicionales = adicionales + 'Plan Materno Infantil';

                if(this.data.medDomicilio == 'NO' && this.data.odontologia == 'NO' && this.data.pmi == 'NO')
                    adicionales = adicionales + 'NINGUNO';

                document.getElementById('adicionales').value = adicionales;
                document.getElementById('validezDesde').value = this.data.desde;
                document.getElementById('validezHasta').value = this.data.hasta;
            }
            else{
                alert('No se encontro el afiliado');
                console.log(response);
            }

        });

        // this.data = response.data;



}

function buscarAfiliadoByDniFactura() {
    var dni = document.getElementById("dni").value;

    //hago una llamada para traer los horarios por especialidad
    axios.get('buscarAfiliado/' +
        dni).
    then((response) => {

        this.data = response.data;
        document.getElementById('nombre').value = this.data.nombre;
        document.getElementById('apellido').value = this.data.apellido;
        document.getElementById('dnia').value = this.data.dni;
        document.getElementById('email').value = this.data.emails;
        document.getElementById('dnia').value = this.data.dni;
        document.getElementById('telefono').value = this.data.telefonos[0].numero;
        document.getElementById('celular').value = this.data.telefonos[1].numero;

    }).catch((e) => {
        console.log(e);
        alert('No se encontro el afiliado')
    });

}

document.querySelector("form").addEventListener("submit",function(event) {
    if (document.getElementById("g-recaptcha-response")) {
        var recaptcha =  document.getElementById("g-recaptcha-response").value;
        if (recaptcha === "") {
            event.preventDefault();
            document.getElementById("enviar").innerHTML = "ENVIAR";
            document.getElementById("enviar").disabled = false;
            alert("Por favor indicar que no es un robot");
        }
    }
});