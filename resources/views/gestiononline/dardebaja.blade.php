@extends('layouts.app')
@section('title', ' | Gestión Online - Dar de baja')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online baja">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Dar de baja</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a class="active">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>{{$Baja->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-lg-5 intro mt-md-4">                            
                            {!!$Baja->descripcion!!}  
                        </div>
                        <div class="col-12 col-lg-7 formulario">                              

                            <form  action=" {{ route('fomulario.enviar.dardebaja') }}"  method="post" onsubmit="enviarFormulario('enviar')">
                                @csrf
                                <input type="hidden" name="idform" value="dardebaja">
                                   <h3>FORMULARIO DE BAJA</h3>
                                <div class="form-group">
                                    <label>Nombre*</label>
                                    <input type="text" name="nombre" id="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label>Apellido*</label>
                                    <input type="text" name="apellido" id="apellido" required>
                                </div>
                                <div class="form-group">
                                    <label>DNI*</label>
                                    <input type="text" name="dni" maxlength="8" id="dni" required>
                                </div>
                                <div class="form-group">
                                    <label>Cel.*</label>
                                    <div>
                                        <input type="text" name="prefijo" id="prefijo" class="prefijo" required>
                                        <input type="text" name="telefono" id="telefono" class="telefono" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>E-mail*</label>
                                    <input type="email" name="email" id="email" required>
                                </div>
                               
                                <div class="form-group">
                                    <label>Día y horario de contacto</label>
                                    <div class="d-flex">
                                        <select name="dia" id="dia">
                                            <option value="Lunes" selected="">Lunes</option>
                                            <option value="Martes">Martes</option>
                                            <option value="Miercoles">Miercoles</option>
                                            <option value="Jueves">Jueves</option>
                                            <option value="Viernes">Viernes</option>
                                        </select>
                                        <select class="ml-2" name="horario" id="horario">
                                            <option value="10" selected="">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Motivo de la baja:*</label>
                                    <select id="btnPlanAdicional" name="baja" class="validar form-pc" required>
                                        <option value="0">SELECCIONE</option>
                                        <option value="43260">MOTIVOS ECONOMICOS</option>
                                        <option value="43261">MOTIVOS PRESTACIONALES</option>
                                        <option value="43262">MOTIVOS ADMINISTRATIVOS</option>
                                        <option value="43263">OTROS</option>
                                    </select>
                                </div>
                                <h3 class="leyenda">*Datos obligatorios<br>
                                    <div class="form-group">
                                        <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                                    </div>
                                    Este formulario NO confirma la baja del servicio; es una solicitud y la empresa se comunicará dentro de las próximas 48 hs. hábiles. </h3>
                                    <button  id="enviar" type="submit">ENVIAR</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection