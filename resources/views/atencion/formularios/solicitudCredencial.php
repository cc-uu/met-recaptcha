<form method="post" id="2929" onsubmit="enviarFormulario('enviar')">
<input type="hidden" name="idSegmento" value="2928">
    <ul class="list-group list_afil">
        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-dd" name="dnititular" placeholder="NÚMERO DE DOCUMENTO DEL TITULAR DE LA COBERTURA*" id="1249">
        </li>

        <li class="list-group-item input_afil_b">
            <div class="row">
                <div class="col-xs-6">
                    <label>FECHA DE NAC. DEL TITULAR</label> 
                </div>
                <div class="col-xs-6">
                    <input type="date" class="validar form-dd" name="fechanacimiento" id="1264">
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-dd" name="dnicambio" placeholder="NRO. DE DOCUMENTO DEL AFILIADO QUE NECESITA LA NUEVA CREDENCIAL*" id="1265">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-dd" placeholder="MOTIVO*" name="motivo" id="1263">
        </li>

        <li class="list-group-item input_afil_b" >
            <label>DOMICILIO DE ENTREGA DE NUEVA CREDENCIAL</label>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validar-email form-c" name="email" placeholder="EMAIL*" id="E-mail">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="CALLE*" name="direccion"  id="Direccion">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="NÚMERO*" name="direccionNumero" id="Numero">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="BARRIO*" name="barrio" id="Barrio">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="LOCALIDAD*" name="provincia" id="Localidad">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="C.P.*" name="postal" id="CPostal">
        </li>

        <li class="list-group-item">
            <input id="enviar" type="submit" class="btnEnviar" value="ENVIAR">
        </li>
    </ul>
</form>