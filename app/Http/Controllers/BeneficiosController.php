<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Common\Paginador;
use Illuminate\Pagination\LengthAwarePaginator;
class BeneficiosController extends Controller
{
    
    public function index() {
    $Contenidos = new Contenido();
    $Paginador = new Paginador(); 
    
    $TodosBeneficios = $Contenidos->getAll(462,0,1000);
    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $Beneficios = $Paginador->getPaginador($TodosBeneficios,9,$currentPage);

    $Valorfiltro = "ULTIMOS BENEFICIOS";
  
		return view ('beneficios.index',compact('Beneficios', 'Valorfiltro'));
    }

    
    public function beneficioAmpliado($id,$titulo) {

      $Contenidos = new Contenido(); 
      $BeneficiosById = $Contenidos->getByID(462,$id);
   
        return view ('beneficios.beneficio-ampliado',compact('BeneficiosById'));
    }

    public function beneficioByIdTag($Valorfiltro) {

      $Contenidos = new Contenido(); 
      $Paginador = new Paginador(); 
      $filtro = 'tags';
     
      $TodosBeneficios = $Contenidos->getByFiltro(462,$Valorfiltro,$filtro);
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      $Beneficios = $Paginador->getPaginador($TodosBeneficios,9,$currentPage);
      $Valorfiltro = strtoupper($Valorfiltro);
      return view ('beneficios.index',compact('Beneficios', 'Valorfiltro'));
    }
    

}