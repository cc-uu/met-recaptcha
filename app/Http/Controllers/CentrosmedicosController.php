<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Producto\Producto;
use App\Clases\Producto\Categoria;
use App\Clases\Producto\SCatalogo;
class CentrosmedicosController extends Controller
{
	public function index($id = null) {

		$Contenidos = new Contenido(); 
		$Catalogo = new Producto(); 
		$CatalogoCategorias = new Categoria(); 
		$NvaCordoba = 10009;
		$Cerro = 10010;
		$PopUP = [];
		
		$limite = 50; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
		
		switch ($id) {
			case 'nueva-cordoba':
				$centrosMedico = $Contenidos->getById(9979,48822);
				$centrosMedicos = $Contenidos->getAll(9979,$offset,$limite,'idContenido','ASC');
				$Categorias = $CatalogoCategorias->getAllOrdenadas($NvaCordoba);
				$idCatalogo = $NvaCordoba;
				$PopUP =  $Contenidos->getAll(9706,0,20);
				
				break;
			case 'cerro-de-las-rosas':
				
				$centrosMedico = $Contenidos->getById(9979,48823);
				
				$centrosMedicos = $Contenidos->getAll(9979,$offset,$limite,'idContenido','ASC');
				$Categorias = $CatalogoCategorias->getAllOrdenadas($Cerro);
				$idCatalogo = $Cerro;
			

				break;
			default:
				$centrosMedico = $Contenidos->getById(9979,48822);
				$centrosMedicos = $Contenidos->getAll(9979,$offset,$limite,'idContenido','ASC');
				$Categorias = $CatalogoCategorias->getAllOrdenadas($NvaCordoba);
				$idCatalogo = $NvaCordoba;
				break;
		}
		
		
		

		return view('centrosmedicos.index',compact('centrosMedicos','Categorias','centrosMedico','idCatalogo','PopUP'));
	}


	public function getByEspecialidad($idcate,$idcato) {

		$Catalogo = new SCatalogo(); 

		$medicos = $Catalogo->getProductos($idcato,$idcate,null,0,99);
		if ($medicos == null) {

			$medicos = 'nulo';

		} else {
			$medicos = $medicos[0]->productos;
		}
		return response()->json($medicos);
	}

	
	public function getByProfesional($idcato,$busqueda, $idcate = 0) {

		$Catalogo = new SCatalogo(); 

		
		$medicos = $Catalogo->getProductosConFiltroRango($idcato,$idcate,null,0,20,'','','','','asc','titulo',urlencode($busqueda));
		
		if ($medicos == null || empty($medicos[0]->productos)) {

			$medicos = 'nulo';

		} else {
			$medicos = $medicos[0]->productos;
		}

		return response()->json($medicos);
	}

		public function getByContenido($especialidad) {
			// print_r($especialidad);die;
		
		$Contenidos = new Contenido(); 
		$especialidad = str_replace(" ", "%20", $especialidad);
		$especialidad = str_replace("+", "", $especialidad);

		// print_r($especialidad);die();

		$Contenido = $Contenidos->getByFiltro(10966, $especialidad ,'titulo');
		if ($Contenido == null) {

			$Contenido = 'nulo';

		} else {
			$Contenido = $Contenido[0];
		}
		return response()->json($Contenido);
	}
}