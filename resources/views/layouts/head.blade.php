<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

<meta name="description" content="{{config('main.META_DESCRIPTION')}}"/>
<meta name="keywords" content="{{config('main.META_KEYWORDS')}}" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{config('main.NOMBRE_SITIO')}}  @yield('title')</title>
<script type="text/javascript">
	var baseUrl = "<?php echo URL::to('/'); ?>/";
</script>

<!-- Facebook Pixel Code - agregado el 20/11/18 -->

<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '790532271137802');
	fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=790532271137802&ev=PageView&noscript=1"
  /></noscript>
  
  <!-- End Facebook Pixel Code -->
  
  <!-- Global site tag (gtag.js) - Google Ads: 822039997  - agregado el 20/11/18-->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-822039997"></script>
  <script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
  
	gtag('config', 'AW-822039997');
  </script>

<script src="https://www.google.com/recaptcha/api.js"></script>
