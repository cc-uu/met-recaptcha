<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Common\Paginador;
use Illuminate\Pagination\LengthAwarePaginator;
class BuscadorController extends Controller
{
	public function index(Request $request) {

		$Contenidos = new Contenido();
		$Paginador = new Paginador(); 
		
		if($request->valorabuscado) {
			$Resultados = $Contenidos->buscador($request->valorabuscado);
			
			session(['valorBuscado' => $request->valorabuscado]);
		}
		else {
			$Resultados = $Contenidos->buscador(session('valorBuscado'));
		}
		

		if(empty($Resultados)) {
		
			$Resultados = 'No se encontraron resultados';
		}else {
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$Resultados = $Paginador->getPaginador($Resultados,15,$currentPage);
		}
			

		return view('buscador.index',compact('Resultados'));
	}
}