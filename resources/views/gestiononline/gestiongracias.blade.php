@extends('layouts.app')
@section('title', ' | Gestión Online - '.$pagina)
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online gracias autorizaciones">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>{{$pagina}}</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}" @if($pagina == "Adhesión al débito") class="active" @endif >Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}" @if($pagina == "Cambio de Plan") class="active" @endif>Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}" @if($pagina == "Contratación de servicios adicionale") class="active" @endif>Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}" @if($pagina == "Descargá y pagá tu factura") class="active" @endif>Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}" @if($pagina == "Descargá tu credencia") class="active" @endif>Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}" @if($pagina == "Medios de pago") class="active" @endif>Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}" @if($pagina == "Formularios Online") class="active" @endif>Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}" @if($pagina == "Dar de baja") class="active" @endif>Dar de baja</a>
                        </li>
                        <li>
                            <a  href="{{ route('autorizaciones') }}" @if($pagina == "Autorizaciones") class="active" @endif>Autorizaciones</a> 
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <div class="mensaje-gracias">
                        <span>"Gracias. A la brevedad <br>nos contactaremos con usted"</span>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection