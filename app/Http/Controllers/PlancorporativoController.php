<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
class PlancorporativoController extends Controller
{
	public function index() {
		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
		$ContenidoCorporativo = $Contenidos->getAll(9852,$offset,$limite,'idContenido','ASC');
	

		return view('plancorporativo.index',compact('ContenidoCorporativo'));
	}
	public function gracias() {
		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.
		$ContenidoCorporativo = $Contenidos->getAll(9852,$offset,$limite,'idContenido','ASC');
	

		return view('plancorporativo.gracias',compact('ContenidoCorporativo'));
	}
}