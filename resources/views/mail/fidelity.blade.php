
<table width='650' cellspacing='0' cellpadding='0' border='0' align='center'>
  <tbody><tr>
    <td colspan='3'>
      <img src='http://app.fidelitytools.net/images/alertas/topFidelity.jpg'>
    </td>
  </tr>
  <tr>
    <td width='50'>&nbsp;</td>
    <td width='550'>&nbsp;</td>
    <td width='50'>&nbsp;</td>
  </tr>
  <tr>
    <td width='50'>&nbsp;</td>
    <td style='font-family:Arial, sans-serif; font-size:14px; line-height:20px; color:#000;' width='550'>
      @php   

      $campos = array_keys($request->all());
      
      @endphp
      @foreach ($campos as $key => $value)
     
      @if($key > 1)
      @switch($value)
          @case('nombre')
            <p><strong>NOMBRE:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('apellido')
            <p><strong>APELLIDO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('dni')
            <p><strong>DNI:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
          @break
          @case('dnititularcobertura')
            <p><strong>DNI:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('fechanacimiento')
            <p><strong>FECHA DE NACIMIENTO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('dniintegrante')
            <p><strong>DNI INTEGRANTE:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('dninuevointegrante')
            <p><strong> DNI DEL INTEGRANTE QUE CONTRATA EL NUEVO SERVICIO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('razonsocial')
            <p><strong>RAZON SOCIAL:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('cuit')
            <p><strong>CUIT:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('localidad')
            <p><strong>LOCALIDAD:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('provincia')
            <p><strong>PROVINCIA:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('edad')
            <p><strong>EDAD:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('horariocontacto')
            <p><strong>HORARIO DE CONTACTO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('canthijos')
            <p><strong>CANT. HIJOS:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('trabajadependecia')
            <p><strong>TRABAJA EN RELACION DE DEPENDENCIA:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('tienecobertura')
            <p><strong>TIENE COBERTURA:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('cual')
            <p><strong>CUAL:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('cantempleados')
            <p><strong>CANT. EMPLEADOS:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('osocial')
            <p><strong>OBRA SOCIAL:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('responsable')
            <p><strong>RESPONSABLE:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('cargore')
            <p><strong>CARGO RESPONSABLE:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('email')
            <p><strong>EMAIL:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('telefono')
            <p><strong>TELEFONO:</strong> {{ $request->prefijo  ?? 'N/A' }} - {{ $request->$value  ?? 'N/A' }}  <br></p>
            @break
          @case('consulta')
            <p><strong>CONSULTA:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
         @case('baja')
            <p><strong>MOTIVO DE BAJA:</strong>
               @switch($request->$value)
                @case(43260)
                  MOTIVOS ECONOMICOS
                  @break
                @case(43261)
                  MOTIVOS PRESTACIONALES
                  @break
                @case(43262)
                  MOTIVOS ADMINISTRATIVOS
                  @break
                @case(43263)
                  OTROS
                  @break
                @default
                  'N/A' 
                  @break
              @endswitch
            <br></p>
            @break
          @case('dia')
            <p><strong>DIA DE CONTACTO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('horario')
            <p><strong>HORARIO DE CONTACTO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('checkPI')
            <p><strong>AREAS QUE SE POSTULA</strong>  @foreach ( $request->checkPI as $item)
              {{ $item  }} @endforeach<br></p>
            @break
          @case('checkP')
            <p><strong>HORARIOS</strong>  @foreach ( $request->checkP as $item)
              {{ $item  }} @endforeach<br></p>
            @break
          @case('arancel')
            <p><strong>ARANCEL:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
          @case('cambioplan')
            <p><strong>CAMBIO DE PLAN PARA:</strong>  
              @if( $request->$value == 4181)
                TODO EL GRUPO
              @else
                INTEGRANTE
              @endif
              <br>
            </p>
            @break
          @case('servicioA')
            <p><strong>SERVICIO ADICIONAL PARA:</strong>
              @switch($request->$value)
                 @case(4181)
                  TODO EL GRUPO
                  @break
                @case(4182)
                  INTEGRANTE
                  @break
                @default
                  'N/A' 
                  @break
              @endswitch 
              <br>
            </p>
            @break
          @case('adicional')
            <p><strong>ADICIONAL:</strong> 
              @switch($request->$value)
                 @case(4187)
                  MÉDICO A DOMICILIO
                  @break
                @case(4188)
                  PLAN ODONTOLÓGICO
                  @break
                @default
                  'N/A' 
                  @break
              @endswitch
             <br></p>
            @break
          @case('nuevoplan')
            <p><strong>NUEVO PLAN:</strong>
               @switch($request->$value)
                 @case(4178)
                  MTA+
                  @break
                @case(4179)
                  MTB+
                  @break
                @case(4180)
                  MTC+
                  @break
                @default
                  'N/A' 
                  @break
              @endswitch
            <br></p>
            @break
          @case('motivo')
            <p><strong>MOTIVO:</strong>  {{ $request->$value  ?? 'N/A' }} <br></p>
            @break
           
              	
      @endswitch
      

      
      
      
      @endif
      @endforeach
    </td>
    <td width='50'>&nbsp;</td>
  </tr>
  <tr>
    <td width='50'>&nbsp;</td>
    <td width='550'>&nbsp;</td>
    <td width='50'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='3'>
      <img src='http://app.fidelitytools.net/images/alertas/pieFidelity.jpg'>
    </td>
  </tr>
</tbody></table>
