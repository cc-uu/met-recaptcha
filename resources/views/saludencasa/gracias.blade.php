<?php if (!empty($xPerfile) && $xPerfile=="GOOGLE") {?>
    <!-- código de conversión de adwords agregado el 02/03/18 -->
    <script>
      fbq('track', 'Lead');
    </script>

    <?php //$this->renderPartial('/layouts/script/css'); ?>

        <div class="container-fluid fondo-deg ">
        <!-- //Codigo de conversion -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 822039997;
    var google_conversion_label = "dZC9CLW473oQvav9hwM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/822039997/?label=dZC9CLW473oQvav9hwM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
<?php }?>

<!-- Codigo de analitycs -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23691022-1']);
  _gaq.push(['_setDomainName', 'metmedicinaprivada.com']);
  _gaq.push(['_trackPageview']);



  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.3.min.js') }}"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos-landing-saludencasa.css') }}" />
<script type="text/javascript" src="{{ asset('/js/vendor-saludencasa.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/main-saludencasa.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/piecu.min.js') }}"></script>

<header class="head">
    <div class="mwrap">
        <div class="logo"><img src="{{ asset('images/landing-saludencasa/logo.png') }}" alt="Logo de Met"></div>
    </div>
</header>
<div class="banner">
    <div class="banner-image bg-image" style="background-image: url({{ asset('images/landing-saludencasa/slide1.jpg') }});">
        <div class="banner-content">
            <div class="mwrap">
                <div class="add-wrapper">
                    <div class="box-text box-text-1">
                        <div class="text-1">Quedarte</div>
                        <div class="text-2">En casa</div>
                        <div class="text-3">es el<span>Mejor plan</span></div>
                    </div>
                    <div class="box-text box-text-2">
                        <div class="text-1">Nuevos afiliados</div>
                        <div class="text-2">
                            <div class="line-1">
                                <div class="prom">
                                    <span class="subtext-1">Hasta</span>
                                    <span class="number">40</span>
                                    <div class="complement"><span>%</span><span>de</span></div>
                                </div>
                                <div class="subtext">
                                    Ahorro
                                </div>
                            </div>
                            <div class="line-2">
                                <span class="subtext-1">en tu</span>
                                <span class="subtext-2">cobertura</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="sect-form">
    <div class="mwrap">
        <div class="form">
            <p>
                <?php if (!empty($xMensaje)) {?>
                <div class="col-xs-12 col-md-11 col-lg-11 mensaje"><?php echo $xMensaje; ?></div>
                <?php }?>
            </p>
        </div>
    </div>
</section>
<section class="sect-content sect-list-items">
    <div class="mwrap">
        <div class="list-items grid-wrapper">
            <div class="grid-3 item">
                <div class="item-wrap">
                    <div class="icon-image"><img src="{{ asset('images/landing-saludencasa/cartilla.png') }}" alt="icono cartilla"></div>
                    <div class="text">
                        <p><strong>Amplia cartilla</strong> con más de 300 sanatorios y clínicas de primer nivel</p>
                    </div>
                </div>
            </div>
            <div class="grid-3 item">
                <div class="item-wrap">
                    <div class="icon-image"><img src="{{ asset('images/landing-saludencasa/centro-medico.png') }}" alt="icono centro médico">
                    </div>
                    <div class="text">
                        <p><strong>2 Centros Médicos MET</strong> exclusivos para afiliados</p>
                    </div>
                </div>
            </div>
            <div class="grid-3 item">
                <div class="item-wrap">
                    <div class="icon-image"><img src="{{ asset('images/landing-saludencasa/internacional.png') }}"
                            alt="icono cobertura nacional e internacional"></div>
                    <div class="text">
                        <p><strong>Cobertura nacional e internacional</strong> con (Assist card)</p>
                    </div>
                </div>
            </div>
            <div class="grid-3 item">
                <div class="item-wrap">
                    <div class="icon-image"><img src="{{ asset('images/landing-saludencasa/mobile.png') }}" alt="icono met mobile"></div>
                    <div class="text">
                        <p>App <strong>MET MOBILE</strong> para tu celular</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sect-content">
    <div class="mwrap">
        <div class="content">
            <h3>MET - Medicina privada</h3>
            <div class="text">
                <p>Somos una empresa de Medicina Prepaga con servicios de excelencia y constante preocupación por el
                    bienestar de nuestros
                    afiliados. Contamos con el respaldo y la experiencia de dos instituciones líderes: Sanatorio
                    Allende y MEDICUS.</p>
                <p>Los servicios de MET Medicina Privada presentan características únicas, como el acceso a
                    profesionales médicos Jefes de
                    Servicio de Sanatorio Allende; cobertura en todas las especialidades médicas (incluida alta
                    complejidad); consultorios
                    externos propios; cobertura nacional e internacional (pacientes en tránsito), y reconocidos
                    profesionales de primer
                    nivel y amplia experiencia.</p>
            </div>
        </div>
    </div>
</section>
<footer class="foot">
    <div class="mwrap">
        <div id="piecu"></div>
    </div>
</footer>