!function($){ "use-strict"; var general = {

  gotoPosition: {
    elem: '.goto',
    action: function (target, time) {
      var target = target || 'body';
      var time = time || 1000;
      $([document.documentElement, document.body]).animate({
        scrollTop: $(target).offset().top
      }, time);
    },
    active: function () {
      var _this = this;
      var elem = $(this.elem);
      if (elem.length) {
        elem.on('click', function (e) {
          e.preventDefault()
          var target = $(this).attr('href');
          var time = $(this).data('time');
          _this.action(target, time);
        })
      }
    }
  },

  form: {
    elem: '.main-form',
    removeError: function (item) {
      item
        .removeClass('error')
        .parent()
        .find('span.error').remove()
        ;
    },
    validate: function (form) {
      var _this = this;
      form.find('.required').map(function () {
        var value = $(this).val();
        _this.removeError($(this));
        $(this).on('keyup', function () {
          _this.removeError($(this));
        });

        if (value == '' || value == undefined) {
          $(this)
            .addClass('error')
            .parent()
            .append('<span class="error">!</span>')
            ;
        }

      });

      if (form.find('.error').length) {
        return false;
      }
      return true;
    },
    active: function () {
      var _this = this;
      var elem = $(this.elem);
      if (elem.length) {
        elem.on('submit', function (e) {
          if (!_this.validate(elem)) {
            e.preventDefault()
          }
        })
      }
    }
  },

  init: function () {
    this.gotoPosition.active();
    this.form.active();
  }
};
general.init();
 }(jQuery);
    function enviarFormulario(boton){
    $("#"+boton).text('ENVIANDO...');
    $("#"+boton).prop('disabled', true);
  };
