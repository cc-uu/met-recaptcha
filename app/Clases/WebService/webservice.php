<?php
namespace App\Clases\WebService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;


class WebService {
	public $especialidades;
	public $localidades;
    public $plandesalud;
    public $instituciones;
    public $profesionales;
    public $farmacias;
    public $opticas;
    public $odontologia;
    public $url;
    public $afiliado;


	public function __construct() {
		try {
			date_default_timezone_set('America/Argentina/Buenos_Aires');
			$this->especialidades = null;
			$this->localidades = null;
            $this->plandesalud = null;
            $this->instituciones = null;
            $this->profesionales = null;
            $this->farmacias = null;
            $this->opticas = null;
            $this->odontologias = null;
            $this->afiliado = null;
            $this->url = "http://webservice.metmedicinaprivada.com/";
        

		} catch (Exception $e) {
			//Sawubona::writeLog($e);
		}
    }
    
    public function getEspecialidades(){
       
        $endpoint="/formulario/getEspecialidades?callback=JSON_CALLBACK";
        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint); 
       
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
   
        if($estado == 200) {
            $especialidades =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            $this->especialidades = json_decode($especialidades);
            return $this->especialidades[0];
        }
        else{
            dd($response);
        }
      
                
    }

    public function getLocalidades(){
       
        $endpoint="/formulario/getLocalidades?callback=JSON_CALLBACK";
        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
   
        if($estado == 200) {
            $localidades =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            $this->localidades = json_decode($localidades);
            return $this->localidades[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getPlanes(){
       
        $endpoint="/formulario/getPlanes?callback=JSON_CALLBACK";
        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();

        if($estado == 200) {
            $Planes =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            
            $this->plandesalud = json_decode($Planes);
            return $this->plandesalud[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getInstituciones($institucion='null' , $especialidad='null',$localidad='null',$plan='null',$offset=0,$limit=10){
       
        $endpoint="especialidad/getInstituciones/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $institucion . '/' .
                        $especialidad . '/' .
                        $localidad . '/' .
                        $plan . '/' .
                        $offset . '/' .
                        $limit . '/';

        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();

        if($estado == 200) {
            $instituciones   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            
            $this->instituciones = json_decode($instituciones);
            return $this->instituciones[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getProfesionalesByInstitucion($especialidad='null',$cuit='null'){
       
        $endpoint="especialidad/getProfesionalesByInstitucion/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $especialidad . '/CUIT/' .
                        $cuit . '/';
                       
        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();

        if($estado == 200) {
            $profesionales   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            
            $this->profesionales = json_decode($profesionales);
            return $this->profesionales[0];
        }
        else{
            dd($response);
        }
      
                
    }

    public function getProfesionales($profesional='null' , $especialidad='null',$localidad='null',$plan='null',$offset=0,$limit=10){
       
        $endpoint="especialidad/getProfesionales/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $profesional . '/' .
                        $especialidad . '/' .
                        $localidad . '/' .
                        $plan . '/' .
                        $offset . '/' .
                        $limit . '/';

        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();

        if($estado == 200) {
            $profesionales   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            
            $this->profesionales = json_decode($profesionales);
            return $this->profesionales[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getFarmacias($localidad='null',$plan='null',$offset=0,$limit=10){
       
        $endpoint="especialidad/getFarmacias/null/null/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $localidad . '/' .
                        $plan . '/' .
                        $offset . '/' .
                        $limit . '/';

        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
       
        if($estado == 200) {
            $farmacias   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
           
            $this->farmacias = json_decode($farmacias);
            return $this->farmacias[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getOpticas($localidad='null',$plan='null',$offset=0,$limit=10){
       
        $endpoint="especialidad/getOpticas/null/null/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $localidad . '/' .
                        $plan . '/' .
                        $offset . '/' .
                        $limit . '/';

        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
       
        if($estado == 200) {
            $opticas   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
           
            $this->opticas = json_decode($opticas);
            return $this->opticas[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getOdontologia($localidad='null',$plan='null',$offset=0,$limit=10){
       
        $endpoint="especialidad/getOdontologia/null/null/";
        $callback = "?callback=JSON_CALLBACK";
        $parametros =   $localidad . '/' .
                        $plan . '/' .
                        $offset . '/' .
                        $limit . '/';

        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint.$parametros.$callback); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
       
        if($estado == 200) {
            $odontologias   =  trim($body->getContents(),'JSON_CALLBACK\(\)');
           
            $this->odontologias = json_decode($odontologias);
            return $this->odontologias[0];
        }
        else{
            dd($response);
        }
      
                
    }


    public function getAfiliado($tipo,$dni){
       
        $endpoint="/credencial/getCredencial/".$tipo."/".$dni."/1/?callback=JSON_CALLBACK";
        $client = new Client(['base_uri' =>  $this->url]);  
        $response = $client->request('GET', $endpoint); 
    
        $estado = $response->getStatusCode();
        $body =  $response->getBody();
   
        if($estado == 200) {
            $Afiliado =  trim($body->getContents(),'JSON_CALLBACK\(\)');
            $this->Afiliado = json_decode($Afiliado);
            return $this->Afiliado[0];
        }
        else{
            dd($response);
        }
      
                
    }

}
?>