<form method="post" id="3681" onsubmit="enviarFormulario('btnEnviar')">
	<input type="hidden" name="idSegmento" value="3681">
    <input type="hidden" name="asignadorAutomatico" value="1">
    <input type="hidden" name="proviene" value="2"> <!-- 2 para Solicita un asesor -->
    <input type="hidden" name="origenWeb" value="1">
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar form-c" name="nombre" placeholder="NOMBRE*" id="Nombre">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar form-c" name="apellido" placeholder="APELLIDO*" id="Apellido">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar form-c" name="localidad" placeholder="LOCALIDAD*" id="Localidad">
			</li>


			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar validarInt form-dd" name="edad" placeholder="EDAD*" id="377">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar validarInt validarDoc form-c" name="dni" placeholder="NÚMERO DE DOCUMENTO*" id="Dni">
			</li>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b" >
				<div class="row">
					<div class="col-xs-4">
						<input class="validarInt form-c" type="text" name="pref3" placeholder="PREFIJO" id="CaractTelefono">
					</div>
					<div class="col-xs-8">
						<input class="validarInt form-c" type="text" name="movil" placeholder="TELÉFONO" id="NumberTelefono">
					</div>
				</div>
			</li>
			<li class="list-group-item input_afil_b" >
				<input type="text" class="validar validarEmail form-c" name="email" placeholder="EMAIL*" id="E-mail">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" class="form-dd" name="hatencion" placeholder="HORARIO DE CONTACTO" id="376">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" class="validarInt form-dd" name="hijos" placeholder="CANTIDAD DE HIJOS" id="445">
			</li>
			<li class="list-group-item input_afil_02">
				<label>¿TRABAJA EN RELACIÓN DE DEPENDENCIA?</label><br>
				<label>SÍ</label>
				<input type="radio" id="811" name="dependenciaSI" value="si" class="form-p">
				<label>NO</label>
				<input type="radio" id="812" name="dependenciaSI" value="no" class="form-p">
			</li>
			<span class="data_oblig">* datos obligatorios</span>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_02">
				<label>¿TIENE COBERTURA?</label>
				<label>SÍ</label>
				<input type="radio" id="809" name="cobertura" class="form-p">
				<label>NO</label>
				<input type="radio" id="810" name="cobertura" class="form-p">
			</li>
			<li class="list-group-item input_afil_b" >
				<input type="text" placeholder="¿CUÁL?" id="375" name="coberturas" class="form-dd">
			</li>
			<li class="list-group-item">
				<textarea class="contact form-c" name="mensaje" placeholder="MENSAJE" id="Comentarios"></textarea>
			</li>

			<input type="radio" id="1413" name="cobertura" class="form-pc" checked="checked" style="position: absolute; top: 140px; right:50px; z-index: -1">

			<li class="list-group-item">
				<input type="submit" value="ENVIAR" id="btnEnviar">
			</li>
		</ul>
    </div>
</form>

<!-- Google Code for Visita a Sitio Web de Met Conversion Page -->

<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 946883472;

var google_conversion_language = "en";

var google_conversion_format = "3";

var google_conversion_color = "ffffff";

var google_conversion_label = "1ee9CPyy3l0QkJfBwwM";

var google_remarketing_only = false;

/* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/946883472/?label=1ee9CPyy3l0QkJfBwwM&amp;guid=ON&amp;script=0"/>

</div>

</noscript>