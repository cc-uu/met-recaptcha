@extends('layouts.app')
@section('title', ' | Plan Corporativo')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion plan-corporativo">     
        <div class="container encabezado">
            <h1>PLAN CORPORATIVO</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-5 left">
                    @foreach ($ContenidoCorporativo as $item)
                   
                        @if ($item->idContenido == 48244)
                            <div class="intro">
                        @else 
                            <div class="puntos">
                        @endif     
                               
                                <h2>{{$item->titulo}}</h2>
                                {!!$item->descripcion!!}      
                            </div>
                        
                      
                   
               

                    @endforeach
                </div>
                <div class="col-12 col-sm-7 right">                    
                    <h2>FORMULARIO DE CONTACTO</h2>
                    <form action=" {{ route('fomulario.enviar') }}"  method="post" onsubmit="enviarFormulario('enviar')">
                        @csrf
                        <input type="hidden" name="idform" value="plancorporativo">

                        <div class="form-group">
                            <label>Razón Social *</label>
                            <input type="text" name="razonsocial" id="razonsocial" required>
                        </div>
                        <div class="form-group">
                            <label>CUIT *</label>
                            <input type="text" maxlength="12" name="cuit" id="cuit" required>
                        </div>
                        <div class="form-group">
                            <label>Cantidad de empleados</label>
                            <input type="text" name="cantempleados" id="cantempleados" required>
                        </div>                        
                        <div class="form-group">
                            <label>Obra social actual</label>
                            <input type="text" name="osocial" id="osocial" required>
                        </div>
                        <div class="form-group">
                            <label>Responsable de la consulta *</label>
                            <input type="text" name="responsable" id="responsable" required>
                        </div>
                        <div class="form-group">
                            <label>Cargo</label>
                            <input type="text" name="cargore" id="cargore" required>
                        </div>
                        <div class="form-group">
                            <label>E-mail *</label>
                            <input type="email" name="email" id="email" required>
                        </div>
                        <div class="form-group">
                            <label>Teléfono *</label>
                            <input type="text" name="prefijo" id="prefijo" class="prefijo" required>
                            <input type="text" name="telefono" id="telefono" class="telefono" required>
                        </div>
                        <div class="form-group">
                            <label>Consulta *</label>
                            <textarea name="consulta" id="consulta" cols="30" rows="10" required></textarea>
                        </div>
                        <h3 class="leyenda">*Datos obligatorios</h3>
                        <div class="form-group">
                            <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                        </div>  
                        <button  id="enviar" type="submit">ENVIAR</button>
                        
                    </form>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection