<form method="post" id="2926" onsubmit="enviarFormulario('enviar')" >
<input type="hidden" name="idSegmento" value="2926">
    <ul class="list-group list_afil">
        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-dd" name="dnititular" placeholder="NÚMERO DE DOCUMENTO DEL TITULAR DE LA COBERTURA*" id="1249">
        </li>

        <li class="list-group-item input_afil_b">
            <div class="row">
                <div class="col-xs-6">
                    <label>FECHA DE NAC. DEL TITULAR</label> 
                </div>
                <div class="col-xs-6">
                    <input type="date" class="validar form-dd" name="fechanacimiento" id="1264">
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b">
        	<div class="row">
                <div class="col-xs-6">
                	<label>TIPO DE FACTURA QUE NECESITO </label>
                </div>
                <div class="col-xs-6">
                    <select id="btnTipoFactura" name="condicionimpo" class="validar form-pc">
                        <option value="0">SELECCIONE</option>
                        <option value="4185">FACTURA A (iva discriminado)</option>
                        <option value="4186">FACTURA B (iva no discriminado)</option>
                    </select>
            	</div>
            </div>
        </li>

        <!-- Factura A -------------------------------------------------------------------------->

        <div id="contCondImp">
            <li class="list-group-item input_afil_b" >
                <input type="text" class="validar form-dd" placeholder="NRO. DE CUIT*" name="cuit" id="1273">
            </li>

            <li class="list-group-item input_afil_b" >
                <input type="text" class="validar form-c" placeholder="NOMBRE Y/O RAZÓN SOCIAL*" name="empresa" id="Empresa">
            </li>
        </div>

        <!-- Fin de factura A -->

        <li class="list-group-item input_afil_b" >
            <label>DOMICILIO DE FACTURACIÓN</label>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" name="direccion" placeholder="CALLE*" id="Direccion">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-c" placeholder="NÚMERO*" name="direccionNumero"  id="Numero">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar form-c" placeholder="BARRIO*" name="barrio" id="Barrio">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar" placeholder="LOCALIDAD*" name="provincia" id="Localidad">
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarInt form-c" placeholder="C.P.*" name="postal" id="CPostal">
        </li>

        <li class="list-group-item input_afil_b" >
            <div class="row">
                <div class="col-xs-4">
                    <input class="validarInt form-c" type="text" placeholder="PREFIJO" name="pref" id="CaractTelefono">                         
                </div>
                <div class="col-xs-8">
                    <input class="validarInt form-c" type="text" placeholder="TELÉFONO" name="telefono" id="NumberTelefono">                            
                </div>
            </div>
        </li>

        <li class="list-group-item input_afil_b" >
            <input type="text" class="validar validarEmail form-c" name="email" placeholder="EMAIL*" id="E-mail">
        </li>

        <li class="list-group-item">
            <input id="enviar" type="submit" class="btnEnviar" value="ENVIAR">
        </li>
    </ul>
</form>