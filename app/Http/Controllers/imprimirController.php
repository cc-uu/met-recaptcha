<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class imprimirController extends Controller
{
   public function imprimir(Request $request){

   		if ($request->isMethod('post')) {
			$nafiliado = $request->input('nafiliado');
			$nombreapellido = $request->input('nombreapellido');
			$dnia = $request->input('dnia');
			$tipoa = $request->input('tipoa');
			$plan = $request->input('plan');
			$adicionales = $request->input('adicionales');
			$validezDesde = $request->input('validezDesde');
			$validezHasta = $request->input('validezHasta');
	        return view('gestiononline.credencial', compact('nafiliado', 'nombreapellido', 'dnia', 'tipoa', 'plan', 'adicionales', 'validezDesde', 'validezHasta'));
	     }
    }
}
