@extends('layouts.app')
@section('title', ' | Home')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div id="carouselPrincipal" class="carousel carousel-principal slide" data-ride="carousel">
        <div class="carousel-inner">

            @foreach ($SlideHomeArriba as $item)

                <div data-slide="{{$loop->index}}" class="carousel-item {{$loop->first ? 'active' : '' }}">
                    <div class="centrar-img">
                        <img src="{{ asset($item->imagenes[0]->path) }}">
                    </div>
                    <div class="container texto">
                        <div class="row">
                            <div class="col-10 col-sm-7 col-md-5 col-lg-4 col-xl-5">
                                <h1>{{$item->titulo}} </h1>
                                @if ($item->bajada != '')
                                <a href="{{$item->link}}" class="boton">{{$item->bajada}} </a>
                                @endif
                               
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

            {{-- <div class="carousel-item">
                <div class="centrar-img">
                    <img src="{{ asset('/images/slide01.jpg') }}">
                </div>
                <div class="container texto">
                    <div class="row">
                        <div class="col-10 col-sm-7 col-md-5 col-lg-4 col-xl-5">
                            <h1>Con MET tenés asegurada una cobertura completa para todas tus experiencias.</h1>
                            <a href="" class="boton">COBERTURA</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="centrar-img">
                    <img src="{{ asset('/images/slide01.jpg') }}">
                </div>
                <div class="container texto">
                    <div class="row">
                        <div class="col-10 col-sm-7 col-md-5 col-lg-4 col-xl-5">
                            <h1>Con MET tenés asegurada una cobertura completa para todas tus experiencias.</h1>
                            <a href="" class="boton">COBERTURA</a>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>  

        <div class="container">

            <ol class="carousel-indicators">

                @foreach ($SlideHomeArriba as $item)
               
                
            <li data-target="#carouselPrincipal" data-slide-to="{{$loop->index}}" class=" {{$loop->first ? "active" : "" }}"></li>
                @endforeach

            </ol>

            <div class="row">
                <div class="buscador-home col-10 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                    <form class="buscador"  action=" {{ route('buscador') }}"  method="post">
                        @csrf
                        <input type="text" name="valorabuscado" required>
                        <!-- <input type="submit" value="buscar"> -->   
                    </form>
                </div> 
            </div> 

            <div class="carousel-planes">
                
                
                <div id="carouselPlanes" class="carousel carousel-responsive slide" data-ride="carousel" data-items="1, 2, 3, 3, 3">
                    <h2>PLANES DE SALUD PARA LA FAMILIA</h2> 
                    <div class="carousel-inner">
                    
                        @foreach ($ContenidosCarrusel as $item)
                        
                     
                        
                    <div class="carousel-item {{$loop->first ? 'active' : '' }} "><!-- a este div agregarle la clase wellness si corresponde -->
                        @if($item->link)
                            <a href="{{$item->link}}">
                        @else
                            <a>
                        @endif
                                <div class="modulo-plan-home {{$item->tags == 'wellness' ? 'wellness' : '' }}">
                                    <h3>{{$item->titulo}}</h3>
                                    <div class="texto">
                                        {!!$item->descripcion!!}
                                        {{-- <ul>
                                            <li><strong>Odontología.</strong> Cartilla ampliada</li>
                                            <li><strong>Prestadores Exclusivos</strong></li>
                                            <li><strong>Internación.</strong> Habitación individual</li>
                                        </ul> --}}
                                        <span>Más info</span>
                                    </div>    
                                </div>                                
                            </a>
                        </div>

                        @endforeach
                        {{-- <div class="carousel-item">
                            <a href="">
                                <div class="modulo-plan-home">
                                    <h3>MTB+</h3>
                                    <div class="texto">
                                        <ul>
                                            <li><strong>Odontología.</strong> Cartilla ampliada</li>
                                            <li><strong>Prestadores Exclusivos</strong></li>
                                            <li><strong>Internación.</strong> Habitación individual</li>
                                        </ul>
                                        <span>más info</span>
                                    </div>
                                </div>                                
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="">
                                <div class="modulo-plan-home">
                                    <h3>MTC+</h3>
                                    <div class="texto">
                                        <ul>
                                            <li><strong>Odontología.</strong> Cartilla ampliada</li>
                                            <li><strong>Prestadores Exclusivos</strong></li>
                                            <li><strong>Internación.</strong> Habitación individual</li>
                                        </ul>
                                        <span>más info</span>
                                    </div>
                                </div>                                
                            </a>
                        </div> --}}
             
                    </div>
                    <a class="carousel-control-prev" href="#carouselPlanes" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>                        
                    </a>
                    <a class="carousel-control-next" href="#carouselPlanes" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>                        
                    </a>
                </div>
            </div> 

            <div class="zocalo">                
                <div class="col-12 col-lg-3 botones-zocalo">
                    <ul>
                        @foreach ($BotonesHome as $item)  
                            @if ($item->descripcion != '')

                                <li>
                                    <a href="{{$item->link}}">{!!$item->descripcion!!}</a>
                                </li>

                            @else 

                                <li>
                                    <a href="{{$item->link}}">
                                        <img src="{{ asset($item->imagenes[0]->path) }}">
                                    </a>
                                </li>
                                
                            @endif
                        

                        @endforeach
{{--                        
                        <li>
                            <a href="">BENEFICIOS <br><strong>MET</strong></a>
                        </li> --}}
                    </ul>
                </div>
                <div class="col-12 col-lg-9 carrusel">
                    <div id="carouselZocalo" class="carousel carousel-zocalo slide" data-ride="carousel">
                        
                        <div class="carousel-inner">
                            @foreach ($SlideHomeAbajo as $SlideHomeAbajoHijo)
                                @if ($loop->first)
                                    @foreach ($SlideHomeAbajoHijo->imagenes as $item)
                                        <div class="carousel-item @if ($loop->first) active @endif">
                                            <div class="centrar-img">
                                                 @if($item->nombre)
                                                    <a href="{{$item->nombre}}">
                                                @else
                                                    <a>
                                                @endif
                                                    <img src="{{ asset($item->path) }}">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                 @else
                                     @foreach ($SlideHomeAbajoHijo->imagenes as $item)
                                        <div class="carousel-item">
                                            <div class="centrar-img">
                                                 @if($item->nombre)
                                                    <a href="{{$item->nombre}}">
                                                @else
                                                    <a>
                                                @endif
                                                    <img src="{{ asset($item->path) }}">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                 @endif
                            @endforeach
                        </div>
                        <ol class="carousel-indicators">
                            @foreach ($SlideHomeAbajoHijo->imagenes as $item)
                                <li data-target="#carouselZocalo" data-slide-to="{{$loop->index}}" class=" {{$loop->first ? "active" : "" }}"></li>
                            @endforeach            
                        </ol>
                        <a class="carousel-control-prev" href="#carouselZocalo" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>                        
                        </a>
                        <a class="carousel-control-next" href="#carouselZocalo" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>                        
                        </a>
                    </div>
                </div>                
            </div>
            

@endsection