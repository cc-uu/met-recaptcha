@extends('layouts.app')
@section('title', ' | Buscador')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion fdo-buscador">     
        <div class="container encabezado-buscador">
            <h1>Buscador</h1>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <button class="chat">Chat <span>Online</span></button>

            <div class="buscador">
              <form  action=" {{ route('buscador') }}"  method="post">
                  @csrf
              <input type="text" name="valorabuscado" required>
              <!-- <input type="submit" value="enviar"> -->
          </form>
          </div> 
        </div>
         
        <div class="container main">
          <div class="row">
              <div class="col-12 col-lg-12 p-0 content-buscador">
                      <div class="row m-0">
                        <div class="resultados-busqueda">
                         
                          @if($Resultados != 'No se encontraron resultados')
                              @foreach ($Resultados as $item)
                                
                              @if ($loop->iteration == 1 || $loop->iteration == 6 || $loop->iteration == 11)
                                <div class="col-12 col-lg-4 resultados">

                              @endif

                                @switch($item['tipoContenido']['id'])
                                    {{-- PARA EL CASO DE LAS GESTIONES OLINE --}}
                                    
                                    @case(9852)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('plancorporativo') }}">ver más</a>
                                        </div>
                                        @break
                                    @case(9854)
                                        <div class="col-12 resultado">
                                          @if($item['id'] != 48257)
                                            <h3>{{ $item['titulo']}}</h3>
                                            @switch( $item['id'])
                                                {{-- Adhesion al debito --}}
                                                @case(48251)
                                                    <a href="{{ route('gestiononline') }}">ver más</a>
                                                    @break
                                                {{--Cambio de plan --}}
                                                @case(48253)
                                                    <a href="{{ route('cambioplan') }}">ver más</a>
                                                    @break
                                                {{-- contratacion de servicios adicionales --}}
                                                @case(48254)
                                                    <a href="{{ route('serviciosa') }}">ver más</a>
                                                    @break
                                                {{-- descaraga tu factura --}}
                                                @case(48255)
                                                    <a href="{{ route('mifactura') }}">ver más</a>
                                                    @break
                                                {{-- descarga tu credencial --}}
                                                @case(48256)
                                                    <a href="{{ route('micredencial') }}">ver más</a>
                                                      @break
                                                {{-- modifica tus datos de factura --}}
                                                @case(48257)
                                                      <a href="{{ route('modfactura') }}">ver más</a>
                                                      @break
                                                {{-- dar de baja --}}
                                                @case(48258)
                                                      <a href="{{ route('dardebaja') }}">ver más</a>
                                                      @break
                                                {{-- formularios onlin --}}
                                                @case(3282)
                                                      <a href="{{ route('formulariosonline') }}">ver más</a>
                                                      @break
                                                {{-- medios de pago --}}
                                                @case(49183)
                                                      <a href="{{ route('mediosdepagos') }}">ver más</a>
                                                      @break       
      
                                            @endswitch
                                         @endif
                                        </div>
                                        @break
                                        
                                    @case(9919)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('mediosdepagos') }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- PARA EL CASO DE LAS NOVEDADES --}}   
                                    @case(494)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('novedaDesplegada', ['id' => $item['id'] ,'titulo' => str_slug($item['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- PARA EL CASO DE LAS BENEFICIOS --}}      
                                    @case(462)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('beneficioDesplegado', ['id' => $item['id'] ,'titulo' => str_slug($item['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                        {{-- PARA EL CASO DE PLANES --}}    
                                    @case(3276)
                                        
                                          <div class="col-12 resultado">
                                            <h3>{{ $item['titulo']}}</h3>
                                            <a href="{{ route('plandesplegado', ['id' => $item['id'] ,'titulo' => str_slug($item['titulo']) ]) }}">ver más</a>
                                          </div>
                                
                                        @break
                                    
                                    @case(581)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('linkinteres') }}">ver más</a>
                                        </div>
                                        @break
                                          {{-- PARA EL CASO DE institucional --}}
                                    @case(9918)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('institucional') }}">ver más</a>
                                        </div>
                                        @break
                                        {{-- PARA EL CASO DE Contacto --}}
                                    @case(9853)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('contacto') }}">ver más</a>
                                        </div>
                                        @break
                                        {{-- PARA EL CASO DE centros medicos --}}
                                    @case(9979)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['titulo']}}</h3>
                                          <a href="{{ route('centrosmedicos') }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- wellnes premium  --}}     
                                    @case(8332)
                                          <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                  {{-- wellnes deluxe  --}}     
                                    @case(8333)
                                          <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- wellnes combinado --}}    
                                    @case(8334)
                                          <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- wellnes expres 1  --}}    
                                    @case(8335)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                    {{-- wellnes expres 2  --}}    
                                    @case(8337)
                                          <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                        {{-- wellnes expres 3  --}}
                                    @case(8338)
                                        <div class="col-12 resultado">
                                          <h3>{{ $item['tipoContenido']['titulo']}}</h3>
                                          <a href="{{ route('wellnessdesplegado', ['id' => $item['tipoContenido']['id'] ,'titulo' => str_slug($item['tipoContenido']['titulo']) ]) }}">ver más</a>
                                        </div>
                                        @break
                                        
                                  
                                        
                                @endswitch
                              
                              @if ($loop->iteration == 5 || $loop->iteration == 10 || $loop->iteration == 15)
                              </div>
                              @endif  
                            @endforeach
                          @else 
                          
                            <h3>{{ $Resultados}}</h3>
                          
                          @endif
            

                        </div>
                       
                          <nav aria-label="navigation">
                            @if($Resultados != 'No se encontraron resultados')
                            {!! $Resultados->links() !!}
                            @endif
                            {{-- <li class="prev"><a href=""><img src="{{ asset('/images/prev.png') }}"></a></li>
                            <li><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li class="next"><a href=""><img src="{{ asset('/images/next.png') }}"></a></li> --}}
                          {{-- </ul> --}}
                        </nav>
                       
                      </div>

        </div>
        <div class="container foot">

@endsection