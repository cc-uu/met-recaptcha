@extends('layouts.app')
@section('title', ' | Gestión Online - Formularios Online')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online formularios-online">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main bg-formonline">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Formularios Online</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a class="active">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>FORMULARIOS ONLINE</h2>
                    <div class="row m-0">
                        <div class="col-lg-7 col-md-10 intro">                            
                            
                            <div class="info-descarga">
                                <p>Para agilizar diversas gestiones, MET brinda a sus socios la opción de 
                                    <strong>descargar online</strong> formularios.</p>
                            </div>
                            @foreach ($Formularios as $item)
                            <div class="contenedor-descargas">
                            <h6>{{$item->titulo}}</h6>
                            <a href="{{$item->archivos[0]->path}}" target="_blank" download="{{$item->archivos[0]->nombre}}">
                                <button>DESCARGAR</button>
                            </a> 
                            </div>
                            @endforeach
                            {{-- <div class="contenedor-descargas">
                                <h6>FORMULARIO DE MEDICACIÓN CRÓNICA DIABETES</h6> <button>DESCARGAR</button>
                            </div>

                            <div class="contenedor-descargas">
                                <h6>FORMULARIO DE MEDICACIÓN CRÓNICA</h6> <button>DESCARGAR</button>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection