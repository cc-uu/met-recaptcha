<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendContact.
 */
class Send extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Request
     */
    public $request;

    /**
     * SendContact constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


    
	$subject = '';
//  Verifico el id del formulario solicitado y segun el id envio el email con asunto y direccion correspondientes
	switch($this->request['idform']){
       
        case 'plancorporativo':
          
            $subject = $this->request['cuit'].' - ' .'Plan Corporativo';
            $view = 'mail.fidelity';
            $to = array('ezalazar@metmedicinaprivada.com','jtissera@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;

        case 'contactoafiliado':
           
            $subject = $this->request['dni'].' - ' .'Contacto Afiliado';
            $view = 'mail.fidelity';
            $to = array('atencionalciente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;
        case 'contactonoafiliado':
           
            $subject = $this->request['dni'].' - ' .'Contacto No Afiliado';
            $view = 'mail.fidelity';
            $to = array('comercial@metmedicinaprivada.com', 'julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;

        case 'debitocuentabanco':
           
            $subject = $this->request['dnicobertura'].' - ' .'Debito Banco';
            $view = 'mail.fidelity';
            $to = array('atencionalcliente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;
        case 'debitocuentatarjeta':
           
            $subject = $this->request['dnicobertura'].' - ' .'Debito Tarjeta' ;
            $view = 'mail.fidelity';
            $to = array('atencionalcliente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;

        case 'cambiodeplan':
           
            $subject = $this->request['dnititularcobertura'].' - ' .'CAMBIO DE PLAN';
            $view = 'mail.fidelity';
            $to = array('atencionalcliente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;

        case 'contratacionserviciosa':
           
            $subject =  $this->request['dni'].' - '.'Contratación de servicios adicionales';
            $view = 'mail.fidelity';
            $to = array('atencionalcliente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;

        case 'dardebaja':
           
            $subject = $this->request['dni'].' - '.'SOLICITUD DE BAJA';
            $view = 'mail.fidelity';
            $to = array('atencionalcliente@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;
        case 'trabajaenmet':
           
            $subject = 'Trabaja en Met';
            $view = 'mail.fidelity';
            $to = array('reclutamiento@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
           
        break;
        case 'prestador':
           
            $subject = 'Queres se prestador';
            $view = 'mail.fidelity';
            $to = array('croldan@metmedicinaprivada.com','lmahl@metmedicinaprivada.com','julieta@sawubona.com.ar','paloma@sawubona.com.ar');
        break;
    
    
    }


  

// Funcion que envia el email 
    if ($this->request->file('archivo')){

        $CV = $this->request->file('archivo');
        $NombreArchivo = $CV->getClientOriginalName();
        $path = 'storage/CV/'.$NombreArchivo;
       


        $envioemail =  $this->to($to)
        ->view($view)
        ->subject($subject)
        ->from($this->request->email, $this->request->nombre)
        ->replyTo($this->request->email, $this->request->nombre)
        ->attach(public_path($path));

        return $envioemail;
    } else {
        $envioemail =  $this->to($to)
        ->view($view)
        ->subject($subject)
        ->from($this->request->email, $this->request->nombre)
        ->replyTo($this->request->email, $this->request->nombre);

        return $envioemail;

    }
       
   
    
   
        
    }
}