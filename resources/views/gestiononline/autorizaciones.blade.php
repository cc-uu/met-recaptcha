@extends('layouts.app')
@section('title', ' | Gestión Online - Autorizaciones')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')
    <header class="head">
        <div class="mwrap">
            <?php if (!empty($xMensaje)) {?>
                <div class="title"><?php echo $xMensaje; ?></div>
                <div class="title">Gracias!</div>
             <?php }?>
        </div>  
    </header>

    <div class="container-fluid fdo-seccion gestion-online autorizaciones">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Autorizaciones</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a class="active">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>{{$Autorizaciones->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-lg-5 intro mt-md-4">                            
                            {!!$Autorizaciones->descripcion!!}  
                        </div>
                        <div class="col-12 col-lg-7 formulario">                 
                           <p>Estamos trabajando para mejorar tu experiencia de usuario.</p>
                           <p>Si tenes que realizar una autorización, envíanos un mail a: <a href="mailto:autorizaciones@metmedicinaprivada.com" target="_blank">autorizaciones@metmedicinaprivada.com</a>,                 
                            adjuntando el pedido medico, y tus datos (nombre y apellido, DNI, e-mail y celular de contacto).</p>
                        </div>
                        <!--<div class="col-12 col-lg-7 formulario">                              

                            <form  action=" {{ route('autorizacionesPost') }}"  method="post">
                                @csrf
                                <input type="hidden" name="idform" value="dardebaja">
                                   <h3>FORMULARIO DE AUTORIZACIÓN</h3>
                                <div class="form-group">
                                    <label>Nombre*</label>
                                    <input type="text" name="nombre" id="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label>Apellido*</label>
                                    <input type="text" name="apellido" id="apellido" required>
                                </div>
                                <div class="form-group">
                                    <label>DNI*</label>
                                    <input type="text" name="dni" maxlength="8" id="dni" required>
                                </div>
                                <div class="form-group">
                                    <label>E-mail*</label>
                                    <input type="email" name="email" id="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Fecha del Turno</label>
                                    <input type="date" name="turno" maxlength="8" id="turno" >
                                </div><div class="form-group">
                                    <label>Prestador donde realizará la práctica*</label>
                                    <input type="text" name="prestador" maxlength="8" id="prestador" required>
                                </div>
                                <div class="form-group">                                                                      
                                    <input type="file" name="pedido" id="pedido" class="inputfile" accept="image/png, image/jpeg, application/pdf" required/>
                                    <label for="pedido">ADJUNTAR PEDIDO MÉDICO</label>
                                </div>
                                <h3 class="leyenda">*Datos obligatorios<br></h3>
                                    <button type="submit">ENVIAR</button>
                            </form>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection