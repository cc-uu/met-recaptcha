@extends('layouts.app')
@section('title', ' | Contacto')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion contacto">     
        <div class="container encabezado">
            <h1>CONTACTO</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-6 left">
                    
                    <div class="nav sucursales flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach ($DireccionesMaps as $item)
                            
                      
                        <a class="@if ($loop->first) active @else  @endif" id="v-pills-{{$item->idContenido}}-tab" data-toggle="pill" href="#v-pills-{{$item->idContenido}}" role="tab" aria-selected="true">
                        <span>{{$item->titulo}}</span>
                            {!!$item->descripcion!!}
                        </a>

                        @endforeach
                        {{-- <a class="" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-selected="false">
                            <span>Cerro de las Rosas</span>
                            <p>Av Rafael Nuñez 5021. Local 1 Atención L a V 9 a 18 hs.</p>
                        </a>
                        <a class="" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-selected="false">
                            <span>Villa Allende</span>
                            <p>Río de Janeiro 281 Local 2. Atención L a V 9 a 13 hs. / 14 a 18 hs.</p>
                        </a>                         --}}
                    </div>
                
                    <div class="tab-content" id="v-pills-tabContent">
                        @foreach ($DireccionesMaps as $item)
                        <div class="tab-pane fade show @if ($loop->first) active @else  @endif"" id="v-pills-{{$item->idContenido}}" role="tabpanel">
                            {!!$item->detalle!!}
                        </div>
                        @endforeach
                        {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2864.9793587660242!2d-64.24098124623977!3d-31.35749352382665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432994b232b6335%3A0x45d94940a716603d!2sCentro%20M%C3%A9dico%20MET%20CERRO!5e0!3m2!1ses!2sar!4v1583499240708!5m2!1ses!2sar" width="100%" height="348" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2027.2967471827806!2d-64.29298680783972!3d-31.290112737896955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94329d1e5d10af2b%3A0x913178cf4af4e6e5!2sMET%20Medicina%20Privada!5e0!3m2!1ses!2sar!4v1583499338915!5m2!1ses!2sar" width="100%" height="348" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>                         --}}
                    </div>
                        
                </div>
                <div class="col-12 col-sm-6 right"> 
                    <ul class="nav formularios nav-fill" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-selected="true">Soy Afiliado</a>
                        </li>
                        <li class="nav-item">
                            <a class="" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-selected="false">No soy Afiliado</a>
                        </li>                        
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel">
                            <h2>FORMULARIO DE CONTACTO</h2>
                            <form action=" {{ route('fomulario.enviar.afiliado') }}"  method="post" onsubmit="enviarFormulario('enviar')">
                                @csrf
                                <input type="hidden" name="idform" value="contactoafiliado">

                                <div class="form-group">
                                    <label>Nombre*</label>
                                    <input type="text" name="nombre" id="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label>Apellido*</label>
                                    <input type="text" name="apellido" id="apellido" required>
                                </div>
                                <div class="form-group">
                                    <label>E-mail*</label>
                                    <input type="email" name="email" id="email" required>
                                </div>
                                <div class="form-group">
                                    <label>DNI*</label>
                                    <input type="text" maxlength="8" name="dni" id="dni" required>
                                </div>                                
                                <div class="form-group">
                                    <label>Teléfono *</label>
                                    <input type="text" name="prefijo" id="prefijo" class="prefijo" required>
                                    <input type="text" name="telefono" id="telefono" class="telefono" required>
                                </div>
                                <div class="form-group">
                                    <label>Consulta</label>
                                    <textarea name="consulta" id="consulta" cols="30" rows="10" required></textarea>
                                </div>
                                <h3 class="leyenda">*Datos obligatorios</h3>
                                <div class="form-group">
                                    <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                                </div>
                                <button id="enviar" type="submit">ENVIAR</button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel">
                            <h2>SOLICITÁ UN ASESOR</h2>
                            <form action=" {{ route('fomulario.enviar.noafiliado') }}"  method="post" onsubmit="enviarFormulario('enviar2')">
                                @csrf
                                <input type="hidden" name="idform" value="contactonoafiliado">
                                <div class="form-group">
                                    <label>Nombre*</label>
                                    <input type="text" name="nombre" id="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label>Apellido*</label>
                                    <input type="text" name="apellido" id="apellido" required>
                                </div>
                                <div class="form-group">
                                    <label>Localidad*</label>
                                    <input type="text" name="localidad" id="localidad" required>
                                </div>
                                <div class="form-group">
                                    <label>Edad*</label>
                                    <input type="text" name="edad" id="edad" required>
                                </div>                                
                                <div class="form-group">
                                    <label>DNI*</label>
                                    <input type="text" name="dni" maxlength="8" id="dni" required>
                                </div>                                
                                <div class="form-group">
                                    <label>Teléfono*</label>
                                    <input type="text" name="prefijo" id="prefijo" class="prefijo" required>
                                    <input type="text" name="telefono" id="telefono" class="telefono" required>
                                </div>
                                <div class="form-group">
                                    <label>E-mail*</label>
                                    <input type="email" name="email" id="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Horario de contacto</label>
                                    <input type="text" name="horariocontacto" id="horariocontacto">
                                </div>
                                <div class="form-group">
                                    <label>Cantidad de hijos</label>
                                    <input type="number" name="canthijos" id="canthijos">
                                </div>
                                <div class="form-group opciones-radio">
                                    <label>¿Trabaja en relación de dependencia?</label>
                                    <label class="container-radio">sí
                                        <input type="radio" checked="checked" name="trabajadependecia" value="si">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-radio">no
                                        <input type="radio" name="trabajadependecia" value="no">
                                        <span class="checkmark"></span>
                                    </label>                                      
                                </div>
                                <div class="form-group opciones-radio">
                                    <label>¿Tiene cobertura?</label>
                                    <label class="container-radio">sí
                                        <input type="radio" checked="checked" name="tienecobertura" value="si">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-radio">no
                                        <input type="radio" name="tienecobertura" value="no">
                                        <span class="checkmark"></span>
                                    </label> 
                                </div>
                                <div class="form-group">
                                    <label>¿Cuál?</label>
                                    <input type="text" name="cual" id="cual">
                                </div>
                                <div class="form-group">
                                    <label>Consulta</label>
                                    <textarea name="consulta" id="consulta" cols="30" rows="10" required></textarea>
                                </div>
                                <h3 class="leyenda">*Datos obligatorios</h3>
                                <div class="form-group">
                                    <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                                </div>  
                                <button id="enviar2" type="submit">ENVIAR</button>
                            </form>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection