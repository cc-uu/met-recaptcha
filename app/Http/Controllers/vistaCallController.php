<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Formulario;

class vistaCallController extends Controller
{
	public function index(Request $request) {
	   	if ($request->isMethod('post')) {
			$SabePerfil = $_SERVER['REQUEST_URI'];
			$xDni 			= $request->input('dni');
			$xEmail 		= $request->input('email');
			$xMovil 		= $request->input('movil');
			$xPref3 		= $request->input('pref3');

			if (! $xEmail 
				|| ! $xMovil
				|| ! $xPref3
				|| ! $xDni
			) {
				print_r('Debe completar los campos obligatorios');
				echo "<script type='text/javascript'>function Redirect(){ alert('Debe completar los campos marcados como abligatorio.'); window.history.back(); } Redirect(); </script>";
			}
	
			$xPersona['nombre'] 		= $request->input('nombre') !== null ? $request->input('nombre') : null;
			$xPersona['apellido'] 		= $request->input('apellido') !== null ? $request->input('apellido') : null;
			$xPersona['email'] 			= $request->input('email') !== null ? $request->input('email') : null;
			$xPersona['observaciones'] 	= $request->input('mensaje') !== null ? $request->input('mensaje') : null;
			$xPersona['dni'] 			= $request->input('dni') !== null ? $request->input('dni') : null;
			$xPersona['pref1'] 			= $request->input('pref1') !== null ? $request->input('pref1') : null;
			$xPersona['telefono'] 		= $request->input('telefono') !== null ? $request->input('telefono') : null;
			$xPersona['pref2'] 			= $request->input('pref2') !== null ? $request->input('pref2') : null;
			$xPersona['telefono2'] 		= $request->input('telefono2') !== null ? $request->input('telefono2') : null;
			$xPersona['telefono3'] 		= $request->input('telefono3') !== null ? $request->input('telefono3') : null;
			$xPersona['pref3'] 			= $request->input('pref3') !== null ? $request->input('pref3') : null;
			$xPersona['movil'] 			= $request->input('movil') !== null ? $request->input('movil') : null;
			$xPersona['provincia'] 		= $request->input('provincia') !== null ? $request->input('provincia') : null;
			$xPersona['empresa'] 		= $request->input('empresa') !== null ? $request->input('empresa') : null;
			$xPersona['direccion'] 		= $request->input('direccion') !== null ? $request->input('direccion') : null;
			$xPersona['direccionNumero']= $request->input('direccionNumero') !== null ? $request->input('direccionNumero') : null;
			$xPersona['barrio'] 		= $request->input('barrio') !== null ? $request->input('barrio') : null;
			$xPersona['postal'] 		= $request->input('postal') !== null ? $request->input('postal') : null;
			$xPersona['localidad'] 		= $request->input('localidad') !== null ? $request->input('localidad') : null;
			$xPersona['origenWeb'] 		= $request->input('origenWeb') !== null ? $request->input('origenWeb') : null;
			$xPersona['sexo'] 			= $request->input('sexo') !== null ? $request->input('sexo') : null;
			$xPersona['proviene'] 		= $request->input('proviene') !== null ? $request->input('proviene') : null;
			$xPersona['idSegmento'] 	= $request->input('idSegmento') !== null ? $request->input('idSegmento') : null;

			$xPersona['datosDurosxSitioxPersona'][] = $request->input('edad') !== null ? ['idDatoDuro' 			=> 377, 'valor' => $request->input('edad') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('hatencion') !== null ? ['idDatoDuro' 		=> 376, 'valor' => $request->input('hatencion') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('hijos') !== null ? ['idDatoDuro' 			=> 445, 'valor' => $request->input('hijos') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('cobertura') !== null ? ['idDatoDuro' 		=> 375, 'valor' => $request->input('cobertura') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('coberturas') !== null ? ['idDatoDuro' 		=> 375, 'valor' => $request->input('coberturas') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('dnititular') !== null ? ['idDatoDuro' 		=> 1249, 'valor' => $request->input('dnititular') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('fechanacimiento') !== null ? ['idDatoDuro' => 1264, 'valor' => $request->input('fechanacimiento') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('dnicambio') !== null ? ['idDatoDuro' 		=> 1256, 'valor' => $request->input('dnicambio') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('motivo') !== null ? ['idDatoDuro' 			=> 1263, 'valor' => $request->input('motivo') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('planConsulta') !== null ? ['planConsulta' 	=> 1272, 'valor' => $request->input('planConsulta') ] : null;
			$xPersona['datosDurosxSitioxPersona'][] = $request->input('cuit') !== null ? ['idDatoDuro' 			=> 1273, 'valor' => $request->input('cuit') ] : null;

			$xPersona['perfilCustomxPersona'][] = $request->input('cambioplan') !== null ? ['idPerfil' 	=> $request->input('cambioplan') ] : null;
			$xPersona['perfilCustomxPersona'][] = $request->input('nuevoplan') !== null ? ['idPerfil' 	=> $request->input('nuevoplan') ] : null;
			$xPersona['perfilCustomxPersona'][] = $request->input('adicional') !== null ? ['idPerfil' 	=> $request->input('adicional') ] : null;
			$xPersona['perfilCustomxPersona'][] = $request->input('condicionimpo') !== null ? ['idPerfil'	=> $request->input('condicionimpo') ] : null;

			$xPersona['datosDurosxSitioxPersona'][] = $request->input('dependenciaSI') !== null ? ['idDatoDuro' => 446, 'valor' => $request->input('dependenciaSI') ] : null;
			$xPersona['asignadorAutomatico'] 		= $request->input('asignadorAutomatico') !== null ? TRUE : null;

			$xPersona['perfilCustomxPersona'] 		= $request->input('nombre') !== null ? $request->input('nombre') : null;

			$Archivos = isset($_FILES['archivo']) ? $_FILES['archivo'] : NULL;

			$xPersona['habilitado'] = 'S';
			
			$xFormulario = new Formulario();

			$resultado = $xFormulario->sendFormulario($xPersona, $Archivos);

			print_r($resultado);
		}
		return view('contacto.formularios.call');
	}			

}
