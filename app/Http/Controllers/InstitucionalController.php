<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
class InstitucionalController extends Controller
{
    public function index() {
        $Contenidos = new Contenido(); 
        $institucional = $Contenidos->getAll(9918,0,10);

        $intro = $Contenidos->getById(9918,48545);
        $mision = $Contenidos->getById(9918,48546);
        $sanatorio = $Contenidos->getById(9918,48547);
        $vision = $Contenidos->getById(9918,48548);
        $valores = $Contenidos->getById(9918,48550);
        $politicas = $Contenidos->getById(9918,48551);
        $certificados = $Contenidos->getById(9918,48552);

        return view ('institucional.index',compact('intro','mision','sanatorio','vision','valores','politicas','certificados'));
    }
}
