
@extends('layouts.app')
@section('title', ' | Cartilla Médica')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>

<script type="text/javascript">
    
var settings = {
  "url": "http://webservice.metmedicinaprivada.com//formulario/getEspecialidades?callback=JSON_CALLBACK",
  "method": "GET",
  "timeout": 0,
  "dataType": "jsonp",
};

$.ajax(settings).done(function (response) {
    for (var i = 0; i < response[0].length; i++) {
        $('#especialidadselect').append($('<option>', {
            value: response[0][i]['value'],
            text: response[0][i]['nombre']
        }));
    } 
});

var settings = {
  "url": "http://webservice.metmedicinaprivada.com//formulario/getLocalidades?callback=JSON_CALLBACK",
  "method": "GET",
  "timeout": 0,
  "dataType": "jsonp",
};

$.ajax(settings).done(function (response) {
    for (var i = 0; i < response[0].length; i++) {
        $('#localidadselect').append($('<option>', {
            value: response[0][i]['value'],
            text: response[0][i]['nombre']
        }));
    } 
});

var settings = {
  "url": "http://webservice.metmedicinaprivada.com//formulario/getPlanes?callback=JSON_CALLBACK",
  "method": "GET",
  "timeout": 0,
  "dataType": "jsonp",
};

$.ajax(settings).done(function (response) {
    for (var i = 0; i < response[0].length; i++) {
        $('#planselect').append($('<option>', {
            value: response[0][i]['value'],
            text: response[0][i]['nombre']
        }));
    } 
});

</script>

    <div class="container-fluid fdo-seccion cartilla-medica">     
        <div class="container encabezado">
            <h1>CARTILLA MÉDICA</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-5 p-0 left">
                    <div class="busqueda">
                        <ul>
                            <li>
                                <a class="activo">por Especialidad</a>
                            </li>
                            <li>
                                <a href="{{ route('geolocalizacion') }}">por Geolocalización</a>
                            </li>
                        </ul>
                        <div class="buscar-por">
                            <label>Buscar por:</label>
                            <select name="tipoBusqueda" id="tipoBusqueda" onchange="tipoBusqueda(this.value)">
                                <option value="institucion">INSTITUCIÓN</option>
                                <option value="farmacia">FARMACIA</option>
                                <option value="optica">ÓPTICA</option>
                                <option value="odontologia">ODONTOLOGÍA</option>
                                <option value="profesional">PROFESIONAL</option>
                            </select>
                        </div>
                        <form>
                        
                            <div class="form-group" id="institucion">
                                <label>Institución</label>
                                <input type="text" name="institucion" id="institucioninput" >
                            </div>
                            <div class="form-group" id="profesional"  style="display: none">
                                <label>Profesional</label>
                                <input type="text" name="profesional"  id="profesionalinput">
                            </div>
                            <div class="form-group" id="especialidad">
                                <label>Especialidad</label>
                                <select name="especialidad" id="especialidadselect" >
                                    <option value="" selected="selected">Seleccione</option>                         
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Localidad</label>
                                <select name="localidad" id="localidadselect" >
                                    <option value="" selected="selected">Seleccione</option>                                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Plan de Salud</label>
                                <select name="plansalud" id="planselect" >
                                    <option value="" selected="selected">Seleccione</option>                            
                                </select>
                            </div>
                            <button type="button" onclick="tomarvaloresBusqueda()">BUSCAR</button>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-sm-7 p-0 right">
                    <div class="resultante">

                        <div class="accordion acordion-resultantes" id="accordionResultantes">
               
                            {{-- <div class="modulo-resultante">
                                <div class="card-header" id="headingTwo">
                                    <a data-toggle="collapse" data-target="#collapseTwo">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingThree">
                                    <a data-toggle="collapse" data-target="#collapseThree">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span>
                                    </a>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingFour">                                
                                    <a data-toggle="collapse" data-target="#collapseFour">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span> 
                                    </a>                                  
                                </div>                          
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingFive">
                                    <a data-toggle="collapse" data-target="#collapseFive">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span>
                                    </a>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingSix">
                                    <a data-toggle="collapse" data-target="#collapseSix">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span>
                                    </a>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingSeven">                                
                                    <a data-toggle="collapse" data-target="#collapseSeven">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span> 
                                    </a>                                  
                                </div>                          
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modulo-resultante">
                                <div class="card-header" id="headingEight">
                                    <a data-toggle="collapse" data-target="#collapseEight">
                                        <h2>CENTRO GINECOLÓGICO ALLENDE PINTO</h2>  
                                        <p>Buenos aires 855<br>
                                            0351-4601055/4604177<br>
                                            Córdoba capital </p> 
                                        <span>Ver más</span>
                                    </a>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionResultantes">
                                    <div class="card-body">
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                        <p>- Lorem ipsum.</p>
                                    </div>
                                </div>
                            </div>                             --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

                
@endsection

