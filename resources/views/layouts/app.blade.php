<!DOCTYPE html>
<html lang="es">
<head>
	@include('layouts.head')
	@include('layouts.scripts.css')
</head>

<!-- Chat de Webcentrix -->
<script type="text/javascript">document.write('<script src="https://wcentrix.net/tabhelp/wctabs.aspx?id=809f1669c24744d3bc2292aa0d06e88d&' + new Date().getTime() + '"><\/script>');</script>

<body>
    

    @yield('contenido')
    @include('layouts.footer')
    @include('layouts.scripts.js')

    <!-- Chat de Tomnit -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
    <script src="https://omnicanalapi.tech/chat/js/omnicanalLib.js"></script>
    <script type="text/javascript">
      var token = "eyJ0aXR1bG8iOiJDSEFUIFZFTlRBUyIsImltZ0VtcHJlc2EiOiJodHRwOi8vMTM4Ljk5LjYuNjAvbG9nb3NDaGF0L01FVC5wbmciLCJpbWdVc3VhcmlvIjoiaHR0cHM6Ly9vbW5pY2FuYWxhcGkudGVjaC9jaGF0L2ltZy91c2VyLnBuZyIsInNlc3Npb25UaW1lb3V0IjoiNjA0ODAwIiwiY29sRGlhbG9nIjoiI2VmZmJmNCIsImNvbEVtcHJlc2EiOiIjMTIxMjEyIiwiY29sVXNlciI6IiMyNjU5NTIiLCJjb2xJbnBUeHQiOiIjM2U0MTQwIiwiY29sSW5wQmciOiIjZWVmM2Y2IiwiY29sSW5wRm9jdXMiOiIjMGVjODc5IiwiY29sQmFjazEiOiIjMGYxNjdiIiwiY29sQmFjazIiOiIjMzE1Mzg3IiwiY29sQmFjazMiOiIjMTc2NDgyIiwiY29sQmFjazQiOiIjMGZhMzc3IiwiY29sQmFjazUiOiIjMTlhOTk4IiwicGFnZUlkIjoiOGRaYXFOelcxY1ZXUHZya01CTU5hOVRvS01Nc2JPIn0=";
      window.api = new OmniCanalApi(token);
      //api.openElement();
    </script>
</body>

</html>
