@extends('layouts.app')
@section('title', ' | Error')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion pagina-error">
        <div class="container main">
            <div class="container encabezado">                
            </div> 
            <div class="top-mensaje">
                <div class="mensaje-error">
                    <span>ALGO SALIÓ MAL</span>
                </div>
            </div> 
        </div>  
        <div class="container foot">

@endsection