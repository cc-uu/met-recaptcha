<div class="row">
	<div class="col-xs-12 datos-credencial">
		<!-- N de Afiliado -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>N° de Afiliado</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
			  <p>{{credencial.nroCarnet}}</p>
			</div>
		</div>

		<!-- Apellido y Nombre -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>Apellido y Nombre</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
				<p>{{credencial.nombre}}</p>
			</div>
		</div>

		<!-- Tipo y nro de doc -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>Documento</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
				<p>{{credencial.documento.tipoDocumento.numero}} {{credencial.documento.numero}}</p>
			</div>
		</div>

		<!-- Tipo afiliado -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>Tipo de Afiliado</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
				<p>{{credencial.tipoAfiliado}}</p>
			</div>
		</div>

		<!-- Plan -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>Plan</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
				<p>{{credencial.planAfiliado}}</p>
			</div>
		</div>

		<!-- Adicionales -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-5 text-right">
				<p>Adicionales</p>
			</div>
			<div class="col-lg-7 text-left col-xs-7">
				<p>{{resultado[0]}} {{resultado[1]}} {{resultado[2]}} </p>
			</div>
		</div>

		<!-- Vigencia -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-3 col-xs-4 text-right">
				<p>Validez</p>
			</div>
			<div class="col-lg-4 col-xs-7">
				<p>Desde: {{credencial.desde}}</p>
			</div>
			<div class="col-lg-4 col-xs-7 col-xs-offset-4">
				<p>Hasta: {{credencial.hasta}}</p>
			</div>
		</div>

		<p class="frase text-center">{{credencial.mensaje}}</p>
	</div>

	<p class="text-center">{{credencial.mensaje}}</p>

	<div class="col-xs-12 datos-credencial no-imprimir">
		<div class="row">
			<div class="col-lg-4 col-xs-12 text-center">
				<button class="form-control no-imprimir" ng-click="imprimir()">Generar</button>
			</div>
			<div class="col-lg-4 col-xs-12 text-center">
				<!--<button class="form-control no-imprimir" ng-click="mail()">Enviar por e-mail</button>-->
			</div>
			<div class="col-lg-4 col-xs-12 text-center">
				<button class="form-control no-imprimir" ng-click="cancelar()">Cerrar</button>
			</div>
		</div>
	</div>
</div>