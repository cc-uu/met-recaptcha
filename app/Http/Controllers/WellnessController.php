<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Persona\Persona;
use App\Sawubona\Sawubona;

class WellnessController extends Controller
{
	public function index($id = 8332, $tituloSelect = null,  $gracias = null) {
		$Contenidos = new Contenido(); 
		$limite = 10; // limite de resultados devueltos.
		$offset = 0; //Variable que indica desde que posicion devuelve los resultados.

		switch ($id) {
			case 8332:
				$PlanesWellnes = $Contenidos->getAll(8332,$offset,$limite,'orden','ASC');
				$tituloSelect = 'PREMIUM';
				break;
			case 8333:
				$PlanesWellnes = $Contenidos->getAll(8333,$offset,$limite,'orden','ASC');
				$tituloSelect = 'DELUXE';
				break;
			case 8334:
				$PlanesWellnes = $Contenidos->getAll(8334,$offset,$limite,'orden','ASC');
				$tituloSelect = 'COMBINADO';
				break;
			case 8335:
				$PlanesWellnes = $Contenidos->getAll(8335,$offset,$limite,'orden','ASC');
				$tituloSelect = 'EXPRESS 1';
				break;
			case 8337:
				$PlanesWellnes = $Contenidos->getAll(8337,$offset,$limite,'orden','ASC');
				$tituloSelect = 'EXPRESS 2';
				break;
			case 8338:
				$PlanesWellnes = $Contenidos->getAll(8338,$offset,$limite,'orden','ASC');
				$tituloSelect = 'EXPRESS 3';
				break;
			case 8338:
				$PlanesWellnes = $Contenidos->getAll(8338,$offset,$limite,'orden','ASC');
				$tituloSelect = 'EXPRESS 3';
				break;
			case 10741:
				$PlanesWellnes = $Contenidos->getAll(10741,$offset,$limite,'orden','ASC');
				$tituloSelect = 'MODULO CORPORAL 1 (Ideal Post- Parto)';
				break;
			case 10740:
				$PlanesWellnes = $Contenidos->getAll(10740,$offset,$limite,'orden','ASC');
				$tituloSelect = 'MODULO CORPORAL 2';
				break;
			case 10743:
				$PlanesWellnes = $Contenidos->getAll(10743,$offset,$limite,'orden','ASC');
				$tituloSelect = 'MODULO DEPILACIÓN FULL';
				break;
			case 10744:
				$PlanesWellnes = $Contenidos->getAll(10744,$offset,$limite,'orden','ASC');
				$tituloSelect = 'MODULO FACIAL FULL';
				break;
			default:
				break;
		}
		$titulo = $tituloSelect;
	
		if ($gracias != null) {
			return view('wellness.gracias',compact('PlanesWellnes','tituloSelect', 'titulo', 'gracias', 'id'));
		}
		return view('wellness.index',compact('PlanesWellnes','tituloSelect', 'gracias', 'titulo', 'id'));
	}

	public function wellnessForm(Request $request) {
		$response = Sawubona::getCaptcha($_POST);
		if (!$response) {  
			return view('wellness');
		}
		$persona = new Persona();
		$persona->AgregaPersonaPlanes($request, 7013, 'Pagina_Web', 'info wellness');
		$id = $request->id;

		return redirect()->route('wellnessdesplegado', ['id' => $id, 'tituloSelect' => 'null', 'gracias' => 'gracias']);
	}

	

}