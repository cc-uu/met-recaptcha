@extends('layouts.app')
@section('title', ' | Trabajá en MET')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion trabaja-en-met">     
        <div class="container encabezado">
            <h1>TRABAJÁ EN MET</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
           
                <form class="row small-checks" action=" {{ route('formulario.enviar.trabajaenmet') }}"  method="post" enctype="multipart/form-data" onsubmit="enviarFormulario('enviar')">
                    @csrf
                    <input type="hidden" name="idform" value="trabajaenmet">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 left">                    
                    <h3>DATOS PERSONALES</h3>
                
                    <div class="form-group">
                        <label>Nombre*</label>
                        <input type="text" name="nombre" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label>Apellido*</label>
                        <input type="text" name="apellido" id="apellido" required>
                    </div>
                    <div class="form-group">
                        <label>Email*</label>
                        <input type="email" name="email" id="email" required>
                    </div>
                    <div class="form-group">
                        <label>Provincia</label>
                        <input type="text" name="provincia" id="provincia">
                    </div>
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" name="prefijo" id="prefijo" class="prefijo" >
                        <input type="text" name="telefono" id="telefono" class="telefono" >
                    </div>
                
                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-7 right">
                    <div class="row">
                        <div class="col-lg-8 col-md-6 col-sm-6">
                            <h3>PUNTOS DE INTERÉS</h3>
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group opciones-radio">                                            
                                        <label class="container-radio">CALIDAD
                                            <input type="checkbox" checked="checked" name="checkPI[]" value="40076">
                                            <span class="checkmark"></span>
                                        </label>                                          
                                        <label class="container-radio">MARKETING
                                            <input type="checkbox" name="checkPI[]" value="40077">
                                            <span class="checkmark"></span>
                                        </label> 
                                        <label class="container-radio">RECURSOS HUMANOS
                                            <input type="checkbox" name="checkPI[]" value="40078">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container-radio">SISTEMAS
                                            <input type="checkbox" name="checkPI[]" value="40079">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6 pl-0">
                                    <div class="form-group opciones-radio">  
                                        <label class="container-radio">VENTAS
                                            <input type="checkbox" name="checkPI[]" value="40080">
                                            <span class="checkmark"></span>
                                        </label>                                          
                                        <label class="container-radio">AT. AL CLIENTE
                                            <input type="checkbox" name="checkPI[]" value="40081">
                                            <span class="checkmark"></span>
                                        </label> 
                                        <label class="container-radio">CONTROL DE GESTIÓN
                                            <input type="checkbox" name="checkPI[]" value="40082">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container-radio">ADMINISTRACIÓN
                                            <input type="checkbox" name="checkPI[]" value="40085" >
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container-radio">PRESTACIONES MÉDICAS
                                            <input type="checkbox" name="checkPI[]" value="40083">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 offset-md-0 col-sm-5 offset-sm-1">
                            <h3 class="derecha">PREFERENCIA</h3>
                            <div class="form-group opciones-radio">  
                                <label class="container-radio">FULL TIME
                                    <input type="checkbox" checked="checked" name="checkP[]"  value="40086">
                                    <span class="checkmark"></span>
                                </label>                                          
                                <label class="container-radio">POR LA MAÑANA
                                    <input type="checkbox" name="checkP[]"  value="40087">
                                    <span class="checkmark"></span>
                                </label> 
                                <label class="container-radio">POR LA TARDE
                                    <input type="checkbox" name="checkP[]"  value="40088">
                                    <span class="checkmark"></span>
                                </label>                                    
                            </div>
                            <div class="form-group">
                                <input type="file" name="archivo" id="archivo" class="inputfile" />
                                <label for="archivo">ADJUNTAR CV</label>
                            </div>
                            <h3 class="leyenda">*datos obligatorios</h3>
                            <div class="form-group">
                                <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                            </div>
                            <button  id="enviar" type="submit">ENVIAR</button>
                        </div>
                    </div>
                </div>
            </form>  
            <div class="row bg-direccion">
                <div class="col-12 p-0">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2024.4038881470808!2d-64.18830752323207!3d-31.424375324461426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432a28f428afa4b%3A0x5c346b45d4ebbefc!2sMET%20Medicina%20Privada!5e0!3m2!1ses!2sar!4v1587561383466!5m2!1ses!2sar" width="100%" height="322" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>

                    <div class="col-lg-4 col-md-12 border-direc">
                        <div class="direccion">
                            <h6>DIRECCIÓN</h6>
                            <P>Obispo Oro 50 <br>
                                Córdoba Capital (CP 5000)<br>
                                Argentina</P>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 border-tel">
                        <div class="telefono">
                            <h6>TELÉFONOS</h6>
                            <a href="tel:0800 3450 638"><strong>Centro de atención </strong>0800 3450 638</a><br>
                            <a href="https://api.whatsapp.com/send?phone=35180007777"><strong> WhatsApp</strong> 351 80007777</a>
                        </div>  
                    </div>
                    <div class="col-lg-4 col-md-12 border-mail">
                        <div class="mail">
                            <h6>E-MAIL</h6>
                            <a href="mailito:atencionalcliente@metmedicinaprivada.com">atencionalcliente@metmedicinaprivada.com</a>
                        </div>
                    </div>    
            </div>  
        </div>  
        <div class="container foot">

@endsection