<?php $this->renderPartial('/atencion/menu', array('xPagina' => 'preguntas')); ?>

<div class="container-fluid descargas_top">
	<div class="container sp">
   	  <div class="col-xs-5 cont_atencion">
        	<h1>PREGUNTAS FRECUENTES</h1>
            <p>En esta sección podrá consultar información básica a saber sobre nuestros servicios.</p>
        </div>
    </div>
</div>

<?php
if(isset($vFaqs)){
    if($vFaqs != null){
        foreach ($vFaqs as $xFaq) { ?>
            <div class="container-fluid modulo_pregunta">
            	<div class="container sp">
                	<div class="col-xs-10">
                    	<a data-toggle="collapse" href="#<?php echo $xFaq->idContenido ?>" aria-expanded="false" aria-controls="desarrollo_pregunta">
                            <h2><?php echo $xFaq->titulo; ?></h2>
                        </a>
                    </div>
                    <div class="col-xs-2">
                    	<a class="btn_desc" data-toggle="collapse" href="#<?php echo $xFaq->idContenido ?>" aria-expanded="false" aria-controls="desarrollo_pregunta">+</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid modulo_desarrollo collapse" id="<?php echo $xFaq->idContenido ?>">
            	<div class="container sp">
                	<div class="col-xs-10">
                		<?php echo $xFaq->descripcion; ?>
                    </div>
                </div>
            </div>
<?php   }
    }
} ?>