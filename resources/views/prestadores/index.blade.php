@extends('layouts.app')
@section('title', ' | Querés ser prestador')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion trabaja-en-met prestadores">     
        <div class="container encabezado">
            <h1>¿QUERÉS SER PRESTADOR?</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <form class="row small-checks left"  action=" {{ route('fomulario.enviar.prestador') }}" method="post" enctype="multipart/form-data" onsubmit="enviarFormulario('enviar')"> 
                @csrf
                <input type="hidden" name="idform" value="prestador">
                <div class="col-12 mb-4">                   
                    <h3>DATOS PERSONALES</h3>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label>Nombre*</label>
                        <input type="text"  name="nombre" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label>Apellido*</label>
                        <input type="text" name="apellido" id="apellido" required>
                    </div>
                    <div class="form-group">
                        <label>E-mail*</label>
                        <input type="text" name="email" id="email" required>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label>Provincia</label>
                        <input type="text" name="provincia" id="provincia">
                    </div>
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" name="prefijo" id="prefijo" class="prefijo" >
                        <input type="text" name="telefono" id="telefono" class="telefono" >
                    </div>
                    <div class="form-group">
                        <label>Arancel Sugerido*</label>
                        <input type="text" name="arancel" id="arancel">
                    </div>
                </div>
                <div class="col-md-2 btn-prestadores">
                        <input type="file" name="archivo" id="archivo" class="inputfile" />
                        <label for="archivo">ADJUNTAR CV</label>
                    <button  id="enviar" class="btn-enviar" type="submit">ENVIAR</button>
                </div>
                <div class="form-group">
                    <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                </div>
            </form>  
            <div class="row bg-direccion">
                <div class="col-12 p-0">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2024.4038881470808!2d-64.18830752323207!3d-31.424375324461426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432a28f428afa4b%3A0x5c346b45d4ebbefc!2sMET%20Medicina%20Privada!5e0!3m2!1ses!2sar!4v1587561383466!5m2!1ses!2sar" width="100%" height="322" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>

                    <div class="col-lg-4 col-md-12 border-direc">
                        <div class="direccion">
                            <h6>DIRECCIÓN</h6>
                            <P>Obispo Oro 50 <br>
                                Córdoba Capital (CP 5000)<br>
                                Argentina</P>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 border-tel">
                        <div class="telefono">
                            <h6>TELÉFONOS</h6>
                            <a href="tel:0800 3450 638"><strong>Centro de atención </strong>0800 3450 638</a><br>
                            <a href="https://api.whatsapp.com/send?phone=35180007777"><strong> WhatsApp</strong> 351 80007777</a>
                        </div>  
                    </div>
                    <div class="col-lg-4 col-md-12 border-mail">
                        <div class="mail">
                            <h6>E-MAIL</h6>
                            <a href="mailito:atencionalcliente@metmedicinaprivada.com">atencionalcliente@metmedicinaprivada.com</a>
                        </div>
                    </div>    
            </div>  
        </div>  
        <div class="container foot">

@endsection