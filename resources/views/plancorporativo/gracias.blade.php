@extends('layouts.app')
@section('title', ' | Plan Corporativo')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion plan-corporativo gracias">     
        <div class="container encabezado">
            <h1>PLAN CORPORATIVO</h1><br>
            <a href="javascript:history.go(-1)">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-5 left">
                    @foreach ($ContenidoCorporativo as $item)
                   
                        @if ($item->idContenido == 48244)
                            <div class="intro">
                        @else 
                            <div class="puntos">
                        @endif 
                                <h2>{{$item->titulo}}</h2>
                                {!!$item->descripcion!!}      
                            </div>

                    @endforeach
                </div>
                <div class="col-12 col-sm-7 right">                    
                    <div class="mensaje-gracias">
                        <span>"Gracias. A la brevedad <br>nos contactaremos con usted"</span>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection