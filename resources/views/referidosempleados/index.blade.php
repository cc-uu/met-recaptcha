<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23691022-1']);
  _gaq.push(['_setDomainName', 'metmedicinaprivada.com']);
  _gaq.push(['_trackPageview']);



  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/libs/jquery-2.1.3.min.js') }}"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos-landing-referidos-empleados.css') }}" />
<script type="text/javascript" src="{{ asset('/js/vendor-referidos-empleados.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/main-referidos-empleados.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/piecu.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{asset('/js/front.js')}}"></script> -->

@include('layouts.headLanding')
<header class="head">
    <div class="mwrap">
        <div class="title">Formulario</div>
    </div>
</header>
<div class="banner">
    <div class="banner-image bg-image" style="background-image: url(images/landing-referidos-empleados/slide1.jpg);">
        <div class="banner-content">
            <div class="mwrap">
                <div class="add-wrapper">
                    <div class="box-text box-text-1">
                        <div class="wrap">
                            <div class="text-1">METé</div>
                            <div class="text-2">un afiliado</div>
                        </div>
                    </div>
                    <div class="box-text box-text-2">
                        <div class="wrap">
                            <div class="text-1">y METé</div>
                            <div class="text-2">bono</div>
                        </div>
                    </div>
                    <div class="box-text box-text-3">
                        <div class="text-1">Referí MET y</div>
                        <div class="text-2">
                            <div class="line-1">
                                ganá hasta
                            </div>
                            <div class="line-2">
                                $1500
                            </div>
                        </div>
                        <div class="text-3">*El premio se otorga si el referido contrata MET</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="sect-form">
    <div class="mwrap">
        <div class="form">
            <form class="main-form" action="{{ route('referidosEmpleadosPost') }}" method="post" onsubmit="enviarFormulario('enviar')">
                @csrf
                <div class="grid-wrapper">
                    <div class="grid-12">
                        <div class="subtitle">
                            <span>Tus datos</span>
                        </div>
                        <div class="field-wrap">
                            <label for="firstname">Nombre:</label>
                            <input type="text" class="required" required id="nombreR" name="nombreR">
                        </div>
                        <div class="field-wrap">
                            <label for="lastname">Apellido:</label>
                            <input type="text" class="required" required id="apellidoR" name="apellidoR">
                        </div>
                        <div class="field-wrap">
                            <label for="dni">DNI:</label>
                            <input type="text" class="required" required id="dniR" name="dniR">
                        </div>
                        <div class="field-wrap">
                            <label for="plan">Nombre de tu plan:</label>
                            <input type="text" class="required" required id="planR" name="planR">
                        </div>
                        <div class="field-wrap">
                            <label for="email">Mail:</label>
                            <input type="email" class="required" required id="emailR" name="emailR">
                        </div>
                        <div class="field-wrap">
                            <label for="contact">Número de contacto:</label>
                            <input type="tel" class="required" required id="pref1R" name="pref1R" placeholder="0-">
                            <input type="tel" class="required" required id="telefonoR" name="telefonoR" placeholder="15">
                        </div>
                    </div>
                    <div class="grid-12">
                        <div class="subtitle">
                            <span>Datos del referido</span>
                        </div>
                        <div class="field-wrap">
                            <label for="firstname">Nombre:</label>
                            <input type="text" class="required" required id="nombre" name="nombre">
                        </div>
                        <div class="field-wrap">
                            <label for="lastname">Apellido:</label>
                            <input type="text" class="required" required id="apellido" name="apellido">
                        </div>
                        <div class="field-wrap">
                            <label for="dni">DNI:</label>
                            <input type="text" class="required" required id="dni" name="dni">
                        </div>
                        <div class="field-wrap">
                            <label for="email">Mail:</label>
                            <input type="email" class="required" required id="email" name="email">
                        </div>
                        <div class="field-wrap">
                            <label for="contact">Número de contacto:</label>
                            <input type="tel" class="required" required id="pref1" name="pref1" placeholder="0-">
                            <input type="tel" class="required" required id="telefono" name="telefono" placeholder="15">
                        </div>
                    </div>
                    <input hidden="hidden" type="text" id="origen" name="origen" value="Campaña de Referidos">
                    <div class="grid-12">
                        <div class="field-submit">
                            <button id="enviar" type="submit"><span>Enviar</span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<footer class="foot">
    <div class="mwrap">
        <div class="logo">
            <img src="{{ asset('images/landing-referidos-empleados/logo.png') }}" alt="Logo de MET">
        </div>
        <div id="piecu"></div>
    </div>
</footer>