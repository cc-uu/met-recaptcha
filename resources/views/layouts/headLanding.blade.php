<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />

<meta name="description" content="Somos una empresa de Medicina Prepaga con servicios de excelencia y constante preocupación por el bienestar de nuestros afiliados. Esto nos trascender la mera calidad médica con el respaldo y la experiencia de dos instituciones líderes: Sanatorio Allende y MEDICUS.  Independencia 714. CÓRDOBA, ARGENTINA | 0800-3450-638 (MET) " />

<meta name="keywords" content="Met medicina, met prepaga, medicina prepaga, salud prepaga, consultorio met, met, medicina córdoba, sanatorio allende, planes de salud met, centro de atencion met, 0800 met, centro médico met, planes de salud met" />

<link rel="shortcut icon" href="{{ asset('/imagenes/favicon.png') }}">	

<!-- Facebook Pixel Code - agregado el 20/11/18 -->

<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '790532271137802');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=790532271137802&ev=PageView&noscript=1"
/></noscript>

<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Ads: 822039997  - agregado el 20/11/18-->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-822039997"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-822039997');
</script>