@extends('layouts.app')
@section('title', ' | Gestión Online - Adhesión al débito')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.go(-1)">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Adhesión al débito</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a class="active">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                   
                <h2>{{$AdhesionDebito->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-md-10 col-lg-5 intro">
                            <h3>{{$AdhesionDebito->bajada}}</h3>
                            {!!$AdhesionDebito->descripcion!!}
                            
                            <ul class="nav botones" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="active" id="cuenta-tab" data-toggle="tab" href="#cuenta" role="tab">POR CUENTA BANCARIA</a>
                                </li>
                                <li class="nav-item">
                                    <a class="" id="tarjeta-tab" data-toggle="tab" href="#tarjeta" role="tab">POR TARJETA DE CRÉDITO</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-10 col-lg-7 formulario">
                            <div class="tab-content">
                                <div class="tab-pane active" id="cuenta">
                                    <iframe src="http://afiliados2.metmedicinaprivada.com/debito_cuenta.asp" frameborder="0" width="100%" height="800"></iframe>
                                    
                                    <!--<h3>DÉBITO AUTOMÁTICO POR CUENTA BANCARIA</h3>                                    
                                 
                                        <form class="vertical" action=" {{ route('fomulario.enviar.debitobanco') }}"  method="post">
                                            @csrf
                                            <input type="hidden" name="idform" value="debitocuentabanco">
                                        <div class="form-group">
                                            <label>Número de Documento del Titular de la Cobertura *</label>
                                            <input type="text" maxlength="8" name="dni" id="dni" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de Documento del Titular de la Cuenta *</label>
                                            <input type="text" maxlength="8" name="dnititularcuenta" id="dnititularcuenta" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nombre del Titular de la Cuenta Bancaria *</label>
                                            <input type="text" name="nombre" id="nombre" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tipo de Cuenta*</label>
                                            <select name="tipocuenta" id="tipocuenta" required>
                                                <option value="" selected="selected">Seleccione</option>
                                                <option value="Caja de ahorro">Caja de ahorro</option>
                                                <option value="Cuenta corriente">Cuenta corriente</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de Cuenta *</label>
                                            <input type="text" name="numerocuenta" maxlength="15" id="numerocuenta" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de CBU <span>(22 dígitos, sin guiones ni espacios)</span> *</label>
                                            <input type="text" maxlength="22" name="numerocbu" id="numerocbu" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de Teléfono *</label>
                                            <input type="text" name="telefono" maxlength="15" id="telefono" required>
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail *</label>
                                            <input type="email" name="email" id="email" required>
                                        </div>
                                        <h3 class="leyenda">*Datos obligatorios<br>
                                            **A partir del mes de {{ $nombreMes}}, la factura correspondiente a su cobertura será debitada de su Cuenta Bancaria. Recibirá la confirmación de su trámite una vez aprobado el mismo.</h3>
                                            <button type="submit">ENVIAR</button>
                                    </form>-->
                                </div>
                                <div class="tab-pane" id="tarjeta">
                                    <iframe src="http://afiliados2.metmedicinaprivada.com/debito_tarjeta.asp" frameborder="0" width="100%" height="900"></iframe>
                                    <!--<h3>DÉBITO AUTOMÁTICO POR TARJETA DE CRÉDITO</h3>
                                    <form class="vertical" action=" {{ route('fomulario.enviar.debitotarjeta') }}"  method="post">
                                        @csrf
                                        <input type="hidden" name="idform" value="debitocuentatarjeta">
                                        <div class="form-group">
                                            <label>Número de Documento del Titular de la Cobertura *</label>
                                            <input type="text" name="dni" maxlength="8" id="dni" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de Documento del Titular de la Tarjeta *</label>
                                            <input type="text" maxlength="8" name="dnititulartarjeta" id="dnititulartarjeta" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nombre del Titular de la Tarjeta <span>(como figura en la tarjeta.)</span> *</label>
                                            <input type="text" name="nombre" id="nombre" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Banco *</label>
                                            <input type="text" name="banco" id="banco" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tarjeta de Crédito*</label>
                                            <select name="tarjeta" id="tarjeta" required>
                                                <option value="Seleccione" selected="selected">Seleccione</option>
                                                <option value="Naranja">Naranja</option>
                                                <option value="Visa">Visa</option>
                                                <option value="Mastercard">Mastercard</option>
                                                <option value="Cordobesa">Cordobesa</option>
                                            </select>                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Número de la Tarjeta *</label>
                                            <input type="text" maxlength="16" name="numerotarjeta" id="numerotarjeta" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Vencimiento de la Tarjeta *</label>
                                            <input type="text" maxlength="4" name="vencimientotarjeta" id="vencimientotarjeta" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Número de Teléfono *</label>
                                            <input type="text" name="telefono" id="telefono" required>
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail *</label>
                                            <input type="email" name="email" id="email" required>
                                        </div>
                                        <h3 class="leyenda">*Datos obligatorios<br>
                                            **A partir del mes de {{ $nombreMes}}, la factura correspondiente a su cobertura será debitada de su Tarjeta de Crédito. Recibirá la confirmación de su trámite una vez aprobado el mismo.</h3>
                                            <button type="submit">ENVIAR</button>
                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection