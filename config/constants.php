<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    // */
    //Example
    // 'company_name' => env('COMPANY_NAME','Acme Inc'),
    // 'company_email' => env('COMPANY_email','contact@acme.inc'),
    //Example PHP
    //Config::get('constants.company_name')
    //config('constants.company_name')
    //Example BLADE
    //{{ config('constants.company_email') }}
    'urlApi'                => 'http://apirest.fidelitytools.net/api/',
    'Key'                   => '265c092ff1813072ffeb07ec2ab84e4d',
    'idSitio'               => '106',
    'extensiones'           => ['doc', 'docx', 'pdf','jpg','jpeg','png'],
    'idAnalytics'           => 'UA-23691022-1',
];