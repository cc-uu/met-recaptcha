<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
use App\Clases\Common\Paginador;
use Illuminate\Pagination\LengthAwarePaginator;
class NovedadesController extends Controller
{
    public function index() {
        $Contenidos = new Contenido(); 
        $Paginador = new Paginador(); 
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $NovedadesDestacadas = $Contenidos->getAllDestacados(494,0,3);
        $TNovedades = $Contenidos->getAllNoDestacados(494,0,1000);
        $Novedades = $Paginador->getPaginador($TNovedades,6,$currentPage);
        
     
        return view ('novedades.index',compact('Novedades','NovedadesDestacadas'));
    }

    public function novedadAmpliada($id,$titulo) {
        $Contenidos = new Contenido(); 
        $NovedadesById = $Contenidos->getByID(494,$id);
        $Novedades = $Contenidos->getAll(494,0,100);
        
     
        return view ('novedades.novedad-ampliada',compact('NovedadesById','Novedades'));
    }
}
