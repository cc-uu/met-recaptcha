@extends('layouts.app')
@section('title', ' | Institucional')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion plan-institucional">     
        <div class="container encabezado">
            <h1>INSTITUCIONAL</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <div class="btn-institucional">
             <a href="{{ route('trabajaenmet') }}"><button>TRABAJÁ EN MET</button></a>   
             <a href="{{ route('prestador') }}"> <button>¿QUERÉS SER PRESTADOR?</button></a>  
            </div>
        </div> 
       
            
      
        <div class="container main">
           
            
            <div class="row">
                <div class="col-12 col-sm-7 col-md-6 left">
                  
                    <div class="intro">
                        <div class="info-institucional">
                           
                           {!! $intro->descripcion !!}
                            {{-- <p>
                                Somos una empresa de Medicina Prepaga con servicios de excelencia y constante preocupación
                                por el bienestar de nuestros afiliados. Esto nos hace trascender la mera calidad médica 
                                con el respaldo y la experiencia de dos instituciones líderes: Sanatorio Allende y MEDICUS.
                            </p>
                            <p>
                                Los servicios de MET Medicina Privada presentan características únicas, como el acceso a 
                                profesionales médicos Jefes de Servicio de Sanatorio Allende; cobertura en todas las 
                                especialidades médicas (incluida alta complejidad); consultorios externos propios; cobertura 
                                nacional e internacional (pacientes en tránsito), y reconocidos profesionales de primer nivel y 
                                amplia experiencia. 
                            </p> --}}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-5 col-md-6 p-0 right"> 
                    <div class="centrar-img">
                        <img src="{{ asset($intro->imagenes[0]->path) }}" alt="">
                    </div>
                </div>
               
            </div>
           
            

            
            <div class="row bg-mision">
                <div class="col-lg-7">
                    <h2>{{$mision->titulo}}</h2>
                    <div class="info-mision">
                        {!!$mision->descripcion!!}
                        {{-- <p> Participar en el mercado de la Medicina Prepaga para satisfacer la demanda social 
                            existente para este servicio, contribuyendo a resolver la necesidad que en este sentido
                            tiene la comunidad, ofreciendo un producto adecuado a estos fines sobre la base de la
                            calidad del servicio y que permita lograr rentabilidad a la Empresa y un ambiente de 
                            trabajo satisfactorio y seguro para su personal. 
                        </p> --}}
                    </div>
                </div>
            </div>
            
           
            <div class="row bg-vision">
               
                <div class="col-sm-8 p-0">
                    <div class="centrar-img">
                        <img src="{{ asset($sanatorio->imagenes[0]->path) }}" alt="">
                    </div>
                </div>
                <div class="col-sm-4 p-0">
                    <div class="bg-gris">
                        <p><strong>{{$sanatorio->titulo}}</strong><br class="d-none d-sm-block">{{$sanatorio->bajada}}</p>
                    </div>
                </div>
               
               
                <div class="col-12">
                    <h2 class="mt-4">{{$vision->titulo}}</h2>
                </div>
                <div class="col-md-6">
                    @php
                    $descripcionesVision = explode("<hr />", $vision->descripcion);
                     @endphp
            
                {!! $descripcionesVision[0] !!}
                </div>
                <div class="col-lg-5 col-md-6">
                    {!! $descripcionesVision[1] !!}
                </div>
                
            </div>
         
           
            <div class="row bg-valores">
                <div class="col-12 mb-4">
                    <h2>{{$valores->bajada}}</h2>
                </div>
                     @php
                        $descripcionesValores = explode("<hr />", $valores->descripcion);
                     @endphp
                <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! $descripcionesValores[0] !!}
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! $descripcionesValores[1] !!}
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! $descripcionesValores[2] !!}
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! $descripcionesValores[3] !!}
                </div>
            </div>
          
            
            <div class="row bg-certificados">
                <div class="certificado">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-12 mt-5">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-12 col-6 order center">
                                <img src="{{ asset($politicas->imagenes[0]->path) }}" alt="">
                            </div>
                            <div class="col-lg-6 col-md-8 col-sm-12 col-6 order">
                                <h1>{{$politicas->titulo}}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        {!!$politicas->descripcion !!}
                        {{-- <ul>
                            <li>Con el objetivo de establecer, implementar y mantener un Sistema de Gestión de la Calidad la dirección se compromete a:</li>
                            <br>
                            <li>• Mejorar continuamente su eficacia.</li>
                            <li>• Cumplir con los requisitos y leyes aplicables para el desarrollo de nuestra actividad.</li>
                            <li>• Desarrollar nuestras actividades procurando lograr la plena satisfacción de las necesidades
                             y expectativas de los afiliados y otras partes interesadas como colaboradores, proveedores, 
                             prestadores y accionistas.</li>
                            <li>• Desarrollar la competencia de nuestros colaboradores y concientizar para generar el compromiso
                             a los integrantes de nuestra organización sobre la importancia del Sistema de Gestión de la Calidad.</li>
    
                        </ul> --}}
                    </div>
                </div>
               
              
                <div class="certificado">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-12 mt-5">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-12 col-6 order center">
                               
                                <img src="{{ asset($certificados->imagenes[0]->path) }}" alt="">
                            </div>
                            <div class="col-lg-6 col-md-8 col-sm-12 col-6 order">
                                <h1>{{$certificados->titulo}}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        {!!$certificados->descripcion !!}
                        {{-- <ul>
                            <li> IRAM Certifica que:
                                MET CORDOBA S.A. – MET MEDICINA PRIVADA
                                Sucursal Obispo Oro: Obispo Oro 50 – (5000) – Córdoba – Pcia. de Córdoba – República Argentina.
                                Oficinas administrativas: Hipolito Yrigoyen 31 P. 1° y 6° – (5000) – Córdoba – Pcia. de Córdoba – República Argentina.
                            </li>
                            <br>
                            <li>
                                Posee un Sistema de Gestión de la Calidad que cumple con los requisitos de la norma:
                                IRAM –ISO 9001:2015<br>
                                Cuyo alcance es:<br>
                                OFICINA Y SUCURSAL<br>
                                Diseño y venta de planes de cobertura médica, atención al afiliado y verificación de la satisfacción del mismo.
                            </li>
                            <li>Certificado de Registro N° 9000-7912 <br>
                                Vigencia Desde: 28/12/2017<br>
                                Hasta: 15/12/2019<br>
                                Emisión: 18/01/2018<br>
                            </li>
                        </ul> --}}
                    </div>
                </div>
               

            </div>
            
        </div>  
        
        <div class="container foot">

@endsection