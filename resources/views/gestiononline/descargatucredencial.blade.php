@extends('layouts.app')
@section('title', ' | Gestión Online - Descargá tu credencial')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online">     
        <div class="container encabezado">
            <h1>GESTIÓN ONLINE</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Descargá tu credencial</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a class="active">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a href="{{ route('mediosdepagos') }}">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                    <h2>{{$DescargaCredencial->titulo}}</h2>
                    <div class="row m-0">
                        <div class="col-12 col-lg-5 intro">  
                            <h3>{{$DescargaCredencial->bajada}}</h3>                            
                            {!!$DescargaCredencial->descripcion!!}                           
                        </div>
                        <div class="col-12 col-lg-7 formulario">
                            <form>
                                <div class="form-group">
                                    <label>Tipo de Documento *</label>
                                    <select name="tipoDni" ng-model="tipoDoc" id="tipoDni">
                                        <option class="text-uppercase" value="0">SELECCIONE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="1" class="ng-binding ng-scope">BB</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="2" class="ng-binding ng-scope">C.F.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="3" class="ng-binding ng-scope">C.I.POLICIA FED</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="4" class="ng-binding ng-scope">CUIT</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="5" class="ng-binding ng-scope">D.N.I.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="6" class="ng-binding ng-scope">L.C.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="7" class="ng-binding ng-scope">L.E.</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="8" class="ng-binding ng-scope">MATRICULA</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="9" class="ng-binding ng-scope">NRO. CLIENTE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="10" class="ng-binding ng-scope">PASAPORTE</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="11" class="ng-binding ng-scope">CUIL</option>
                                        <option ng-repeat="tp in tiposDocumentos" value="12" class="ng-binding ng-scope">S.D.</option>                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Número de Documento *</label>
                                    <input type="text" name="dni" id="dni" maxlength="8" required>
                                </div>
                                
                                <h3 class="leyenda">*Datos obligatorios</h3>
                                    <button type="button" onclick="buscarAfiliadoByCredencial()">BUSCAR</button>
                            </form>  

                            <!-- Resultado -->
                            <form action="{{ route('imprimirPdf') }}" method="POST" onsubmit="enviarFormulario('enviar')">
                                @csrf
                                <div class="form-group">
                                    <label>N° de Afiliado</label>
                                    <input type="text" name="nafiliado" id="nafiliado" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Apellido y Nombre</label>
                                    <input type="text" name="nombreapellido" id="nombreapellido" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Documento</label>
                                    <input type="text" name="dnia" id="dnia" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Tipo de Afiliado</label>
                                    <input type="text" name="tipoa" id="tipoa" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Plan</label>
                                    <input type="text" name="plan" id="plan" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Adicionales</label>
                                    <input type="text" name="adicionales" id="adicionales" value="" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label>Validez Desde</label>
                                    <input type="text" name="validezDesde" id="validezDesde" value="" readonly="readonly">                                    
                                </div>
                                 <div class="form-group">
                                    <label>Validez Hasta </label>
                                    <input type="text" name="validezHasta" id="validezHasta" value="" readonly="readonly">                                    
                                </div>
                                <h3 class="leyenda">*El uso de esta credencial provisoria es personal e intransferible. DEBE IR ACOMPAÑADA CON SU DNI para su validez.</h3>
                                <div class="form-group">
                                    <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld0goQaAAAAACvPo6tN3YXaXp_U1A2Ti20uh_-_" data-callback="correctCaptcha"></div>
                                </div>
                                    <button  id="enviar" type="submit">GENERAR</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">

@endsection