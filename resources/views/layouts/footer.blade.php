        <div class="footer">
            <ul class="telefonos">
                <li>
                    <a href="wtai://wp/mc;08003450638">0800 - 345 - 0638</a>
                </li>
                <li>
                    <a href="wtai://wp/mc;08104444364">0810 - 4444364
                        <span>EMERGENCIAS · EMI</span>
                    </a>
                </li>
                <li>
                    <a href="wtai://wp/mc;08002882000">0800 - 288 - 2000
                        <span><img src="{{ asset('/images/logo-assistcard.svg') }}"></span>
                    </a>
                </li>
                <li>
                    <a href="https://miportal.sanatorioallende.com/" target="_blank">                        
                        <span>TURNOS CENTRO <br>MÉDICO MET</span>
                    </a>
                </li>
            </ul>
            <ul class="links">
                <li>
                    <a href="{{ route('novedades') }}">NOVEDADES</a>
                </li>
                <li>
                    <a href="{{ route('institucional') }}">INSTITUCIONAL</a>
                </li>
                <li>
                    <a href="{{ route('linkinteres') }}">LINKS DE INTERÉS</a>
                </li>
                <li>
                    <a href="{{ route('prestador') }}">¿QUERÉS SER PRESTADOR?</a>
                </li>
                <li>
                    <a href="http://prestaciones.metcordoba.com.ar/pages/login.faces" target="_blank">VALIDADOR DE PRESTADORES</a>
                </li>
                <li>
                    <a href="{{ route('trabajaenmet') }}">TRABAJÁ EN MET</a>
                </li>
            </ul>
            <ul class="logos">
                <li>
                    <a href="https://servicios1.afip.gov.ar/clavefiscal/qr/mobilePublicInfo.aspx?req=e1ttZXRob2Q9Z2V0UHVibGljSW5mb11bcGVyc29uYT0zMDY0Njk1MDYyM11bdGlwb2RvbWljaWxpbz0wXVtzZWN1ZW5jaWE9MF1bdXJsPWh0dHA6Ly93d3cubWV0bWVkaWNpbmFwcml2YWRhLmNvbS9dfQ==" target="_blank">
                        <img src="{{ asset('/images/data-fiscal.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{ asset('/pdf/iram-iso.pdf') }}" target="_blank">
                        <img src="{{ asset('/images/iram.jpg') }}">
                    </a>
                </li>
                <li>
                    <a>
                        <img src="{{ asset('/images/superintendencia.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="https://www.sanatorioallende.com/" target="_blank">
                        <img src="{{ asset('/images/sanatorio-allende.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="https://medicus.com.ar/" target="_blank">
                        <img src="{{ asset('/images/medicus.jpg') }}">
                    </a>
                </li>
            </ul>
        </div>
                         <!-- Modal Loading -->
<button class="btn btn-primary" type="button" id="loading" class="btn btn-lg" data-toggle="modal" data-target="#myModal" hidden="hidden">
    Loading Test
</button>
        <div id="sawubona-footer">
        </div>

    </div>
</div>




<div class="container">
  <!-- Modal -->
  <div class="modal fade modal-loading" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body  text-center">
                <!-- <img
                  class="loader"
                  src="img/Loading_2.gif"                                  
                /> -->
                <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                  <span class="sr-only">Cargando...</span>
                </div>
          <p>Ya casi terminamos,<br> tus datos están siendo procesados</p>
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>
  <!-- End Modal Loading -->