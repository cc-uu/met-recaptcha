<script>
    var Sawubona = {
        baseUrl: baseUrl,
        map: null,
        onloadImage: null,
        scrolling: null,
        shopping: null,
        usuarioTwitter: null
    }
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', '<?= config("main.ID_ANALYTIICS") ?>', 'auto');
    ga('send', 'pageview');
</script>

<script type="text/javascript" src="{{asset('/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/jquery/jquery.touch/jquery.touch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/bootstrap/bootstrap.carousel-responsive/bootstrap.carousel-responsive.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/sawubona/sawubona.footer/sawubona.footer.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/core/popper.min.js')}}"></script>


<script type="text/javascript" src="{{asset('/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/shared.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/numeric.js')}}"></script>

<script type="text/javascript" src="{{asset('/js/front.js')}}"></script>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr4L29JH5Z2tVFlLrVcYNdbWF432aAjXU&callback=initMap&libraries=places&v=weekly"
  defer
></script>



<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
