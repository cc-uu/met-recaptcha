<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Persona\Persona;

class referidosEmpleadosController extends Controller
{
    public function index(Request $request) 
    {
    	if ($request->isMethod('post')) {
		   	$xOrigen 	=$request->input('origen');
			$xNombre 	=$request->input('nombre');
			$xApellido 	=$request->input('apellido');
			$xDni 		=$request->input('dni');
			$xEmail 	=$request->input('email');
			$xPref1 	=$request->input('pref1');
			$xTelefono 	=$request->input('telefono');
			$xPref3 	=$request->input('pref1');

			//datos referente
			$xNombreR 	=$request->input('nombreR');
			$xApellidoR =$request->input('apellidoR');
			$xDniR 		=$request->input('dniR');
			$xPlanR 	=$request->input('planR');
			$xEmailR 	=$request->input('emailR');
			$xPref1R 	=$request->input('pref1R');
			$xTelefonoR =$request->input('telefonoR');

			if (! $xEmail 
				|| ! $xNombre
				|| ! $xApellido
				|| ! $xPref1
				|| ! $xDni
				|| ! $xEmailR
				|| ! $xNombreR
				|| ! $xApellidoR
				|| ! $xPref1R
				|| ! $xDniR
				|| ! $xPlanR
			) {
				echo '<script language="javascript">alert("Por favor complete todos los campos");</script>';
				return view('referidosempleados.index');
			}

			$oPersona = New Persona();
			$xIdSegmentoGCClientes = 3681;
			$Persona = $oPersona->getByDni($xIdSegmentoGCClientes, $xDni);
			if (!($Persona instanceof Persona)) {
				$Persona = new Persona();
				$Persona->idPersona = 0;
				$Persona->nombre = $xNombre;
				$Persona->apellido = $xApellido;
				$Persona->dni = $xDni;
			}
			$Persona->observaciones = 
				'Nombre referente : '.$xNombreR.$xApellidoR.',
				Dni referente : '.$xDniR.',
				Email referente : '.$xEmailR.',
				telefono referente : '.$xPref1R.'-'.$xTelefonoR.',
				Plan referente : '.$xPlanR;
			$Persona->emailReferente = $xEmailR;
			$Persona->emails[0] = $xEmail;
			$Persona->dniReferente = $xDniR;
			$Persona->email = $xEmail;
			$Persona->pref3 = $xPref3;
			$Persona->movil = $xTelefono;
			$xPerfile = 'Referidos_MET_Empleados';
			$xPerfilExP = NULL;
			
			// Datos personales
			$xDatosPersonales = []; 
			$xDniRArr 	= ['idDatoDuro' => 5197,
			 				'valor' 	=> $xDniR];
			$xEmailRArr = ['idDatoDuro' => 5196,
			 				'valor' 	=> $xEmailR];
			$xOrigenArr = ['idDatoDuro' => 5189,
			 				'valor' 	=> $xOrigen];

			array_push($xDatosPersonales, $xDniRArr);
			array_push($xDatosPersonales, $xEmailRArr);
			array_push($xDatosPersonales, $xOrigenArr);

			// $Parametros = "";

			$NuevaC = $oPersona->enviarAlAsignadorAutomatico($Persona, $xPerfile, $xPerfilExP, $xDatosPersonales);
			if ($NuevaC) {
				$xMensaje = 'La consulta se ha enviado correctamente.';
			} else {
				$xMensaje = 'Ha sucedido un error.Intentelo mas tarde';
			}

			$Parametros = ['xMensaje'	=> $xMensaje,
						   'xPerfile'	=> $xPerfile];

			return view('referidosempleados.gracias', compact('Parametros','xMensaje', 'xPerfile'));
		}

    	return view('referidosempleados.index');
	}
}
