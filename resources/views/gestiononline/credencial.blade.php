@include('layouts.scripts.css')

<div class="container vista-credencial">
    <img src="{{ asset('images/bg-credencial.jpg') }}" class="bg-credencial">
    <div class="row">
        <div class="col-12 datos-credencial">
            <!-- N de Afiliado -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>N° de Afiliado</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                <p>{{$nafiliado}}</p>
                </div>
            </div>

            <!-- Apellido y Nombre -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>Apellido y Nombre</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                    <p>{{$nombreapellido}}</p>
                </div>
            </div>

            <!-- Tipo y nro de doc -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>Documento</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                    <p>{{$dnia}}</p>
                </div>
            </div>

            <!-- Tipo afiliado -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>Tipo de Afiliado</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                    <p>{{$tipoa}}</p>
                </div>
            </div>

            <!-- Plan -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>Plan</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                    <p>{{$plan}}</p>
                </div>
            </div>

            <!-- Adicionales -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-5 text-right">
                    <p>Adicionales</p>
                </div>
                <div class="col-lg-7 text-left col-7">
                    <p>{{$adicionales}}</p>
                </div>
            </div>

            <!-- Vigencia -->
            <div class="row">
                <div class="offset-lg-1 col-lg-3 col-4 text-right">
                    <p>Validez</p>
                </div>
                <div class="col-lg-4 col-7">
                    <p>Desde: {{$validezDesde}} </p>
                </div>
                <div class="col-lg-4 col-7 offset-4">
                    <p>Hasta: {{$validezHasta}} </p>
                </div>
            </div>

            <p class="frase text-center"></p>
        </div>

        <p class="text-center"></p>

        <div class="col-12 datos-credencial no-imprimir">
            <div class="row">
                <div class="col-lg-4 col-12 text-center">
                    <button class="form-control no-imprimir" ng-click="imprimir()">Generar</button>
                </div>			
                <div class="col-lg-4 col-12 text-center">
                    <button class="form-control no-imprimir" ng-click="cancelar()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
 window.print(); 
 window.onafterprint = function(){
      window.history.back();
}
</script>