<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Contenido\Contenido;
class LinkInteresController extends Controller
{
    public function index() {

        $Contenidos = new Contenido(); 
        $linkInteres = $Contenidos->getAll(581,0,25);

        return view ('linkinteres.index',compact('linkInteres'));
    }
}
