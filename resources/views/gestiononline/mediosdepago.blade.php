@extends('layouts.app')
@section('title', ' | Gestión Online - Medios de pago')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion gestion-online baja">     
        <div class="container encabezado">
            <h1>Gestión Online</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <button class="chat">Chat <span>Online</span></button>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 p-0">
                    <li class="d-block d-sm-none btn-beneficio">
                        <button class="navbar-toggle" type="button" data-target="#menu-beneficio" data-toggle="collapse">
                        <h1>Medios de pago</h1>
                        <img class="svg" src="{{ asset('../images/iconos/up-menu.svg') }}" alt="">
                    </li>
                </div>
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 menu-gestiones collapse" id="menu-beneficio">
                    <ul>
                        <li>
                            <a href="{{ route('gestiononline') }}">Adhesión al débito</a>
                        </li>
                        <li>
                            <a href="{{ route('cambioplan') }}">Cambio de Plan</a>
                        </li>
                        <li>
                            <a href="{{ route('serviciosa') }}">Contratación de servicios adicionales</a>
                        </li>
                        <li>
                            <a href="{{ route('mifactura') }}">Descargá y pagá tu factura</a>
                        </li>
                        <li>
                            <a href="{{ route('micredencial') }}">Descargá tu credencial</a>
                        </li>
                        <!--<li>
                            <a href="{{ route('modfactura') }}">Modificación de datos de factura</a>
                        </li>-->
                        <li>
                            <a class="active">Medios de pago</a>
                        </li>
                        <li>
                            <a href="{{ route('formulariosonline') }}">Formularios Online</a>
                        </li>
                        <li>
                            <a href="{{ route('dardebaja') }}">Dar de baja</a>
                        </li>
                        <li>
                            <a href="{{ route('autorizaciones') }}">Autorizaciones</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-7 col-md-8 col-lg-9 p-0 right gestion">
                
                   
                   
                    <h2>{{$leyenda->titulo}}</h2>
                    <div class="row m-0">
                    <div class="row m-0 contenedor-medios-de-pago">
                        <div class="col-12 col-lg-6">
                            {!!$leyenda->descripcion!!}  
                        </div>
                    </div>
          
                        <div class="row m-0 contenedor-medios-de-pago">
                            
                            @foreach ($mediosDepago as $item)
                     
                            
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset($item->imagenes[0]->path) }}" alt="">
                                </i>
                                
                               
                                <button>{{$item->titulo}}</button>
                             
                            {!!$item->descripcion!!}
                                {{-- <p>Adherite al débito automático ingresando en 
                                    "<strong>MI MET</strong>" - "<strong>Gestión Online</strong>" - 
                                    "Adhesión al débito automático" 
                                    o llamando a nuestro <strong>0800</strong>. 

                                </p>--}}
                            </div>
                            @endforeach
                            {{-- <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/rapipago.svg') }}" alt="">
                                </i>
                                <button>PAGO FÁCIL / RAPIPAGO</button>
                                <p>Podés abonar tu factura consultando las sucursales habilitadas y medios de pago 
de cada entidad en <strong>www.localespagofacil.com
 y/o www.rapipago.com.ar</strong>
                                </p>
                            </div>
                        </div>

                        <div class="row m-0 contenedor-medios-de-pago">
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/deposito.svg') }}" alt="">
                                </i>
                                <button>DEPÓSITO O TRANSFERENCIA</button>
                                <p>Dirigí el depósito/transferencia al CBU <strong>2850309530000000080702</strong> (Banco Macro)
y enviá el comprobante a <strong>canalesdepago@metmedicinaprivada.com</strong>
                                </p>
                            </div>
                      
                             <div class="col-12 col-lg-6">
                                    <i class="iconos-medios">
                                        <img class="svg" src="{{ asset('../images/iconos/linkpago.svg') }}" alt="">
                                    </i>
                                    <a href="//{{$item->link}}">
                                    <button>PAGO MIS CUENTAS / LINK</button></a>
                                    <p>El número de gestión requerido para pago mis cuentas (sin factura) o para pagos link, se encuentra en la esquina superior derecha de tus facturas. (como opción se puede mostrar con la imagen de una factura donde están los números)
                                    </p>
                                </div>
                           
                           
                        </div>

                        <div class="row m-0 contenedor-medios-de-pago">
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/telefono.svg') }}" alt="">
                                </i>
                                <button>PAGO TELEFÓNICO</button>
                                <p>Aboná llamando al <strong>0800 3450 638 opción 4</strong>. </br>

Deberás informar:
DNI del Cliente, los números de la tarjeta (16 dígitos) y la fecha de vencimiento o fecha 'hasta' que se encuentran en el frente de la tarjeta, y el Código de seguridad (últimos 3 dígitos) al dorso sobre la banda blanca. 

                                </p>
                            </div>
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/mercadopago.svg') }}" alt="">
                                </i>
                                <button>MERCADOPAGO</button>
                                <p>Escaneá el código que se encuentra en la esquina inferior derecha de tu factura. También podés seleccionar 'Buscar Empresa' hasta encontrar el servicio MET CÓRDOBA, ingresando el número del código de barra manualmente. Tu pago será acreditado en la cuenta de MET dentro de las 48hs hábiles.
                                </p>
                            </div>
                        </div>

                        <div class="row m-0 contenedor-medios-de-pago">
                          
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/pagarcomar.png') }}" alt="">
                                </i>
                                <a href=""><button>PAGAR.com.ar</button></a>
                                <p>Seleccioná en 'Rubro' Medicina Prepaga y Obras Sociales, en 'Servicio' MET CÓRDOBA y como 'Código' el número de gestión para pagos Link que se encuentra en la esquina superior derecha de tu factura.

                                </p>
                            </div>
                        
                            <div class="col-12 col-lg-6">
                                <i class="iconos-medios">
                                    <img class="svg" src="{{ asset('../images/iconos/uala.svg') }}" alt="">
                                </i>
                                <button>TARJETA UALÁ / TARJETA BURBANK</button>
                                <p>Buscá a MET CÓRDOBA como Empresa e ingresá el número de gestión para pago sin factura que se encuentra en la esquina superior derecha de tu factura. El plazo de acreditación será dentro de las 48hs hábiles de haber abonado.
                                </p>
                            </div> --}}
                        </div>
                    </div>
                  
                    
                </div>
        <div class="container foot">
@endsection