<?php

/**
 * Ruta del home
 */

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/error', 'ErrorController@error')->name('error');
/**
 * Rutas de Error
 */
Route::get('/error/401', 'ErrorController@error401')->name('error401');
Route::get('/error/403', 'ErrorController@error403')->name('error403');
Route::get('/error/404', 'ErrorController@error404')->name('error404');
Route::get('/error/419', 'ErrorController@error419')->name('error419');
Route::get('/error/429', 'ErrorController@error429')->name('error429');
Route::get('/error/500', 'ErrorController@error500')->name('error500');
Route::get('/error/503', 'ErrorController@error503')->name('error503');

/**
 * Rutas de Catalogo
 */

/**
* Rutas del carrito
*/

/**  
* csrf Token
*/
Route::get('refresh-csrf', function(){
    return csrf_token();
    
});

//Vistas menu
Route::get('/cartillamedica', 'CartillamedicaController@index')->name('cartillamedica');
Route::get('/cartillamedica/geolocalizacion', 'CartillamedicaController@geolocalizacion')->name('geolocalizacion');
// Route::get('/planesdesalud', 'PlanesdesaludController@index')->name('planesdesalud');
Route::get('/planesdesalud/comparativa', 'PlanesdesaludController@comparativa');
Route::get('/plancorporativo', 'PlancorporativoController@index')->name('plancorporativo');
Route::get('/contacto', 'ContactoController@index')->name('contacto');
Route::get('/gestiononline', 'GestiononlineController@index')->name('gestiononline');
// Route::get('/centrosmedicos', 'CentrosmedicosController@index')->name('centrosmedicos');
Route::get('/novedades', 'NovedadesController@index')->name('novedades');
Route::get('/link-de-interes', 'LinkInteresController@index')->name('linkinteres');
Route::get('/institucional', 'InstitucionalController@index')->name('institucional');
Route::get('/beneficios', 'BeneficiosController@index')->name('beneficios');

//Gestion Online
Route::get('/gestiononline/cambiodeplan', 'GestiononlineController@cambiodeplan')->name('cambioplan');
Route::get('/gestiononline/contrataciondeserviciosadicionales', 'GestiononlineController@contrataciondeserviciosadicionales')->name('serviciosa');
Route::get('/gestiononline/descargatufactura', 'GestiononlineController@descargatufactura')->name('mifactura');
Route::get('/gestiononline/descargatucredencial', 'GestiononlineController@descargatucredencial')->name('micredencial');
Route::get('/gestiononline/modificaciondedatosdefactura', 'GestiononlineController@modificaciondedatosdefactura')->name('modfactura');
Route::get('/gestiononline/dardebaja', 'GestiononlineController@dardebaja')->name('dardebaja');
Route::get('/gestiononline/formulariosonline', 'GestiononlineController@formulariosonline')->name('formulariosonline');
Route::get('/gestiononline/mediosdepago', 'GestiononlineController@mediosdepago')->name('mediosdepagos');
Route::get('/trabajaenmet', 'TrabajaenmetController@index')->name('trabajaenmet');
Route::get('/prestadores', 'PrestadoresController@index')->name('prestador');

//AGREGADOS ANDRES
Route::get('/planesdesalud/comparativa/{tipo}', 'PlanesdesaludController@comparativa')->name('plancomparativo');
Route::get('/planesdesalud/plan/{id?}/{titulo?}', 'PlanesdesaludController@plandesplegado')->name('plandesplegado');
Route::get('/planesdesalud/plan/{id}/{titulo}/{gracias}', 'PlanesdesaludController@plandesplegadogracias')->name('plandesplegadoFormGracias');
Route::post('/planesdesalud/plan/enviar', 'PlanesdesaludController@plandesplegadoForm')->name('plandesplegadoForm');
Route::get('/novedades/novedad-ampliada/{id}/{titulo}', 'NovedadesController@novedadAmpliada')->name('novedaDesplegada');
Route::get('/beneficios/beneficio-ampliado/{id}/{titulo}', 'BeneficiosController@beneficioAmpliado')->name('beneficioDesplegado');
Route::get('/beneficios/{tag}', 'BeneficiosController@beneficioByIdTag')->name('beneficioByTag');

//FORMULARIOS
Route::post('/plancorporativo/enviado', 'FormulariosController@index')->name('fomulario.enviar');
Route::post('/contacto/enviado-afiliado', 'FormulariosController@contactoAfiliado')->name('fomulario.enviar.afiliado');
Route::post('/contacto/enviado-no-afiliado', 'FormulariosController@contactoNoAfiliado')->name('fomulario.enviar.noafiliado');
Route::post('/gestiononline/enviado-debitobanco', 'FormulariosController@contactoDebitoBanco')->name('fomulario.enviar.debitobanco');
Route::post('/gestiononline/enviado-debitotarjeta', 'FormulariosController@contactoDebitoTarjeta')->name('fomulario.enviar.debitotarjeta');
Route::post('/gestiononline/enviado-cambioplan', 'FormulariosController@contactoCambioPlan')->name('fomulario.enviar.cambiodeplan');
Route::post('/gestiononline/enviado-contratacion', 'FormulariosController@contactoContratacionSA')->name('fomulario.enviar.contratacionserviciosa');
Route::post('gestiononline/dar-de-baja', 'FormulariosController@contactoDardeBaja')->name('fomulario.enviar.dardebaja');
Route::post('gestiononline/modificar-datos', 'FormulariosController@contactoModificiarDatos')->name('fomulario.enviar.modificardatos');
Route::post('trabajaenmet/enviado', 'FormulariosController@contactoTrabajaEnMet')->name('formulario.enviar.trabajaenmet');
Route::post('prestador/enviado', 'FormulariosController@contactoPrestador')->name('fomulario.enviar.prestador');
Route::get('gestiononline/buscarAfiliado/{dni}', 'FormulariosController@contactoBuscarAfiliadoByDni');
Route::get('gestiononline/buscarAfiliadoWS/{tipo}/{dni}', 'FormulariosController@contactoBuscarAfiliadoByWS');
//Cartilla medica
Route::get('cartillamedica/farmacias/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getFarmacias');
Route::get('cartillamedica/opticas/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getOpticas');
Route::get('cartillamedica/odontologia/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getOdontologia');
Route::get('/cartillamedica/profesionales/{profesional}/{especialidad}/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getProfesionales');
Route::get('/cartillamedica/instituciones/{instituciones}/{especialidad}/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getInstituciones');
Route::get('/cartillamedica/instituciones/{especialidad}/{cuit}', 'CartillamedicaController@getProfesionalesByInstitucion');

Route::get('/cartillamedica/cartillamedica/instituciones/{instituciones}/{especialidad}/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getInstituciones');
Route::get('/cartillamedica/cartillamedica/instituciones/{especialidad}/{cuit}', 'CartillamedicaController@getProfesionalesByInstitucion');
Route::get('cartillamedica/cartillamedica/farmacias/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getFarmacias');
Route::get('cartillamedica/cartillamedica/opticas/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getOpticas');
Route::get('cartillamedica/cartillamedica/odontologia/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getOdontologia');
Route::get('cartillamedica/cartillamedica/profesionales/{profesional}/{especialidad}/{localidad}/{plan}/{offset?}/{limit?}', 'CartillamedicaController@getProfesionales'); 

//Centros medicos 
Route::get('/centrosmedicos/especialidad/{idCate}/{idCato}', 'CentrosmedicosController@getByEspecialidad');
Route::get('/centrosmedicos/profesional/{idCato}/{busqueda}', 'CentrosmedicosController@getByProfesional');

Route::get('/centrosmedicos/contenido/{especialidad}', 'CentrosmedicosController@getByContenido');

Route::get('/centrosmedicos/centrosmedicos/especialidad/{idCate}/{idCato}', 'CentrosmedicosController@getByEspecialidad');
Route::get('/centrosmedicos/centrosmedicos/profesional/{idCato}/{busqueda}', 'CentrosmedicosController@getByProfesional');
Route::get('/centrosmedicos/{id?}', 'CentrosmedicosController@index')->name('centrosmedicos'); 
Route::get('/wellness', 'WellnessController@index')->name('wellness');
Route::get('/wellness/{id}/{titulo?}/{gracias?}', 'WellnessController@index')->name('wellnessdesplegado');
Route::post('/wellness/enviar', 'WellnessController@wellnessForm')->name('wellnessdesplegadoForm');
Route::get('/gestiononline/autorizaciones', 'GestiononlineController@autorizaciones')->name('autorizaciones');
Route::post('/gestiononline/autorizaciones', 'GestiononlineController@autorizaciones')->name('autorizacionesPost');
Route::post('/buscador', 'BuscadorController@index')->name('buscador');
Route::get('/buscador', 'BuscadorController@index');
Route::get('/gestiononline/credencial', 'GestiononlineController@credencial')->name('credencial');

// Agregados Juli
// Gracias
Route::get('/gestiononline/{pagina}/gracias', function($pagina){
	return view('gestiononline.gestiongracias',compact('pagina'));
})->name('gestiongracias');
Route::get('/contacto/gracias', 'ContactoController@gracias')->name('contactogracias');
Route::get('/plancorporativo/gracias', 'PlancorporativoController@gracias')->name('plangracias');
Route::get('/trabajaenmet/gracias', 'TrabajaenmetController@gracias')->name('gracias');

//Landings vigentes
Route::get('/referidos-afiliados', 'referidosAfiliadosController@index');
Route::post('/referidos-afiliados', 'referidosAfiliadosController@index')->name('referidosAfiliadosPost');
Route::get('/referidos-empleados', 'referidosEmpleadosController@index');
Route::post('/referidos-empleados', 'referidosEmpleadosController@index')->name('referidosEmpleadosPost');
Route::get('/saludencasa', 'saludEnCasaController@index');
Route::post('/saludencasa', 'saludEnCasaController@index')->name('saludEnCasaPost');
Route::get('/mejorplansalud', 'mejorPlanSaludController@index');
Route::post('/mejorplansalud', 'mejorPlanSaludController@index')->name('mejorPlanSaludsPost');
Route::get('/vista-call', 'vistaCallController@index');
Route::post('/vista-call', 'vistaCallController@index')->name('vistaCallOriginPost');
Route::get('/mti', 'mtiController@index');
Route::post('/mti', 'mtiController@index')->name('mtisPost');

//fomularios de contacto
Route::post('/contacto/send', 'contactoController@send')->name('vistaCallPost');
Route::post('/imprimir', 'imprimirController@imprimir')->name('imprimirPdf');