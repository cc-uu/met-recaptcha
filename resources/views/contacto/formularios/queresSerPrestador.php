<form method="post" enctype="multipart/form-data" id="1237" onsubmit="enviarFormulario('btnEnviar')">
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b" >
				<input type="text" name="nombre" class="validar form-c" placeholder="NOMBRE*" id="Nombre">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" name="apellido" class="validar form-c" placeholder="APELLIDO*" id="Apellido">
			</li>

			<li class="list-group-item input_afil_b" >
				<input type="text" name="email" class="validar validarEmail form-c" placeholder="EMAIL*" id="E-mail">
			</li>
			<input type="hidden" name="idSegmento" value="1237">

			<li class="list-group-item input_afil_b" >
				<input type="text" class="form-c" placeholder="PROVINCIA" id="Provincia">
			</li>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">
			<li class="list-group-item input_afil_b" >
				<div class="row">
					<div class="col-xs-4">
						<input class="validarInt form-c" name="pref3" type="text" placeholder="PREFIJO" id="CaractTelefono">
					</div>
					<div class="col-xs-8">
						<input class="validarInt form-c" name="movil" type="text" placeholder="TELÉFONO" id="NumberTelefono">
					</div>
				</div>
			</li>
			<li class="list-group-item input_afil_b" >
				<input type="text" class="form-dd" placeholder="ARANCEL SUGERIDO" id="1246" name="arancel_sugerido">
			</li>
			<span class="data_oblig">* datos obligatorios</span>
		</ul>
	</div>
	<div class="col-xs-4">
		<ul class="list-group list_afil">

			<li class="list-group-item input_afil_02">

                <div class="fileUpload btn btn-primary">
                    <span>ADJUNTAR CV</span>
                    <input type="file" class="upload form-c" name="archivo" id="fileCurriculum" />

                </div>
			</li>

			<!--
			<li class="input_afil_03"><p>Si es Centro Médico o Institución Sanatorial, cargue aquí su nomenclador con aranceles:</p></li>
			<li class="list-group-item input_afil_02">

                <div class="fileUpload btn btn-primary">
                    <span>ADJUNTAR ARCHIVO</span>
                    <input type="file" class="upload" id="fileArchivo" />
                </div>

			</li>
			<li class="input_afil_03"><p style="color:#00a893;">Archivos pdf, word o jpg, que no superen los 2 MB cada archivo.</p></li>
			-->


			<li class="list-group-item">

				<input type="submit" value="ENVIAR" id="btnEnviar">
			</li>
		</ul>
	</div>
</form>