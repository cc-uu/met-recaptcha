<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Clases\Persona\Persona;

class Formulario extends Model
{
    public $idFormulario;
	public $idSegmento;
	public $nombre;
	public $habilitado;
	public $descripcion;
	public $header;
	public $bajada;
	public $css;
	public $campos; //	Array de campo
	public $datosDuros; //	Array de dato duro
	public $perfiles; //	Array de perfil
	public $idCampoValidacion; //	Campo para bebeto
	public $cantIngreso; //	Campo para bebeto
	public $segmentosValidacion; //	Campo para bebeto
	public $emailAlertas;
	public $confirmacion;
	public $configuracion;

	//	Idiomas
	//	es: Español
	//	en: Ingles
	//	po: Portugues
	//	fr: frances
	//	it: Italiano

	public function __construct($xIdFormulario = null, $xIdioma = 'es') {
		if (!$xIdFormulario) {
			return;
		}

		$this->setFormulario($xIdFormulario, $xIdioma);
	}

	public function setFormulario($xIdFormulario = null, $xIdioma = 'es') {
		if (!$xIdFormulario) {
			return;
		}

		try
		{
			$url = config('constants.urlApi') . "Formulario?xKey=" . config('constants.Key') . "&xIdSitio=" . config('constants.idSitio') . "&xIdFormulario=" . $xIdFormulario . "&xIdioma=" . $xIdioma;
			$vDataAPI = file_get_contents($url);
			$vDataAPI = json_decode($vDataAPI, true);
			$vDataAPI = $vDataAPI['data']['formularios'][0];

			if (!$vDataAPI) {
				return;
			}

			if (!$vDataAPI['habilitado']) {
				return;
			}

			$this->idFormulario = $vDataAPI['idFormulario'];
			$this->idSegmento = $vDataAPI['segmento']['idSegmento'];
			$this->nombre = $vDataAPI['nombre'];
			$this->habilitado = $vDataAPI['habilitado'];
			$this->descripcion = $vDataAPI['descripcion'];
			$this->header = $vDataAPI['header'];
			$this->bajada = $vDataAPI['bajada'];
			$this->css = $vDataAPI['css'];
			$this->emailAlertas = $vDataAPI['emailAlertas'];
			$this->confirmacion = $vDataAPI['confirmacion'];

			//	Campos fijos
			if ($vDataAPI['listaCampoFijo']) {
				foreach ($vDataAPI['listaCampoFijo'] as $xCampoAPI) {
					$this->campos[] = new Campo($xCampoAPI, $xIdioma);
				}
			}

			//	Datos duros
			if ($vDataAPI['configFormulario']['listaDatosDurosxSitio']) {
				foreach ($vDataAPI['configFormulario']['listaDatosDurosxSitio'] as $xDatoDuroAPI) {
					$this->datosDuros[] = new DatoDuro($xDatoDuroAPI);
				}
			}

			//	Perfiles
			if ($vDataAPI['configFormulario']['listaPerfilCustom']) {
				foreach ($vDataAPI['configFormulario']['listaPerfilCustom'] as $xPefilAPI) {
					$this->perfiles[] = new Perfil($xPefilAPI);
				}
			}

			//	Configuracion de impresion
			$this->configuracion =
				[
				'idioma' => $xIdioma,
				'columnas' => $vDataAPI['columnas'],
				'placeholder' => $vDataAPI['placeHolder'],
				'label' => $vDataAPI['label'],
				'radio' => $vDataAPI['radio'],
				'campoFull' => $vDataAPI['campoFull'],
			];

		} catch (Exception $e) {return null;}
	}

	public function printFormulario() {

		//	Cabecera
		echo '<form method="post">';
		echo '<div class="row">';
		echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
		echo '<h1>' . $this->nombre . '</h1>';
		if ($this->bajada != '') {
			echo '<h2>' . $this->bajada . '</h2>';
		}

		echo '</div>';
		echo '</div>';
		echo '<div class="row">';

		// Campos
		if ($this->campos) {
			foreach ($this->campos as $xCampo) {
				$xCampo->printCampo(
					$this->configuracion['idioma'],
					$this->configuracion['columnas'],
					$this->configuracion['placeholder'],
					$this->configuracion['label'],
					$this->configuracion['radio'],
					$this->configuracion['campoFull']
				);
			}
		}

		//	Datos duros
		if ($this->datosDuros) {
			foreach ($this->datosDuros as $xDatoDuro) {
				$xDatoDuro->printDatoDuro(
					$this->configuracion['columnas'],
					$this->configuracion['placeholder'],
					$this->configuracion['label'],
					$this->configuracion['radio']
				);
			}
		}

		//	Perfiles
		if ($this->perfiles) {
			foreach ($this->perfiles as $xPerfil) {
				$xPerfil->printPerfil(
					$this->configuracion['columnas'],
					$this->configuracion['placeholder'],
					$this->configuracion['label'],
					$this->configuracion['radio'],
					$this->configuracion['campoFull']
				);
			}
		}

		echo '</div>';

		if (!$this->campos && !$this->datosDuros && !$this->perfiles) {
			return;
		}

		echo '<div class="row">';
		echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
		echo '<button type="button" id="btnEnviarFormulario" class="btn btn-default">Enviar</button>';
		echo '</div>';
		echo '</div>';
		echo '</form>';

		echo '<div class="modal fade" id="mFormulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		echo '	<div class="modal-dialog">';
		echo '		<div class="modal-content">';
		echo '		</div>';
		echo '	</div>';
		echo '</div>';

		//	Javascript
		echo '<script>
            var xIdioma="' . $this->configuracion['idioma'] . '";
            var xSitio={idSitio:"' . config('constants.idSitio') . '"};
            var xSegmento={idSegmento:"' . $this->idSegmento . '"};
            var xEmailAlertas = Array();';

		if ($this->emailAlertas) {
			foreach ($this->emailAlertas as $xEmail) {
				'xEmailAlertas.push(' . $xEmail . ')';
			}
		}

		echo 'var xFormulario={
                idFormulario:"' . $this->idFormulario . '",
                nombre:"' . $this->nombre . '",
                idioma:"' . $this->configuracion['idioma'] . '",
                idCampoValidacion:"' . $this->idCampoValidacion . '",
                cantIngreso:"' . $this->cantIngreso . '",
                segmentosValidacion:"' . $this->segmentosValidacion . '",
                emailAlertas: xEmailAlertas,
                confirmacion: "' . $this->confirmacion . '"
            };
            </script>';
		echo '<script src="{{ asset("/libs/jquery-2.1.1.min.js") }}"></script>';
		echo '<script src="{{ asset("/js/formulario.js") }}"></script>';
	}

	public function sendFormulario($xPersona = null, $archivos = null) {
		if (!$xPersona) {
			return;
		}

		if (isset($xPersona['asignadorAutomatico'])) {
			return $this->sendToAsignadorAutomatico($xPersona);
		}

		$xPersona['sitio'] = new Sitio(config('constants.idSitio') );

		try
		{
			//API Url
			$url = config('constants.urlApi') . "PersonaMet?xKey=" . config('constants.Key') . "&xIdSegmentoAux=581";

			
			$ch = curl_init($url);

			$jsonData = $xPersona;
			$jsonDataEncoded = json_encode($jsonData);

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $jsonDataEncoded,
				CURLOPT_HTTPHEADER => array(
					"Content-Type: application/json",
					"Postman-Token: 5cb536c9-de43-4a38-bba3-54e0d027e9b3",
					"cache-control: no-cache"
				),
			));

			$result = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);


			$retorno_json = json_decode($result);

			//	Subir archivos
			if (isset($archivos) && !empty($archivos) && !empty($archivos['name']) && isset($retorno_json->data->personas[0]->idPersona) && $retorno_json->data->personas[0]->idPersona > 0) {
				
				$xRuta = 'temp/';
				$xInfo = pathinfo($archivos['name']);
				$xExtension = $xInfo['extension'];

				$xNewname = $xInfo['filename'] . '.' . $xExtension;

				$xTarget = $xRuta . $xNewname;

				if (in_array($xExtension, config('constants.extensiones') )) {
					if (move_uploaded_file($archivos['tmp_name'], $xTarget)) {
						$oPersona = new Persona();
						$personaAPI = $retorno_json->data->personas[0];

						$oArchivo = new Archivo();
						$oArchivo->nombre = $xNewname;
						$oArchivo->extension = $xExtension;
						$oArchivo->content = base64_encode(fread(fopen($xTarget, "rb"), $archivos['size']));

						$personaAPI->archivosxPersona = array($oArchivo);

						unlink($xTarget);

						$arreglo = json_encode($personaAPI);

						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_URL => "https://apirest.fidelitytools.net/api/ArchivosxPersona?xKey=265c092ff1813072ffeb07ec2ab84e4d",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => $arreglo,
						  CURLOPT_HTTPHEADER => array(
						    "Content-Type: application/json",
						    "Postman-Token: 5cb536c9-de43-4a38-bba3-54e0d027e9b3",
						    "cache-control: no-cache"
						  ),
						));

						$response1 = curl_exec($curl);
						$err = curl_error($curl);
						curl_close($curl);
						
					}
				}
			}

			return $result;
		} catch (Exception $e) {}
	}

	private function sendToAsignadorAutomatico($xPersona) {
		try
		{
			// echo '<pre>';
			// print_r($xPersona);die;
			// echo '</pre>';
			$Persona = new Persona();
			if (isset($xPersona['nombre'])) {
				$Persona->nombre = $xPersona['nombre'];
			}

			if (isset($xPersona['apellido'])) {
				$Persona->apellido = $xPersona['apellido'];
			}

			if (isset($xPersona['email'])) {
				$Persona->email = $xPersona['email'];
			}

			if (isset($xPersona['observaciones'])) {
				$Persona->observaciones = $xPersona['observaciones'];
			}

			if (isset($xPersona['dni'])) {
				$Persona->dni = $xPersona['dni'];
			}
			/**
			 * Telefono
			 */
			if (isset($xPersona['pref1'])) {
				$Persona->pref1 = $xPersona['pref1'];
			}
			if (isset($xPersona['telefono'])) {
				$Persona->telefono = $xPersona['telefono'];
			}

			/**
			 * Telefono 2
			 */
			if (isset($xPersona['pref2'])) {
				$Persona->pref2 = $xPersona['pref2'];
			}
			if (isset($xPersona['telefono2'])) {
				$Persona->telefono2 = $xPersona['telefono2'];
			}

			/**
			 * Telefono 3
			 */
			if (isset($xPersona['telefono3'])) {
				$Persona->telefono3 = $xPersona['telefono3'];
			}

			/**
			 * Movil
			 */
			if (isset($xPersona['pref3'])) {
				$Persona->pref3 = $xPersona['pref3'];
			}
			if (isset($xPersona['movil'])) {
				$Persona->movil = $xPersona['movil'];
			}

			if (isset($xPersona['localidad'])) {
				$Persona->localidad = $xPersona['localidad'];
			}

			if (isset($xPersona['provincia'])) {
				$Persona->provincia = $xPersona['provincia'];
			}

			if (isset($xPersona['empresa'])) {
				$Persona->empresa = $xPersona['empresa'];
			}

			if (isset($xPersona['direccion'])) {
				$Persona->direccion = $xPersona['direccion'];
			}

			if (isset($xPersona['numero'])) {
				$Persona->numero = $xPersona['numero'];
			}

			if (isset($xPersona['barrio'])) {
				$Persona->barrio = $xPersona['barrio'];
			}

			if (isset($xPersona['postal'])) {
				$Persona->postal = $xPersona['postal'];
			}

			if (isset($xPersona['segmento'])) {
				$Persona->segmento = $xPersona['segmento'];
			}

			if (isset($xPersona['sexo'])) {
				$Persona->sexo = $xPersona['sexo'];
			}

			if (isset($xPersona['proviene'])) {
				$Persona->proviene = $xPersona['proviene'];
			}

			if (isset($xPersona['datosDurosxSitioxPersona'])) {
				$Persona->camposPersonales = $this->convertDatosDurosPorSitioPorPersona_to_camposPersonales($xPersona['datosDurosxSitioxPersona']);
			}
			
			if (isset($xPersona['origenWeb']) AND $xPersona['origenWeb'] == 1) {
				$sPerfil = "WEB";
			} else {
				$sPerfil = $xPersona['origenWeb'];
			}

			// echo '<pre>';
			// print_r($Persona);die;

			if (isset($xPersona['perfilCustomxPersona'])) {
				$Persona->perfilCustomxPersona = $xPersona['perfilCustomxPersona'];
			}

			$Persona->enviarAlAsignadorAutomatico($Persona, $sPerfil);

		} catch (Exception $e) {return "Exception {$e->getMessage()}";}
	}

	private function convertDatosDurosPorSitioPorPersona_to_camposPersonales($arrDatosDurosXSitioXPersona) {
		$accumulator = [];

		if (!is_array($arrDatosDurosXSitioXPersona) || !count($arrDatosDurosXSitioXPersona)) {
			return $accumulator;
		}

		foreach ($arrDatosDurosXSitioXPersona as $arrAssocDatoDuro) {
			$temp = [];
			$temp['idCampoPersonal'] = $arrAssocDatoDuro['idDatoDuro'];
			$temp['valor'] = $arrAssocDatoDuro['valor'];
			if (!empty($temp['idCampoPersonal']) && !empty($temp['valor'])) {
				$accumulator[] = $temp;
			}
		}

		return $accumulator;
	}
}
