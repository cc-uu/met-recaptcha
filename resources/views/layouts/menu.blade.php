<div class="top">
    <div class="container p-0">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('/images/logo-metmedicinaprivada.svg') }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarMenu">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{!route::is('cartillamedica') ? : 'active'}}">
                        <a class="nav-link {{!route::is('cartillamedica') ? : 'active'}}" href="{{ route('cartillamedica') }}">CARTILLA MÉDICA</a>
                    </li>
                    <li class="nav-item {{!route::is('plandesplegado') ? : 'active'}}">
                        <a class="nav-link {{!route::is('plandesplegado') ? : 'active'}}" href="{{ route('plandesplegado') }}">PLANES DE SALUD</a>
                    </li>
                    <li class="nav-item {{!route::is('plancorporativo') ? : 'active'}}">
                        <a class="nav-link {{!route::is('plancorporativo') ? : 'active'}}" href="{{ route('plancorporativo') }}">PLAN CORPORATIVO</a>
                    </li>
                    <li class="nav-item {{!route::is('contacto') ? : 'active'}}">
                        <a class="nav-link {{!route::is('contacto') ? : 'active'}}" href="{{ route('contacto') }}">CONTACTO</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        MI MET
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item"  href="{{ route('gestiononline') }}">GESTIÓN ONLINE</a>
                            <a class="dropdown-item" href="{{ route('centrosmedicos',  ['id' => 'nueva-cordoba']) }}">CENTROS MÉDICOS</a>
                            <a class="dropdown-item" href="https://miportal.sanatorioallende.com/auth/loginPortal" target="_blank">TURNOS</a>
                            <a class="dropdown-item" href="{{ route('mifactura') }}">MI FACTURA</a>
                            <a class="dropdown-item" href="{{ route('micredencial') }}">MI CREDENCIAL</a>                            
                            <a class="dropdown-item" href="{{ route('wellness') }}">WELLNESS</a>
                            <a class="dropdown-item" href="{{ route('beneficios') }}">BENEFICIOS</a>
                        </div>
                    </li>                
                </ul>              
            </div>
        </nav>
    </div>
</div>