<?php
return [
	'ID_SITIO' => '106',
	'URL_API' => 'https://apirest.fidelitytools.net/api/',
	'URL_REST' => 'https://rest.fidelitytools.net/Fidelitytools/',
	'URL_WEBAPI' => 'https://rest.fidelitytools.net',
	'URL_WS' => 'https://ws.fidelitytools.net/v2/api',
	'FIDELITY_KEY' => '265c092ff1813072ffeb07ec2ab84e4d',
	'WS_KEY' => '5d52c64c5cb8340f980bdaab',


	'PAGE_CACHE' => false,
	'ID_ANALYTIICS' => 'UA-23691022-1',
	'IDIOMA_DEFAULT' => 'es',
	'MULTIDIOMAS' => true,
	'NOMBRE_SITIO' => "MET Medicina Privada",

	'ID_DESCUENTOS' => 0,
	'ID_ECOMMERCE' => 3565,
	'ID_ECOMMERCE_ENCODE' => 'MzU2NQ',
	'ID_SITIO_ENCODE' => 'MTA2',
	
	'JS_MAP' => false,
	'JS_SCROLLING' => false,
	'JS_SCROLLING_LIMIT' => false,
	'JS_SCROLLING_LOADING' => true,
	'JS_SHOPPING_CART' => false,

	'META_DESCRIPTION' => 'Somos una empresa de Medicina Prepaga con servicios de excelencia y constante preocupación por el bienestar de nuestros afiliados. Esto nos trascender la mera calidad médica con el respaldo y la experiencia de dos instituciones líderes: Sanatorio Allende y MEDICUS.  Independencia 714. CÓRDOBA, ARGENTINA | 0800-3450-638 (MET)',
	'META_KEYWORDS' => 'Met medicina, met prepaga, medicina prepaga, salud prepaga, consultorio met, met, medicina córdoba, sanatorio allende, planes de salud met, centro de atencion met, 0800 met, centro médico met, planes de salud met',

	'USUARIO_TWITTER' => '@',
	'IS_CACHE' => (isset($_SERVER['SERVER_ADDR']) AND ($_SERVER['SERVER_ADDR'] == '10.1.2.10')) ? true : false,
	'CACHE' => true,
	'CACHE_CATEGORIA' => 7200,
	'CACHE_CONTENIDO' => 5,
	'CACHE_PERSONA' => 3600,
	'CACHE_PRODUCTO' => 600,
	'CACHE_ARCHIVO' => 7200,
	'CACHE_ETIQUETA' => 7200,
	'DEVMODE' => FALSE,
];
