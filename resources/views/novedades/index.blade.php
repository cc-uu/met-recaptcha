@extends('layouts.app')
@section('title', ' | Novedades')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

    <div class="container-fluid fdo-seccion contacto novedades">     
        <div class="container encabezado">
            <h1>NOVEDADES</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
        </div> 
        <div class="container main">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-5 col-lg-5 left"> 
                    <div class="tab-content" id="v-pills-tabContent">
                    @foreach ($NovedadesDestacadas as $item)
                    <div class="tab-pane fade show centrar-img {{$loop->first ? 'active' : '' }}" id="v-pills-home-{{$item->idContenido}}" role="tabpanel">
                        @if($item->imagenes)
                        <img src="{{ asset($item->imagenes[0]->path) }}" alt="">
                        @else
                        <img src="{{ asset('../images/novedad.jpg') }}" alt="">
                        @endif
                    </div>
                    @endforeach
                </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 right">
                    <div class="tab-content" id="v-pills-tabContent">
                
                        @foreach ($NovedadesDestacadas as $item)                                                     
                            
                    <div class="tab-pane fade show {{$loop->first ? "active" : "" }}" id="v-pills-home-{{$item->idContenido}}" role="tabpanel">
                           <p><strong>{{$item->titulo}} </strong></p>
                         
                           <p>   {{ Str::limit(strip_tags($item->descripcion),200)}}</p>

                                <a href="{{ route('novedaDesplegada', ['id' => $item->idContenido ,'titulo' => str_slug($item->titulo) ]) }}"><span>Ver más</span></a>
                        </div>
                       

                        @endforeach
                        {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel">
                            <p><strong> CON MET TENES MÉDICO ON LINE </strong></p>

                           <p> Por ser afiliado MET, tenés un nuevo servicio: MÉDICO ON LINE 24hs</p>
                            
                            <p>Con la APP Llamando Al Doctor, tenés atención médica calificada e inmediata por 
                                videollamada, disponible las 24 horas, los 365 días del año.</p>
                                <span>Ver más</span>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel">
                            <p><strong> CON MET TENES MÉDICO ON LINE </strong></p>

                           <p> Por ser afiliado MET, tenés un nuevo servicio: MÉDICO ON LINE 24hs</p>
                            
                            <p>Con la APP Llamando Al Doctor, tenés atención médica calificada e inmediata por 
                                videollamada, disponible las 24 horas, los 365 días del año.</p>
                                <span>Ver más</span>
                        </div>                         --}}
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-12 right flex">
                    <div class="nav sucursales flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach ($NovedadesDestacadas as $item)
                     
                      
                    <a class="{{$loop->first ? "active" : "" }}" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home-{{$item->idContenido}}" role="tab" aria-selected="true">
                        <span> {{ $item->titulo }}</span>
                        </a>


                        @endforeach
                        {{-- <a class="" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-selected="false">
                            <span>CON MET TENES MÉDICO
                                ON LINE</span>
                        </a>
                        <a class="" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-selected="false">
                            <span>CON MET TENES MÉDICO
                                ON LINE</span>
                        </a>                         --}}
                    </div>
                </div>
            </div>
        </div>  

        <div class="container main">
            <div class="row resultante">
                @foreach ($Novedades as $item)
                     
               
                    <div class="acordion-resultantes col-md-6">
                        <div class="modulo-resultante">
                            <div class="card-header">                                
                                <a href="{{ route('novedaDesplegada', ['id' => $item->idContenido ,'titulo' => str_slug($item->titulo) ]) }}">
                                    <div class="centrar-img">
                                        @if($item->imagenes)
                                        <img src="{{ asset($item->imagenes[0]->path) }}" alt="">
                                        @else
                                        <img src="{{ asset('../images/novedad.jpg') }}" alt="">
                                        @endif
                                    </div>
                                    <div class="info-novedades">
                                        <h2>{{ $item->titulo }}</h2>
                                        <p>{{ Str::limit(strip_tags($item->descripcion),100)}}</p> 
                                        <div class="btn-novedad">
                                            <span>Ver más</span>
                                        </div> 
                                    </div>
                                </a>                                  
                            </div>
                        </div>                          
                    </div>

               

                @endforeach
                
                
                {{-- <div class="acordion-resultantes col-md-6">
                    <div class="modulo-resultante">
                        <div class="card-header">                                
                            <a href="#">
                                <div class="centrar-img">
                                    <img src="{{ asset('../images/novedad.jpg') }}" alt="">
                                </div>
                                <div class="info-novedades">
                                    <h2>PIEL PLENA</h2>
                                    <p>Estar radiante es tu elección y nosotros te acercamos 
                                            un beneficio con MET, en PIEL PLENA accedes al 10% de </p> 
                                    <div class="btn-novedad">
                                        <span>Ir al sitio</span>
                                    </div> 
                                </div>
                            </a>                                  
                        </div>
                    </div>                          
                </div> --}}
            </div>
            <div class="row paginador">
                <nav aria-label="navigation">
                    {!! $Novedades->links() !!}
                    {{-- <ul class="pagination">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><img class="svg" src="{{ asset('../images/iconos/down.svg') }}" alt=""></a>
                      </li>
            
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#"><img class="svg" src="{{ asset('../images/iconos/up.svg') }}" alt=""></a>
                      </li>
                    </ul> --}}
                  </nav>
            </div>
        </div>
        <div class="container foot">

@endsection