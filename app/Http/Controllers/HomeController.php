<?php
namespace App\Http\Controllers;
use App\Clases\Contenido\Contenido;
/**
 *  [index description]  -> Muestra la aplicacion.
 *  [prueba description]  -> Funcion para realizar pruebas unicamente.
 */

class HomeController extends Controller {
	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		$Contenidos = new Contenido();

		$SlideHomeArriba = $Contenidos->getAll(9849,0,10,'orden','ASC');
	   	$SlideHomeAbajo = $Contenidos->getAll(2210,0,10,'orden','ASC'); 
		$BotonesHome = array_sort_recursive($Contenidos->getAll(9850));
		$ContenidosCarrusel =  $Contenidos->getAll(9977,0,20,'orden','ASC');
		$TitulosCarrusel =  $Contenidos->getAll(9978,0,20);
		// $PopUP =  $Contenidos->getAll(9706,0,20);
	  
	  return view('home.index',compact('SlideHomeArriba','SlideHomeAbajo','BotonesHome','TitulosCarrusel','ContenidosCarrusel'));
	
	}
}