@extends('layouts.app')
@section('title', ' | Centros Médicos')
@section('body-clase','landing-page sidebar-collapse')

@section('contenido')
    @include('layouts.menu')

        <!-- Pop up -->
    @if ($PopUP != null && $PopUP[0])
        <div class="modal fade" id="modal-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">                
                    <div class="modal-body">  
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>         
                        <img src="{{$PopUP[0]->imagenes[0]->path }}">
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container-fluid fdo-seccion centros-medicos">     
        <div class="container encabezado">
            <h1>CENTROS MÉDICOS</h1><br>
            <a href="javascript:history.back()">
                <img src="{{ asset('/images/volver.svg') }}"> Volver
            </a>
            <div class="btn-institucional">
                @foreach ($centrosMedicos as $item)
                    <a href="{{route('centrosmedicos',['id' => str_slug($item->titulo)])}}">
                        <button class="btn-mapa-{{$item->orden}}@if ( (Request::path() == 'centrosmedicos/nueva-cordoba' && $loop->first) || (Request::path() == 'centrosmedicos/cerro-de-las-rosas'  && !$loop->first) ) active @endif">{{$item->titulo}}</button>
                    </a>
                @endforeach
               
                
            </div>
        </div> 
        <div class="container main">
            <div class="row">
               
                <div class="col-12 col-sm-5 col-md-4 col-lg-5 p-0 left mapa-1">
                    <div class="centrar-img">
                        
                        <img src="{{ asset($centrosMedico->imagenes[0]->path) }}">
                    </div>
                </div>
                
                    <div class="col-12 col-sm-7 col-md-8 col-lg-7 p-0 right mapa-1">
                       {!! $centrosMedico->detalle!!} 
                    </div>
               
                {{-- <div class="col-12 col-sm-7 col-md-8 col-lg-7 p-0 right mapa-2">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2024.4038881470808!2d-64.18830752323207!3d-31.424375324461426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9432a28f428afa4b%3A0x5c346b45d4ebbefc!2sMET%20Medicina%20Privada!5e0!3m2!1ses!2sar!4v1587561383466!5m2!1ses!2sar" width="100%" height="322" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div> --}}
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 centros-xs">
                    
                    
                        {!! $centrosMedico->descripcion!!} 
                        
                   
                </div>
                <div class="col-xl-4 col-lg-3 col-md-6 col-sm-6 buscador-especialidad">
                    <div class="contenedor-buscador">
                        <h2>PLANTEL MÉDICO Y HORARIOS</h2>
                        <form>
                            <div class="form-group">
                                <label>Profesional</label>
                                <input type="text" name="profesional" id="profesional">
                            <input type="hidden" name="idcatalogo" value="{{$idCatalogo}}" id="idcatalogo">
                               
                            </div>

                            <div class="form-group">
                                <label>Especialidad</label>
                                <select name="especialidad" id="especialidad">
                                    <option value="" selected="selected">Seleccione</option>
                                    @foreach ($Categorias as $item)
                                <option value="{{$item->idCategoria}}">{{$item->nombre}}</option>
                                    @endforeach
                                    
                                    
                                </select>
                            </div>
<!-- Nuevo filtro--------------------------------------------------------------------->
                            <div class="form-group">
                                <label>Modalidad</label>
                                <select name="modalidad" id="modalidad">
                                    <!-- <option value="" >Seleccione</option> -->
                                    <option selected="selected" value="presencial">Presencial</option>
                                    <option value="telefonico">Telefónica</option>
                                </select>
                            </div>
<!-- Fin Nuevo filtro--------------------------------------------------------------------->
                            
                            <button type="button" onclick="buscarhorarios()"> BUSCAR</button>
                            
                        </form>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-9 turnos">
                    <div class="table">
                        <table class="table-bordered"  id="tablahorarios">
                        <thead>
                          <tr>
                            <th class="titulo-table" scope="col" width="25%"><p id="especialidadSeleccionado"></p></th>
                            <th class="dias" scope="col" width="15%"><p>LUNES</p></th>
                            <th class="dias" scope="col" width="15%"><p>MARTES</p></th>
                            <th class="dias" scope="col" width="15%"><p>MIÉRCOLES</p></th>
                            <th class="dias" scope="col" width="15%"><p>JUEVES</p></th>
                            <th class="dias" scope="col" width="15%"><p>VIERNES</p></th>
                          </tr>
                        </thead>

                        <tbody>
              
                        </tbody>
                      </table>
                  
                    <div id="descripcionTabla">
                    </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="container foot">


@endsection