
    function initialize() {
      var marcadores = [
        ['Posada del Labriego', -32.746949,-63.779372],
        ['Hotel Los Naranjos', -32.7556057,-63.7926248],
        ['Plumas Hotel & Suites', -32.75913,-63.7878349],
        ['Hotel Parai-Maru', -32.7503563,-63.7848696],
        ['Posada del Viajero', -33.0896651,-64.3100152,14],
        ['Grand Hotel', -33.1235999,-64.3504528,17],
        ['Howard Johnson', -33.1126797,-64.3499436,17],
        ['Crillón Hotel', -33.1274532,-64.3505893,17],
        ['Hotel República',      -32.4134886,-63.2393472],
        ['Hotel Milenium',-32.399639,-63.2470527],
        ['Babel',-32.4082015,-63.2466341],
        ['Center Hotel',-32.412977,-63.2367227],
        ['Howard Johnson',-32.414977,-63.2440307]
      ];
      var map = new google.maps.Map(document.getElementById('mapa'), {
        zoom: 8,
        center: new google.maps.LatLng(-33.0514448,-63.6520461),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < marcadores.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
          map: map
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(marcadores[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
